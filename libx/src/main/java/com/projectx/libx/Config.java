package com.projectx.libx;

import com.projectx.libx.View.LicenceDialog;

public class Config {

    private static Config instance = null;
    protected Config() {
        // Exists only to defeat instantiation.
    }
    public static Config getInstance() {
        if(instance == null) {
            instance = new Config();
        }
        return instance;
    }

    public void tes(LicenceDialog act) {
        act.Close();
    }
}
