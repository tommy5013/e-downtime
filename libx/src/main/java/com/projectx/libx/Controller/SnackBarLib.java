package com.projectx.libx.Controller;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.TextView;

import com.projectx.libx.R;

public class SnackBarLib {
    private Context context;
    private View view;
    public final String WARNING = "warning";
    public final String ERROR = "error";
    public final String SUCCESSFUL = "successful";
    public final String INFO = "info";

    public void initSnackBar(Context context,View view){
        this.context = context;
        this.view = view;
    }
    public void show(String text, String type){
        Snackbar snackbar = Snackbar.make(view, text, Snackbar.LENGTH_LONG);
        View snackbarLayout = snackbar.getView();
        TextView textView = (TextView) snackbarLayout.findViewById(android.support.design.R.id.snackbar_text);
        switch (type){
            case WARNING:
                snackbarLayout.setBackgroundColor(ContextCompat.getColor(context, R.color.warning_color));
                textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.baseline_error_outline_white_24, 0, 0, 0);
                break;
            case ERROR:
                snackbarLayout.setBackgroundColor(ContextCompat.getColor(context, R.color.error_color));
                textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.outline_clear_white_24, 0, 0, 0);
                break;
            case SUCCESSFUL:
                snackbarLayout.setBackgroundColor(ContextCompat.getColor(context, R.color.successful_color));
                textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.baseline_done_white_24, 0, 0, 0);
                break;
            case INFO:
                snackbarLayout.setBackgroundColor(ContextCompat.getColor(context, R.color.info_color));
                textView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.outline_info_white_24, 0, 0, 0);
                break;
        }
        textView.setCompoundDrawablePadding(context.getResources().getDimensionPixelOffset(R.dimen.snackbar_icon_padding));
        snackbar.show();
    }
}
