package com.projectx.libx.Utils;


import android.content.Context;
import android.content.SharedPreferences;

public class SharedPref {

    public static String LICSTATUS="LICSTATUS";
    public static String LICVAL="0";
    public static final String MyPREFERENCES = "LICPREF" ;

    SharedPreferences sharedpreferences;

    public String GetLic(final Context context){
        sharedpreferences = context.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        try{
            LICVAL = sharedpreferences.getString(LICSTATUS,"0");
            return LICVAL;
        }
        catch (Exception ex){
            return "0";
        }
    }


    public String SaveLic(final Context context,String  value){
        sharedpreferences = context.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        try{
            SharedPreferences.Editor editor = sharedpreferences.edit();
            editor.putString(LICSTATUS, value);
            editor.commit();
            return "1";
        }
        catch (Exception ex){
            return "0";
        }
    }

}
