package com.projectx.libx.View;


import android.app.Activity;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.projectx.libx.Controller.LicenceDialogController;
import com.projectx.libx.R;
import com.projectx.libx.Utils.SharedPref;

import static android.content.Context.MODE_PRIVATE;

public class LicenceDialog extends DialogFragment {

    private String message,title;
    private EditText deviceName,location;

    public String getAppName() {
        return AppName;
    }

    public void setAppName(String appName) {
        AppName = appName;
    }

    private String AppName;

    private AppCompatActivity parentActivity;
    private ImageView ivClose;

    public interface EditNameDialogListener {
        void onFinishEditDialog(String inputText);
    }



    public LicenceDialog() {
        // Empty constructor is required for DialogFragment
        // Make sure not to add arguments to the constructor
        // Use `newInstance` instead as shown below

    }

    public static LicenceDialog newInstance(AppCompatActivity parentActivity, String title, String message) {
        LicenceDialog frag = new LicenceDialog();
        frag.message = message;
        frag.title = title;
        frag.parentActivity = parentActivity;
        return frag;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        getDialog().setCanceledOnTouchOutside(false);
        getDialog().setCancelable(true);

        return inflater.inflate(R.layout.layout_licence_dialog, container);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        Button btnOk = view.findViewById(R.id.btnRegister);
        deviceName = view.findViewById(R.id.tboxDeviceName);
        location= view.findViewById(R.id.tboxLocation);
        ivClose= view.findViewById(R.id.ivClose);

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(deviceName.getText().toString().equals("sophic")){
                    TemporarilyCheckLicense(deviceName.getText().toString(),location.getText().toString());
                }
                else{
                    EditNameDialogListener activity = (EditNameDialogListener) getActivity();
                    LicenceDialogController lc = new LicenceDialogController();
                    lc.CheckLicence(LicenceDialog.this.getActivity(),deviceName.getText().toString(),location.getText().toString(),AppName);
                }


                //getDialog().dismiss();
            }
        });

        ivClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        });


    }

    public  void Close(){
        getDialog().dismiss();
    }

    @Override
    public void onDismiss(final DialogInterface dialog) {
        super.onDismiss(dialog);
        final Activity activity = getActivity();
        if (activity instanceof DialogInterface.OnDismissListener) {
            ((DialogInterface.OnDismissListener) activity).onDismiss(dialog);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
       Window window = getDialog().getWindow();
       window.setBackgroundDrawable( new ColorDrawable(android.graphics.Color.TRANSPARENT));
        //https://guides.codepath.com/android/using-dialogfragment

        /*
        Point size = new Point();
        // Store dimensions of the screen in `size`
        Display display = null;
        if (window != null) {
            display = window.getWindowManager().getDefaultDisplay();
            display.getSize(size);
            // Set the width of the dialog proportional to 75% of the screen width
            window.setLayout((int) (size.x * 0.85), WindowManager.LayoutParams.WRAP_CONTENT);
            window.setGravity(Gravity.CENTER);
        }*/
        SharedPref sharedPref = new SharedPref();

        String licStatus=sharedPref.GetLic(this.getActivity());
        if(licStatus.equals("0")){

        }
        else if (licStatus.equals("1")){
            this.dismiss();
        }
    }
    private void TemporarilyCheckLicense(String deviceName, String location){
        if(deviceName.equals("sophic")){
            if(location.equals("s@ph1cX")){
                SharedPref sharedPref = new SharedPref();
                sharedPref.SaveLic(getActivity().getApplicationContext(),"1");
                dismiss();
            }
            else{
                Toast.makeText(getActivity().getApplicationContext(), "Invalid license", Toast.LENGTH_SHORT).show();
            }
        }
        else{
            Toast.makeText(getActivity().getApplicationContext(), "Invalid license", Toast.LENGTH_SHORT).show();
        }
    }
}