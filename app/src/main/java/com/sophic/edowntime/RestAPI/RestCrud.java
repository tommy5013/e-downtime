package com.sophic.edowntime.RestAPI;

import android.database.Observable;

import com.sophic.edowntime.Model.CoModel;
import com.sophic.edowntime.Model.DownTimeModel;
import com.sophic.edowntime.Model.MachineModel;
import com.sophic.edowntime.Model.PmModel;

import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

/**
 * Created by X13 on 3/4/2017.
 */

public interface RestCrud {
    /*
    @Headers({
            "Content-Type: text/plain",
            "Accept: text/plain"
    })
    */

   // @GET("RestApi/AlertAck/{alertid}")
    //Call<String> setAck(@Path("alertid") String alertid);

    /*
    @FormUrlEncoded
    @POST("RestApi/AddNewPm")
    Call<String> example(
            @Field("alertid") String alertid);
    */

    @POST("RestApi/GetUser")
    @Headers({
            "Content-Type: application/json",
            "Accept: text/plain"
    })

    Call<MachineModel.UserInfo> GetUser (@Body MachineModel.UserInfo post);

    @POST("RestApi/UserLogout")
    @Headers({
            "Content-Type: application/json",
            "Accept: text/plain"
    })

    Call<MachineModel.UserInfo> UserLogout (@Body MachineModel.UserInfo post);

    @POST("RestApi/SaveLine")
    @Headers({
            "Content-Type: application/json",
            "Accept: text/plain"
    })

    Call<String> SaveLine (@Body MachineModel.UserInfo post);


    @POST("RestApi/AddNewPm")
    @Headers({
            "Content-Type: application/json",
            "Accept: text/plain"
    })

    Call<String> AddNewPm (@Body PmModel.ModelNewPm post);



    // @FormUrlEncoded
    //@POST("RestApi/AddNewPm/{alertid}")
    //Call<String> AddNewPm(@Path("alertid") String alertid);

    @POST("RestApi/AlertAck/{alertid}")
    Call<String> setAck(@Path("alertid") String alertid);

    @POST("RestApi/AlertDone")
    @Headers({
            "Content-Type: application/json",
            "Accept: text/plain"
    })
    //Call<MachineModel.AlertDone> setDone (@Body MachineModel.AlertDone post);
    Call<String> setDone (@Body MachineModel.AlertDone post);

    @GET("RestApi/CheckAnswer/{TicketId}")
    Call<String> CheckAnswer(@Path("TicketId") String TicketId);


    @GET("RestApi/GetMachineList")
    Call<List<MachineModel.AllMachineList>> GetMachineList();

    @POST("RestApi/ClosePM/{PmId}")
    Call<String> ClosePM(@Path("PmId") String PmId);

    @POST("RestApi/UpdatePmCheckList/{PmId}")
    Call<String> UpdatePmCheckList(@Path("PmId") String PmId);

    @GET("RestApi/Test")
    Call<String> Test();

    @GET("RestApi/GetPmList/{userid}")
    Call<List<PmModel>> GetPMList(@Path("userid") String userid);

    @GET("RestApi/GetUserLine/{userid}")
    Call<String> GetUserLine(@Path("userid") String userid);

    @GET("RestApi/GetLine")
    Call<List<MachineModel.Line>> GetLine();

    @GET("RestApi/GetLineList")
    Call<List<MachineModel.LineList>> GetLineList();

    @POST("RestApi/GetLine")
    @Headers({
            "Content-Type: application/json",
            "Accept: text/plain"
    })

    Call<List<MachineModel.LineByMachine>> GetLineByMachine (@Body MachineModel.AllMachineModel post);

    //@GET("RestApi/GetLine/{MachineName}")
    //Call<List<MachineModel.LineByMachine>> GetLineByMachine(@Path("MachineName") String MachineName);

    @GET("RestApi/GetMachine/{LineId}")
    Call<List<MachineModel.Machine>> GetMachine(@Path("LineId") String LineId);

    @POST("RestApi/CountAllMachine")
    @Headers({
            "Content-Type: application/json",
            "Accept: text/plain"
    })

    Call<List<MachineModel>> CountMachineAlert (@Body MachineModel.AllMachineModel post);



    @GET("RestApi/GetPmLine")
    Call<List<PmModel.GetPmLine>> GetPmLine();

    @GET("RestApi/GetPmMachine/{LineId}")
    Call<List<PmModel.GetPmMachine>> GetPmMachine(@Path("LineId") String LineId);

    @GET("RestApi/GetPmMachineDetail/{PmId}")
    Call<List<PmModel.GetPmMachineDetail>> GetPmMachineDetail(@Path("PmId") String PmId);

    @GET("RestApi/GetPmCheckList/{PmId}")
    Call<List<PmModel.PmCheckList>> GetPmCheckList(@Path("PmId") String PmId);

    @GET("RestApi/CountAllMachine")
    void listUsers(Callback<List<PmModel.PmCheckList>> cb);


    //CO commands
    @GET("RestApi/GetCoLine")
    Call<List<CoModel.GetCoLine>> GetCoLine();

    @GET("RestApi/GetCoMachine/{LineId}")
    Call<List<CoModel.GetCoMachine>> GetCoMachine(@Path("LineId") String LineId);

    @GET("RestApi/GetCoMachineDetail/{PmId}")
    Call<List<CoModel.GetCoMachineDetail>> GetCoMachineDetail(@Path("PmId") String PmId);

    @GET("RestApi/GetCoCheckList/{PmId}")
    Call<List<CoModel.CoCheckList>> GetCoCheckList(@Path("PmId") String PmId);

    @POST("RestApi/CloseCo/{PmId}")
    Call<String> CloseCo(@Path("PmId") String PmId);

    @POST("RestApi/UpdateCoCheckList/{PmId}")
    Call<String> UpdateCoCheckList(@Path("PmId") String PmId);

    @POST("RestApi/AddNewCo")
    @Headers({
            "Content-Type: application/json",
            "Accept: text/plain"
    })

    Call<String> AddNewCo (@Body CoModel.ModelNewCo post);

    @GET("RestApi/GetDownLine")
    Call<List<DownTimeModel.GetDownLine>> GetDownLine();

    @GET("RestApi/GetDownMachineDetail/{PmId}")
    Call<List<DownTimeModel.GetDownMachineDetail>> GetDownMachineDetail(@Path("PmId") String PmId);

    @POST("RestApi/CloseDownTime")
    @Headers({
            "Content-Type: application/json",
            "Accept: text/plain"
    })
        //Call<MachineModel.AlertDone> setDone (@Body MachineModel.AlertDone post);
    Call<String> CloseDownTime (@Body DownTimeModel.ModelAddCloseDowntime post);

    @POST("RestApi/AddNewDownTime")
    @Headers({
            "Content-Type: application/json",
            "Accept: text/plain"
    })
    Call<String> AddNewDownTime (@Body DownTimeModel.ModelAddCloseDowntime post);

   // @GET("RestApi/GetDownMachineDetail/{PmId}")
    //Call<List<CoModel.GetDownMachineDetail>> GetCoMachineDetail(@Path("PmId") String PmId);

    //@POST("products/{alertid}")
    //Call<String> setAck(@Path("alertid") String alertid, @Body String body);

    //Call<String> setAck(@Path("alertid") String alertid, @Body String body);
    //Call<String> setAck(@Body String alertid);

    //Call<String> setAck(@Body String alertid);
    @GET("RestApi/GetAlert/{MachineType}")
    Call<String> GetAlert(@Path("MachineType") String MachineType);

    @GET("RestApi/GetAlertByPic/{value}")
    Call<String> GetAlertByPic(@Path("value") String value);

    @GET("RestApi/CheckUpdate")
    Call<String> CheckUpdate();

    @Multipart
    @POST("RestApi/PostUserImage/{OtherParam}")
    Call<ResponseBody> PostUserImage(
            @Part MultipartBody.Part file,
            @Path("OtherParam") String OtherParam
    );
}
