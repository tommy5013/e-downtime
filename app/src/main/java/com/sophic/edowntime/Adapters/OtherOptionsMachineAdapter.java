package com.sophic.edowntime.Adapters;

import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.TextView;

import com.sophic.edowntime.Activities.GetLineByMachine;
import com.sophic.edowntime.Model.MachineModel;
import com.sophic.edowntime.Model.PmModel;
import com.sophic.edowntime.R;

import java.util.ArrayList;
import java.util.List;

public class OtherOptionsMachineAdapter extends RecyclerView.Adapter<OtherOptionsMachineAdapter.ViewHolder> {

    private GetLineByMachine parentActivity;
    private List<MachineModel.LineByMachine>tempList;
    private List<PmModel.GetPmLine>tempList2;
    private String modelName;
    private ItemClickListener mClickListener;

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        ConstraintLayout wholeLL;
        TextView textView11,textView12;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            wholeLL = itemView.findViewById(R.id.wholeLL);
            textView11 = itemView.findViewById(R.id.textView11);
            textView12 = itemView.findViewById(R.id.textView12);
            wholeLL.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }

    public OtherOptionsMachineAdapter(GetLineByMachine parentActivity, List<MachineModel.LineByMachine> tempList){
        this.parentActivity = parentActivity;
        this.tempList = tempList;
        modelName = "LineByMachine";
    }
    public OtherOptionsMachineAdapter(List<PmModel.GetPmLine> tempList2, GetLineByMachine parentActivity){
        this.parentActivity = parentActivity;
        this.tempList2 = tempList2;
        modelName = "GetPmLine";
    }

    @NonNull
    @Override
    public OtherOptionsMachineAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_get_line_by_machine,viewGroup,false);
        return new OtherOptionsMachineAdapter.ViewHolder(view);
    }

    @SuppressLint("WrongConstant")
    @Override
    public void onBindViewHolder(@NonNull OtherOptionsMachineAdapter.ViewHolder viewHolder, int i) {

        if(tempList != null){
            final MachineModel.LineByMachine mTempList = tempList.get(i);
            if(mTempList != null){
                if(mTempList.LineName != null){
                    viewHolder.textView11.setText(mTempList.LineName);
                    viewHolder.textView12.setText(String.valueOf(mTempList.ErrorCount));
                }

                ObjectAnimator anim = ObjectAnimator.ofInt(viewHolder.wholeLL,"backgroundColor",Color.RED,Color.parseColor("#a30000"),Color.RED);
                anim.setDuration(1000);
                anim.setEvaluator(new ArgbEvaluator());
                anim.setRepeatMode(Animation.REVERSE);
                anim.setRepeatCount(Animation.INFINITE);
                anim.start();
            }
        }

        if(tempList2 != null){
            final PmModel.GetPmLine mTempList2 = tempList2.get(i);
            if(mTempList2 != null){
                if(mTempList2.LineNameMap != null){
                    viewHolder.textView11.setText(mTempList2.LineNameMap);
                    viewHolder.textView12.setText(String.valueOf(mTempList2.c));
                }

                ObjectAnimator anim = ObjectAnimator.ofInt(viewHolder.wholeLL,"backgroundColor",Color.RED,Color.parseColor("#a30000"),Color.RED);
                anim.setDuration(1000);
                anim.setEvaluator(new ArgbEvaluator());
                anim.setRepeatMode(Animation.REVERSE);
                anim.setRepeatCount(Animation.INFINITE);
                anim.start();
            }
        }

    }

    @Override
    public int getItemCount() {
        return tempList.size();
    }

    public void refreshRecyclerView(){
//        if(modelName != null){
//            switch(modelName){
//                case "LineByMachine":
//                    List<MachineModel.LineByMachine>mTempList = new ArrayList<>();
//                    mTempList.clear();
//                    mTempList.addAll(tempList);
//                    tempList.clear();
//                    tempList.addAll(mTempList);
//                    notifyDataSetChanged();
//                    break;
//                case "GetPmLine":
//                    List<PmModel.GetPmLine>mTempList2 = new ArrayList<>();
//                    mTempList2.clear();
//                    mTempList2.addAll(tempList2);
//                    tempList2.clear();
//                    tempList2.addAll(mTempList2);
//                    notifyDataSetChanged();
//                    break;
//            }
//        }
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }

    // allows clicks events to be caught
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }
}
