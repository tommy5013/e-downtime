package com.sophic.edowntime.Adapters;

import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.TextView;

import com.sophic.edowntime.Activities.DownMainPage;
import com.sophic.edowntime.Activities.PMMainPage;
import com.sophic.edowntime.Model.DownTimeModel;
import com.sophic.edowntime.Model.PmModel;
import com.sophic.edowntime.R;

import java.util.ArrayList;
import java.util.List;

public class DownAdapter extends RecyclerView.Adapter<DownAdapter.ViewHolder> {
    private String modelName;
    private ItemClickListener mClickListener;
    private DownMainPage parentActivity;
    private List<DownTimeModel.GetDownMachineDetail> getDownMachineDetailList;
    private List<DownTimeModel.GetDownLine>  getDownLineList;
    private DownTimeModel.GetDownMachineDetail getDownMachineDetail;
    private String closeId;

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ConstraintLayout wholeLL;
        TextView tvCounter;
        Button btnMachineName;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            wholeLL = itemView.findViewById(R.id.wholeLL);
            tvCounter = itemView.findViewById(R.id.tvCounter);
            btnMachineName = itemView.findViewById(R.id.btnMachineName);
            wholeLL.setOnClickListener(this);
            btnMachineName.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }

    public DownAdapter() {
    }

    public DownAdapter(String closeId,List<DownTimeModel.GetDownMachineDetail> getDownMachineDetailList,DownTimeModel.GetDownMachineDetail getDownMachineDetail) {
        this.closeId = closeId;
        this.getDownMachineDetailList = getDownMachineDetailList;
        this.getDownMachineDetail = getDownMachineDetail;
        modelName = "closeId";
    }

    public DownAdapter(DownMainPage parentActivity, List<DownTimeModel.GetDownMachineDetail> getDownMachineDetailList) {
        this.parentActivity = parentActivity;
        this.getDownMachineDetailList = getDownMachineDetailList;
        modelName = "DownMachineDetailList";
    }
    public DownAdapter(List<DownTimeModel.GetDownLine>  getDownLineList, DownMainPage parentActivity ) {
        this.parentActivity = parentActivity;
        this.getDownLineList = getDownLineList;
        modelName = "DownLineList";
    }

    @NonNull
    @Override
    public DownAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_display_machine, viewGroup, false);
        return new DownAdapter.ViewHolder(view);
    }

    @SuppressLint("WrongConstant")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        if (modelName.equals("DownLineList")) {
            if (getDownLineList != null) {
                final DownTimeModel.GetDownLine getDownLine = getDownLineList.get(i);
                if (getDownLine != null) {
                    if (getDownLine.LineNameMap != null) {
                        viewHolder.btnMachineName.setText(getDownLine.LineNameMap);
                    }
                    if (getDownLine.c != 0) {
                        viewHolder.tvCounter.setText(String.valueOf(getDownLine.c));
                    }
                    //Found here https://stackoverflow.com/questions/43063644/how-to-set-rounded-corner-on-imageview-while-using-animation-on-it
                    //Instead of setbackground if want keep border radius
                    ObjectAnimator anim = ObjectAnimator.ofInt(viewHolder.btnMachineName, "backgroundColor", Color.parseColor("#2433ff"), Color.parseColor("#000ba4"), Color.parseColor("#2433ff"));
//                    ObjectAnimator anim = ObjectAnimator.ofInt(viewHolder.btnMachineName,"backgroundResource",R.drawable.bg_animation_color_light_blue,Color.parseColor("#a30000"),R.drawable.bg_animation_color_light_blue);
                    anim.setDuration(1000);
                    anim.setEvaluator(new ArgbEvaluator());
                    anim.setRepeatMode(Animation.REVERSE);
                    anim.setRepeatCount(Animation.INFINITE);
                    anim.start();
                }
            }
        } else {
            if (getDownMachineDetailList != null) {
                final DownTimeModel.GetDownMachineDetail getDownMachineDetail = getDownMachineDetailList.get(i);
                if (getDownMachineDetail != null) {
                    if (getDownMachineDetail.StationName != null && getDownMachineDetail.Id != null) {
                        String btnName = getDownMachineDetail.StationName + "\n" + getDownMachineDetail.Id;
                        viewHolder.btnMachineName.setText(btnName);
                    }
                    viewHolder.tvCounter.setVisibility(View.GONE);
                }
            }
        }
    }

    @Override
    public int getItemCount() {
        if (modelName.equals("DownLineList")) {
            return getDownLineList.size();
        }
        else{
            return getDownMachineDetailList.size();
        }
    }

    public void refreshRecyclerView() {
        if (modelName != null) {
            switch (modelName) {
                case "DownLineList":
                    List<DownTimeModel.GetDownLine>  getDownLineTempList = new ArrayList<>();
                    getDownLineTempList.clear();
                    getDownLineTempList.addAll(getDownLineList);
                    getDownLineList.clear();
                    getDownLineList.addAll(getDownLineTempList);
                    notifyDataSetChanged();
                    break;
                case "DownMachineDetailList":
                    List<DownTimeModel.GetDownMachineDetail> getDownMachineDetailTempList = new ArrayList<>();
                    getDownMachineDetailTempList.clear();
                    getDownMachineDetailTempList.addAll(getDownMachineDetailList);
                    getDownMachineDetailList.clear();
                    getDownMachineDetailList.addAll(getDownMachineDetailTempList);
                    notifyDataSetChanged();
                    break;
                case "closeId":
                    getDownMachineDetailTempList = new ArrayList<>();
                    getDownMachineDetailTempList.clear();
                    getDownMachineDetailTempList.addAll(getDownMachineDetailList);
                    getDownMachineDetailTempList.remove(getDownMachineDetail);
                    getDownMachineDetailList.clear();
                    getDownMachineDetailList.addAll(getDownMachineDetailTempList);
                    notifyDataSetChanged();
                    break;
            }
        }
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }

    // allows clicks events to be caught
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }
}