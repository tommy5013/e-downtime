package com.sophic.edowntime.Adapters;

import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.TextView;

import com.sophic.edowntime.Activities.COMainPage;
import com.sophic.edowntime.Activities.GetLineByMachine;
import com.sophic.edowntime.Activities.PMMainPage;
import com.sophic.edowntime.Model.CoModel;
import com.sophic.edowntime.Model.PmModel;
import com.sophic.edowntime.R;

import java.util.ArrayList;
import java.util.List;

public class PMAdapter extends RecyclerView.Adapter<PMAdapter.ViewHolder> {
    private String modelName;
    private ItemClickListener mClickListener;
    private PMMainPage parentActivity;
    private List<PmModel.GetPmMachine> getPmMachineList;
    private List<PmModel.GetPmLine>  getPmLineList;
    private String closeId;
    private PmModel.GetPmMachine getPmMachine;

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        ConstraintLayout wholeLL;
        TextView tvCounter;
        Button btnMachineName;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            wholeLL = itemView.findViewById(R.id.wholeLL);
            tvCounter = itemView.findViewById(R.id.tvCounter);
            btnMachineName = itemView.findViewById(R.id.btnMachineName);
            wholeLL.setOnClickListener(this);
            btnMachineName.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }

    public PMAdapter() {}

    public PMAdapter(String closeId,List<PmModel.GetPmMachine> getPmMachineList,PmModel.GetPmMachine getPmMachine) {
        this.closeId = closeId;
        this.getPmMachineList = getPmMachineList;
        this.getPmMachine = getPmMachine;
        modelName = "closeId";
    }

    public PMAdapter(PMMainPage parentActivity, List<PmModel.GetPmMachine> getPmMachineList) {
        this.parentActivity = parentActivity;
        this.getPmMachineList = getPmMachineList;
        modelName = "PmMachineList";
    }
    public PMAdapter(List<PmModel.GetPmLine>  getPmLineList,PMMainPage parentActivity ) {
        this.parentActivity = parentActivity;
        this.getPmLineList = getPmLineList;
        modelName = "PmLineList";
    }

    @NonNull
    @Override
    public PMAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_display_machine, viewGroup, false);
        return new PMAdapter.ViewHolder(view);
    }

    @SuppressLint("WrongConstant")
    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        if (modelName.equals("PmLineList")) {
            if (getPmLineList != null) {
                final PmModel.GetPmLine getPmLine = getPmLineList.get(i);
                if (getPmLine != null) {
                    if (getPmLine.LineNameMap != null) {
                        viewHolder.btnMachineName.setText(getPmLine.LineNameMap);
                    }
                    if (getPmLine.c != 0) {
                        viewHolder.tvCounter.setText(String.valueOf(getPmLine.c));
                    }
                    //Found here https://stackoverflow.com/questions/43063644/how-to-set-rounded-corner-on-imageview-while-using-animation-on-it
                    //Instead of setbackground if want keep border radius
                    ObjectAnimator anim = ObjectAnimator.ofInt(viewHolder.btnMachineName, "backgroundColor", Color.parseColor("#2433ff"), Color.parseColor("#000ba4"), Color.parseColor("#2433ff"));
//                    ObjectAnimator anim = ObjectAnimator.ofInt(viewHolder.btnMachineName,"backgroundResource",R.drawable.bg_animation_color_light_blue,Color.parseColor("#a30000"),R.drawable.bg_animation_color_light_blue);
                    anim.setDuration(1000);
                    anim.setEvaluator(new ArgbEvaluator());
                    anim.setRepeatMode(Animation.REVERSE);
                    anim.setRepeatCount(Animation.INFINITE);
                    anim.start();
                }
            }
        } else {
            if (getPmMachineList != null) {
                final PmModel.GetPmMachine getPmMachine = getPmMachineList.get(i);
                if (getPmMachine != null) {
                    if (getPmMachine.StationName != null && getPmMachine.Id != 0) {
                        String btnName = getPmMachine.StationName + "\n" + getPmMachine.Id;
                        viewHolder.btnMachineName.setText(btnName);
                    }
                    viewHolder.tvCounter.setVisibility(View.GONE);
                }
            }
        }
    }

    @Override
    public int getItemCount() {
        if (modelName.equals("PmLineList")) {
            return getPmLineList.size();
        }
        else{
            return getPmMachineList.size();
        }
    }

    public void refreshRecyclerView() {
        if (modelName != null) {
            switch (modelName) {
                case "PmLineList":
                    List<PmModel.GetPmLine>  getPmLineTempList = new ArrayList<>();
                    getPmLineTempList.clear();
                    getPmLineTempList.addAll(getPmLineList);
                    getPmLineList.clear();
                    getPmLineList.addAll(getPmLineTempList);
                    notifyDataSetChanged();
                    break;
                case "PmMachineList":
                    List<PmModel.GetPmMachine> getPmMachineTempList = new ArrayList<>();
                    getPmMachineTempList.clear();
                    getPmMachineTempList.addAll(getPmMachineList);
                    getPmMachineList.clear();
                    getPmMachineList.addAll(getPmMachineTempList);
                    notifyDataSetChanged();
                    break;

                case "closeId":
                    getPmMachineTempList = new ArrayList<>();
                    getPmMachineTempList.clear();
                    getPmMachineTempList.addAll(getPmMachineList);
                    getPmMachineTempList.remove(getPmMachine);
                    getPmMachineList.clear();
                    getPmMachineList.addAll(getPmMachineTempList);
                    notifyDataSetChanged();
                    break;
            }
        }
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }

    // allows clicks events to be caught
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }
}