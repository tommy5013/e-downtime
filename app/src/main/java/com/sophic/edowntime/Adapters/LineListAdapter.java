package com.sophic.edowntime.Adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.sophic.edowntime.Model.MyConfig;
import com.sophic.edowntime.R;

import java.util.List;

/**
 * Created by X-PC on 22/5/2018.
 */

public class LineListAdapter extends BaseAdapter{

    private Context context;
    private List<String> mobileValues;


    public LineListAdapter(Context context, List<String> mobileValues) {
        this.context = context;
        this.mobileValues = mobileValues;
    }

    public void Update(List<String> mobileValues){
        //this.mobileValues = mobileValues;
        notifyDataSetChanged();

    }

    @Override
    public int getCount() {
        return mobileValues.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup viewGroup) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View gridView;

        if (convertView == null) {

            gridView = new View(context);

            // get layout from mobile.xml
            gridView = inflater.inflate(R.layout.lineadapter, null);

            // set value into textview
                final CheckBox btn1 = (CheckBox) gridView.findViewById(R.id.btnGrid);

            String[] _list = mobileValues.get(position).split(",");

            final String Linename = _list[0];
            String PicList = _list[1];

            if(MyConfig.UserLineList.contains(Linename)){
               // MyConfig.LineList.add(Linename);
               // btn1.setChecked(true);
            }
            else {
              //  btn1.setChecked(false);
            }

            btn1.setText(Linename);
            btn1.setTextColor(Color.WHITE);

            //btn1.setText(mobileValues.get(position));
            //btn1.setTextColor(Color.WHITE);


            if (PicList.equals("0")|| PicList.equals("null")||PicList==null)
                btn1.setBackgroundResource(R.drawable.checkboxred);
          /*  if (btn1.getText().toString().equalsIgnoreCase("BK5.2"))
                btn1.setBackgroundResource(R.drawable.checkboxgreen);
            if (btn1.getText().toString().equalsIgnoreCase("BK6.1"))
                btn1.setBackgroundResource(R.drawable.checkboxgreen);*/


            btn1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        if(!MyConfig.LineList.contains(Linename)){

                            MyConfig.LineList.add(Linename);
                        }

                    }else{
                        if(MyConfig.LineList.contains(Linename))
                            MyConfig.LineList.remove(Linename);
                    }

                    String lineName = "";
                    for(String e : MyConfig.LineList){
                        lineName += "," + e;
                    }

                    //Toast.makeText(context, lineName, Toast.LENGTH_LONG).show();
                }
            });

            String lineName = "";
            for(String e : MyConfig.LineList){
                lineName += "," + e;
            }

            //Toast.makeText(context, lineName, Toast.LENGTH_LONG).show();


        } else {
            gridView = (View) convertView;
        }

        return gridView;
    }

}
