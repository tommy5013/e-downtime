package com.sophic.edowntime.Adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.sophic.edowntime.Activities.table_display;
import com.sophic.edowntime.Model.Machine;
import com.sophic.edowntime.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class TableDisplayAdapter extends RecyclerView.Adapter<TableDisplayAdapter.ViewHolder> {
    public String TAG = TableDisplayAdapter.class.getName();
    private table_display parentActivity;
    private List<Machine> machinesList = new ArrayList<>();
    private ItemClickListener mClickListener;
    private SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    private int position;
    private String getnewalert;

    public TableDisplayAdapter(table_display parentActivity,List<Machine> machinesList,String getnewalert){
        this.parentActivity = parentActivity;
        this.machinesList = machinesList;
        this.getnewalert = getnewalert;
    }

    @NonNull
    @Override
    public TableDisplayAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_table_display,viewGroup,false);
        return new TableDisplayAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        if(machinesList != null){
            Machine machine = machinesList.get(i);
            if(machine != null){
                if(machine.MachineType != null){
                    if(machine.MachineType.equals("CPK")){
                        if(machine.productName != null){
                            viewHolder.tvLine.setText(machine.productName);
                        }
                        else{
                            viewHolder.tvLine.setText("Null");
                        }
                        if(!machine.WaitingTime.equals("null")){
                            String date = dateFuntion(machine.WaitingTime);
                            Date currentDate = new Date();
                            try {
                                Date StartDate = formatter.parse(date);
                                long diff = currentDate.getTime() - StartDate.getTime();
                                long seconds = diff / 1000;
                                long minutes = seconds / 60;
                                viewHolder.tvWaitingTime.setText(minutes+"(min)");

                            } catch (ParseException e) {
                                e.printStackTrace();
                                Log.e(TAG,e.getMessage());
                            }
                        }
                        else{
                            if(!machine.TimeIn.equals("null")){
                                String date = dateFuntion(machine.TimeIn);
                                Date currentDate = new Date();
                                try {
                                    Date StartDate = formatter.parse(date);
                                    long diff = currentDate.getTime() - StartDate.getTime();
                                    long seconds = diff / 1000;
                                    long minutes = seconds / 60;
                                    viewHolder.tvWaitingTime.setText(minutes+"(min)");

                                } catch (ParseException e) {
                                    e.printStackTrace();
                                    Log.e(TAG,e.getMessage());
                                }
                            }
                        }
                        if(machine.MachineType != null){
                            viewHolder.tvMachine.setText(machine.MachineType);
                        }
                        else{
                            viewHolder.tvMachine.setText("Null");
                        }
                        if(machine.Status != null){
                            viewHolder.tvModule.setText("");
                        }
                        else{
                            viewHolder.tvModule.setText("Null");
                        }
                        if(machine.Cpk != null){
                            viewHolder.itemDetail.setText(machine.Cpk);
                        }
                        else{
                            viewHolder.itemDetail.setText("Null");
                        }
                    }
                    //////////////////////NOT CPK TYPE MACHINE//////////////////////
                    else{
                        if(machine.DataMachineName != null){
                            viewHolder.tvMachine.setText(machine.DataMachineName);
                        }
                        if(machine.DataLineName != null){
                            viewHolder.tvLine.setText(machine.DataLineName);
                        }
                        if(machine.ModuleNo != null && !TextUtils.isEmpty(machine.ModuleNo)){
                            viewHolder.tvModule.setText(machine.ModuleNo);
                        }
                        else{
                            viewHolder.tvModule.setVisibility(View.GONE);
                        }
                        if(!machine.WaitingTime.equals("null")){
                            String date = dateFuntion(machine.WaitingTime);
                            Date currentDate = new Date();
                            try {
                                Date StartDate = formatter.parse(date);
                                long diff = currentDate.getTime() - StartDate.getTime();
                                long seconds = diff / 1000;
                                long minutes = seconds / 60;
                                viewHolder.tvWaitingTime.setText(minutes+"(min)");

                            } catch (ParseException e) {
                                e.printStackTrace();
                                Log.e(TAG,e.getMessage());
                            }
                        }
                        else{
                            if(!machine.TimeIn.equals("null")){
                                String date = dateFuntion(machine.TimeIn);
                                Date currentDate = new Date();
                                try {
                                    Date StartDate = formatter.parse(date);
                                    long diff = currentDate.getTime() - StartDate.getTime();
                                    long seconds = diff / 1000;
                                    long minutes = seconds / 60;
                                    viewHolder.tvWaitingTime.setText(minutes+"(min)");

                                } catch (ParseException e) {
                                    e.printStackTrace();
                                    Log.e(TAG,e.getMessage());
                                }
                            }
                        }
                        if(machine.ErrorCode != null){
                            viewHolder.itemDetail.setText("Error Code: "+machine.ErrorCode);
                        }
                    }
                }
            }
        }
    }


    @Override
    public int getItemCount() {
        return machinesList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView tvMachine,tvLine,tvModule,tvWaitingTime,itemDetail;
        Button acceptBTN,infoBTN;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvMachine = itemView.findViewById(R.id.tvMachine);
            tvLine = itemView.findViewById(R.id.tvLine);
            tvModule = itemView.findViewById(R.id.tvModule);
            tvWaitingTime = itemView.findViewById(R.id.tvWaitingTime);
            itemDetail = itemView.findViewById(R.id.itemDetail);
            acceptBTN = itemView.findViewById(R.id.acceptBTN);
            infoBTN = itemView.findViewById(R.id.infoBTN);
            acceptBTN.setOnClickListener(this);
            infoBTN.setOnClickListener(this);

            if(getnewalert.equals("false")){
                acceptBTN.setText("Done");
            }
            else{
                acceptBTN.setVisibility(View.VISIBLE);
            }
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition(),machinesList.get(getAdapterPosition()));
        }
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position, Machine machine);
    }

    // allows clicks events to be caught
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    private String dateFuntion(String date){
        String finalDate = "-";
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        Date myDate = null;
        try {
            myDate = dateFormat.parse(date);
            SimpleDateFormat timeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            finalDate = timeFormat.format(myDate);

            Log.e(TAG,"Final data : " + finalDate);

        } catch (ParseException e) {
            e.printStackTrace();
            Log.e(TAG,e.getMessage());
        }
        return finalDate;
    }

    public void setPosition(int position){
        this.position = position;
    }
    public int getPosition(){
        return position;
    }

    public void updateList(){
        List<Machine>tempList = new ArrayList<>();
        tempList.clear();
        machinesList.remove(getPosition());
        tempList.addAll(machinesList);
        machinesList.clear();
        machinesList.addAll(tempList);
        notifyDataSetChanged();
    }
}
