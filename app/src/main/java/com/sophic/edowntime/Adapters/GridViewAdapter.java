package com.sophic.edowntime.Adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.sophic.edowntime.Activities.ListAllMachine;
import com.sophic.edowntime.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by MQ on 2016/11/11.
 */

public class GridViewAdapter extends BaseAdapter {
    private List<String> dataList;
    private ListAllMachine parentActivity;

    public GridViewAdapter(List<String> datas, int page, ListAllMachine parentActivity) {
        dataList = new ArrayList<>();
        //start end分别代表要显示的数组在总数据List中的开始和结束位置
        int start = page * ListAllMachine.item_grid_num;
        int end = start + ListAllMachine.item_grid_num;
        while ((start < datas.size()) && (start < end)) {
            dataList.add(datas.get(start));
            start++;
        }
        this.parentActivity = parentActivity;
    }

    @Override
    public int getCount() {
        return dataList.size();
    }

    @Override
    public Object getItem(int i) {
        return dataList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View itemView, ViewGroup viewGroup) {
        ViewHolder mHolder;
        if (itemView == null) {
            mHolder = new ViewHolder();
            itemView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_gridview, viewGroup, false);
            mHolder.iv_img = (ImageView) itemView.findViewById(R.id.iv_img);
            mHolder.tv_text = (TextView) itemView.findViewById(R.id.tv_text);
            mHolder.wholeLL = (LinearLayout) itemView.findViewById(R.id.wholeLL);
            itemView.setTag(mHolder);
        } else {
            mHolder = (ViewHolder) itemView.getTag();
        }
        final String machineType = dataList.get(i);
        if (machineType != null) {
//            mHolder.iv_img.setImageResource(R.drawable.icon_machine_64);
            mHolder.tv_text.setText(machineType);
            parentActivity.SetImage(machineType,mHolder.iv_img);
        }

        mHolder.wholeLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                parentActivity.IntentOtherMachinePage(machineType,v);
            }
        });
        return itemView;
    }

    private class ViewHolder {
        private ImageView iv_img;
        private TextView tv_text;
        private LinearLayout wholeLL;
    }
}
