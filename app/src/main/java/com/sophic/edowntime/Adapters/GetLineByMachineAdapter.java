package com.sophic.edowntime.Adapters;

import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import com.sophic.edowntime.Activities.COMainPage;
import com.sophic.edowntime.Activities.GetLineByMachine;
import com.sophic.edowntime.Activities.LineSelectActivity;
import com.sophic.edowntime.Model.CoModel;
import com.sophic.edowntime.Model.MachineModel;
import com.sophic.edowntime.Model.MyConfig;
import com.sophic.edowntime.Model.PmModel;
import com.sophic.edowntime.R;

import java.util.ArrayList;
import java.util.List;

public class GetLineByMachineAdapter extends RecyclerView.Adapter<GetLineByMachineAdapter.ViewHolder> {

    private GetLineByMachine parentActivity;
    private List<MachineModel.LineByMachine>tempList;
    private List<PmModel.GetPmLine>tempList2;
    private String modelName;
    private ItemClickListener mClickListener;
    private List<CoModel.GetCoLine> COMachineList;
    private List<CoModel.GetCoMachine> getCoMachineList;
    private COMainPage coMainPage;
    private String closeId;
    private CoModel.GetCoMachine getCoMachine;

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        ConstraintLayout wholeLL;
        TextView tvCounter;
        Button btnMachineName;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            wholeLL = itemView.findViewById(R.id.wholeLL);
            tvCounter = itemView.findViewById(R.id.tvCounter);
            btnMachineName = itemView.findViewById(R.id.btnMachineName);
            wholeLL.setOnClickListener(this);
            btnMachineName.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }
    public GetLineByMachineAdapter(){};

    public GetLineByMachineAdapter(String closeId, List<CoModel.GetCoMachine> getCoMachineList, CoModel.GetCoMachine getCoMachine){
        this.closeId = closeId;
        this.getCoMachineList = getCoMachineList;
        this.getCoMachine = getCoMachine;
        modelName = "closeId";
    }

    public GetLineByMachineAdapter(GetLineByMachine parentActivity, List<MachineModel.LineByMachine> tempList){
        this.parentActivity = parentActivity;
        this.tempList = tempList;
        modelName = "LineByMachine";
    }
    public GetLineByMachineAdapter(List<PmModel.GetPmLine> tempList2, GetLineByMachine parentActivity){
        this.parentActivity = parentActivity;
        this.tempList2 = tempList2;
        modelName = "GetPmLine";
    }
    public GetLineByMachineAdapter(List<CoModel.GetCoLine> COMachineList, COMainPage coMainPage){
        this.coMainPage = coMainPage;
        this.COMachineList = COMachineList;
        modelName = "CO";
    }
    public GetLineByMachineAdapter(COMainPage coMainPage, List<CoModel.GetCoMachine> getCoMachineList){
        this.coMainPage = coMainPage;
        this.getCoMachineList = getCoMachineList;
        modelName = "SUBCO";
    }

    @NonNull
    @Override
    public GetLineByMachineAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_display_machine,viewGroup,false);
        return new GetLineByMachineAdapter.ViewHolder(view);
    }

    @SuppressLint("WrongConstant")
    @Override
    public void onBindViewHolder(@NonNull GetLineByMachineAdapter.ViewHolder viewHolder, int i) {
        if(modelName.equals("CO")){
            if(COMachineList != null){
                final CoModel.GetCoLine getCoLineItem = COMachineList.get(i);
                if(getCoLineItem != null){
                    if(getCoLineItem.LineNameMap != null){
                        viewHolder.btnMachineName.setText(getCoLineItem.LineNameMap);
                    }
                    if(getCoLineItem.c != 0){
                        viewHolder.tvCounter.setText(String.valueOf(getCoLineItem.c));
                    }
                    //Found here https://stackoverflow.com/questions/43063644/how-to-set-rounded-corner-on-imageview-while-using-animation-on-it
                    //Instead of setbackground if want keep border radius
                    ObjectAnimator anim = ObjectAnimator.ofInt(viewHolder.btnMachineName,"backgroundColor",Color.parseColor("#2433ff"),Color.parseColor("#000ba4"),Color.parseColor("#2433ff"));
//                    ObjectAnimator anim = ObjectAnimator.ofInt(viewHolder.btnMachineName,"backgroundResource",R.drawable.bg_animation_color_light_blue,Color.parseColor("#a30000"),R.drawable.bg_animation_color_light_blue);
                    anim.setDuration(1000);
                    anim.setEvaluator(new ArgbEvaluator());
                    anim.setRepeatMode(Animation.REVERSE);
                    anim.setRepeatCount(Animation.INFINITE);
                    anim.start();
                }
            }
        }
        else if(modelName.equals("SUBCO")){
            if(getCoMachineList != null){
                final CoModel.GetCoMachine getCoMachine = getCoMachineList.get(i);
                if(getCoMachine != null){
                    if(getCoMachine.StationName != null && getCoMachine.Id != 0){
                        String btnName = getCoMachine.StationName + "\n" + getCoMachine.Id ;
                        viewHolder.btnMachineName.setText(btnName);
                    }
                    viewHolder.tvCounter.setVisibility(View.GONE);
                }
            }
        }
        else{
            if(tempList != null){
                final MachineModel.LineByMachine mTempList = tempList.get(i);
                if(mTempList != null){
                    if(mTempList.LineName != null){
                        viewHolder.btnMachineName.setText(mTempList.LineName);
                        viewHolder.tvCounter.setText(String.valueOf(mTempList.ErrorCount));
                    }

                    ObjectAnimator anim = ObjectAnimator.ofInt(viewHolder.btnMachineName,"backgroundColor",Color.RED,Color.parseColor("#a30000"),Color.RED);
                    anim.setDuration(1000);
                    anim.setEvaluator(new ArgbEvaluator());
                    anim.setRepeatMode(Animation.REVERSE);
                    anim.setRepeatCount(Animation.INFINITE);
                    anim.start();
                }
            }

            if(tempList2 != null){
                final PmModel.GetPmLine mTempList2 = tempList2.get(i);
                if(mTempList2 != null){
                    if(mTempList2.LineNameMap != null){
                        viewHolder.btnMachineName.setText(mTempList2.LineNameMap);
                        viewHolder.tvCounter.setText(String.valueOf(mTempList2.c));
                    }

                    ObjectAnimator anim = ObjectAnimator.ofInt(viewHolder.btnMachineName,"backgroundColor",Color.RED,Color.parseColor("#a30000"),Color.RED);
                    anim.setDuration(1000);
                    anim.setEvaluator(new ArgbEvaluator());
                    anim.setRepeatMode(Animation.REVERSE);
                    anim.setRepeatCount(Animation.INFINITE);
                    anim.start();
                }
            }

        }

    }

    @Override
    public int getItemCount() {
        if(modelName.equals("CO")){
            return COMachineList.size();
        }
        else if(modelName.equals("SUBCO")){
            return getCoMachineList.size();
        }
        else{
            return tempList.size();
        }
    }

    public void refreshRecyclerView(){
        if(modelName != null){
            switch(modelName){
                case "LineByMachine":
                    List<MachineModel.LineByMachine>mTempList = new ArrayList<>();
                    mTempList.clear();
                    mTempList.addAll(tempList);
                    tempList.clear();
                    tempList.addAll(mTempList);
                    notifyDataSetChanged();
                    break;
                case "GetPmLine":
                    List<PmModel.GetPmLine>mTempList2 = new ArrayList<>();
                    mTempList2.clear();
                    mTempList2.addAll(tempList2);
                    tempList2.clear();
                    tempList2.addAll(mTempList2);
                    notifyDataSetChanged();
                    break;

                case "CO":
                    List<CoModel.GetCoLine> COMachineTempList = new ArrayList<>();
                    COMachineTempList.clear();
                    COMachineTempList.addAll(COMachineList);
                    COMachineList.clear();
                    COMachineList.addAll(COMachineTempList);
                    notifyDataSetChanged();
                    break;

                case"SUBCO":
                    List<CoModel.GetCoMachine> getCoMachineTempList = new ArrayList<>();
                    getCoMachineTempList.clear();
                    getCoMachineTempList.addAll(getCoMachineList);
                    getCoMachineList.clear();
                    getCoMachineList.addAll(getCoMachineTempList);
                    notifyDataSetChanged();
                    break;

                case "closeId":
                    getCoMachineTempList = new ArrayList<>();
                    getCoMachineTempList.clear();
                    getCoMachineTempList.addAll(getCoMachineList);
                    getCoMachineTempList.remove(getCoMachine);
                    getCoMachineList.clear();
                    getCoMachineList.addAll(getCoMachineTempList);
                    notifyDataSetChanged();
                    break;
            }
        }
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position);
    }

    // allows clicks events to be caught
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }
}
