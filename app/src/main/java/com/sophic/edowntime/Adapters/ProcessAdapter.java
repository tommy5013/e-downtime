package com.sophic.edowntime.Adapters;

import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.sophic.edowntime.Activities.ListAllMachine;
import com.sophic.edowntime.Model.ProcessModel;
import com.sophic.edowntime.R;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class ProcessAdapter extends RecyclerView.Adapter<ProcessAdapter.ViewHolder> {

    private AppCompatActivity parentActivity;
    private ItemClickListener mClickListener;
    private List<ProcessModel> allProcessList;
    private DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        ConstraintLayout wholeLL;
        TextView processName,processClassName,processLastDate,processStatus;
        Button btnStop;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            wholeLL = itemView.findViewById(R.id.wholeLL);
            processName = itemView.findViewById(R.id.processName);
            processClassName = itemView.findViewById(R.id.processClassName);
            processLastDate = itemView.findViewById(R.id.processLastDate);
            processStatus = itemView.findViewById(R.id.processStatus);
            btnStop = itemView.findViewById(R.id.btnStop);
            btnStop.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition(),processName.getText().toString());
        }
    }

    public ProcessAdapter(AppCompatActivity parentActivity, List<ProcessModel> allProcessList){
        this.parentActivity = parentActivity;
        this.allProcessList = allProcessList;
    }

    @NonNull
    @Override
    public ProcessAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_display_process,viewGroup,false);
        return new ProcessAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ProcessAdapter.ViewHolder viewHolder, int i) {

        if(allProcessList != null){
            final ProcessModel processModel = allProcessList.get(i);
            if(processModel != null){
                if(processModel.Name != null){
                    viewHolder.processName.setText(processModel.Name);
                }
                if(processModel.ClassName != null){
                    viewHolder.processClassName.setText(processModel.ClassName);

                }
                if(processModel.LastRunningDate != null){
                    String strDate = dateFormat.format(processModel.LastRunningDate);
                    viewHolder.processLastDate.setText(strDate);

                }
                if(processModel.Status != null){
                    if(processModel.Status.equals(new ProcessModel().STATUS_RUNNING)){
                        viewHolder.btnStop.setText(new ProcessModel().STATUS_STOP);
                    }
                    else{
                        viewHolder.btnStop.setText(new ProcessModel().STATUS_RUNNING);
                    }
                    viewHolder.processStatus.setText(processModel.Status);

                }
            }
        }
    }

    @Override
    public int getItemCount() {
        return allProcessList.size();
    }


    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position, String processName);
    }

    // allows clicks events to be caught
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    public void refreshRecycleView(List<ProcessModel> tempProcessModel){
        allProcessList = new ArrayList<>();
        allProcessList.addAll(tempProcessModel);
        notifyDataSetChanged();
    }
}
