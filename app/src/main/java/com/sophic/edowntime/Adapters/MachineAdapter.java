package com.sophic.edowntime.Adapters;

import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.sophic.edowntime.Activities.ListAllMachine;
import com.sophic.edowntime.Model.MachineModel;
import com.sophic.edowntime.Model.MyConfig;
import com.sophic.edowntime.R;

import java.util.List;

public class MachineAdapter extends RecyclerView.Adapter<MachineAdapter.ViewHolder> {

    private ListAllMachine parentActivity;
    private ItemClickListener mClickListener;
    private List<MachineModel> allMachineList;

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        ConstraintLayout wholeLL;
        TextView tvCounter;
        Button btnMachineName;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            wholeLL = itemView.findViewById(R.id.wholeLL);
            tvCounter = itemView.findViewById(R.id.tvCounter);
            btnMachineName = itemView.findViewById(R.id.btnMachineName);
            btnMachineName.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition(),btnMachineName.getText().toString());
        }
    }

    public MachineAdapter(ListAllMachine parentActivity,List<MachineModel> allMachineList){
        this.parentActivity = parentActivity;
        this.allMachineList = allMachineList;
    }

    @NonNull
    @Override
    public MachineAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.adapter_display_machine,viewGroup,false);
        return new MachineAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MachineAdapter.ViewHolder viewHolder, int i) {

        if(allMachineList != null){
            final MachineModel allMachine = allMachineList.get(i);
            if(allMachine != null){
                if(allMachine.MachineType != null){
                    viewHolder.btnMachineName.setText(allMachine.MachineType);
                }
                if(allMachine.ErrorCount != null){
                    viewHolder.tvCounter.setText(allMachine.ErrorCount);
                    if(Integer.parseInt(allMachine.ErrorCount) <= 0){
                        viewHolder.btnMachineName.setBackground(parentActivity.getResources().getDrawable(R.drawable.button_border_grey));
                    }
                }
            }
        }
    }

    @Override
    public int getItemCount() {
        return allMachineList.size();
    }


    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position,String machineType);
    }

    // allows clicks events to be caught
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

//    private void DisplayCount(ViewHolder viewHolder, String mTempList){
//
//        switch(mTempList){
//            case "NXT":
//                viewHolder.tvCounter.setText(String.valueOf(myConfig.getNxtCurrent()));
//                break;
//
//            case "SPI":
//                viewHolder.tvCounter.setText(String.valueOf(myConfig.getSPICurrent()));
//                break;
//
//            case "DEK":
//                viewHolder.tvCounter.setText(String.valueOf(myConfig.getDEKCurrent()));
//                break;
//
//            case "AOI":
//                if(myConfig != null){
//                    if(myConfig.getAOICurrent() != 0){
//                        viewHolder.tvCounter.setText(String.valueOf(myConfig.getAOICurrent()));
//                    }
//                }
//                break;
//
//            case "BTU":
//                viewHolder.tvCounter.setText(String.valueOf(myConfig.getBTUCurrent()));
//                break;
//
//            case "EB":
//                viewHolder.tvCounter.setText(String.valueOf(myConfig.getEBCurrent()));
//                break;
//
//            case "CPK":
//                viewHolder.tvCounter.setText(String.valueOf(myConfig.getCPKCurrent()));
//                break;
//
//            case "CAML":
//                viewHolder.tvCounter.setText(String.valueOf(myConfig.getCMLCurrent()));
//                break;
//
//            case "CAMUL":
//                viewHolder.tvCounter.setText(String.valueOf(myConfig.getCAMULCurrent()));
//                break;
//        }
//    }
}
