package com.sophic.edowntime.Adapters;

import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Toast;

import com.sophic.edowntime.Activities.LineSelectActivity;
import com.sophic.edowntime.Model.MachineModel;
import com.sophic.edowntime.Model.MyConfig;
import com.sophic.edowntime.R;

import java.util.List;

public class LineSelectAdapter extends RecyclerView.Adapter<LineSelectAdapter.ViewHolder> {

    private LineSelectActivity parentActivity;
    private List<MachineModel.LineList>tempList;

    public class ViewHolder extends RecyclerView.ViewHolder{
        CheckBox checkBoxBTN;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            checkBoxBTN = itemView.findViewById(R.id.btnGrid);
        }
    }

    public LineSelectAdapter(LineSelectActivity parentActivity, List<MachineModel.LineList> tempList){
        this.parentActivity = parentActivity;
        this.tempList = tempList;
    }

    @NonNull
    @Override
    public LineSelectAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.lineadapter,viewGroup,false);
        return new LineSelectAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull LineSelectAdapter.ViewHolder viewHolder, int i) {
        final MachineModel.LineList mTempList = tempList.get(i);
        if(mTempList != null){
            if(mTempList.LineName != null && mTempList.PicCount!= null){
                String mName = mTempList.LineName+","+mTempList.PicCount;
                viewHolder.checkBoxBTN.setText(mTempList.LineName);
                viewHolder.checkBoxBTN.setTextColor(Color.WHITE);

                if (mTempList.PicCount.equals("0")|| mTempList.PicCount.equals("null")||mTempList.PicCount==null){
                    viewHolder.checkBoxBTN.setBackgroundResource(R.drawable.checkboxred);
                }
                viewHolder.checkBoxBTN.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        if (isChecked) {
                            if(!MyConfig.LineList.contains(mTempList.LineName)){

                                MyConfig.LineList.add(mTempList.LineName);
                            }

                        }else{
                            if(MyConfig.LineList.contains(mTempList.LineName))
                                MyConfig.LineList.remove(mTempList.LineName);
                        }

//                        Toast.makeText(parentActivity, String.valueOf(MyConfig.LineList.size()), Toast.LENGTH_SHORT).show();
                    }
                });
            }
        }
    }

    @Override
    public int getItemCount() {
        return tempList.size();
    }
}
