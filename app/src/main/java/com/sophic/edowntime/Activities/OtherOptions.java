package com.sophic.edowntime.Activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.sophic.edowntime.Helper.AutoLogout;
import com.sophic.edowntime.Model.MyConfig;
import com.sophic.edowntime.R;

public class OtherOptions extends Activity implements  android.view.View.OnClickListener  {

    Button btnCO,btnPM,btnBack,btnDownTime;
    MyConfig myConfig = MyConfig.getInstance();
    AutoLogout autoLogout;

    @Override
    public void onResume()
    {
        super.onResume();
        autoLogout.start();
    }

    public void onDestroy() {
        autoLogout.stop();
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
        autoLogout.stop();
    }

    @Override
    public void onUserInteraction()
    {
        autoLogout.reset();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_other_options);

        btnPM = (Button) findViewById(R.id.btnPM);
        btnCO = (Button) findViewById(R.id.btnCO);
        btnBack= (Button) findViewById(R.id.btnBack);
        btnDownTime= (Button) findViewById(R.id.btnDownTime);
        //btnCamera = (Button) findViewById(R.id.btnCamera);
        btnPM.setOnClickListener(this);
        btnCO.setOnClickListener(this);
        btnDownTime.setOnClickListener(this);
        btnBack.setOnClickListener(this);
        autoLogout = new AutoLogout(this);
        autoLogout.start();
    }


    @Override
    public void onClick(View view) {
        Intent intent;
        switch (view.getId()) {
            case R.id.btnPM:
                //myConfig.myTimer.cancel();
                intent = new Intent(OtherOptions.this, PMMainPage.class);
                intent.putExtra("Type","LinePage");
                myConfig.setCurrentPage("PmLine");
                startActivity(intent);
                break;

            case R.id.btnCO:
                //myConfig.myTimer.cancel();
                intent = new Intent(OtherOptions.this, COMainPage.class);
                intent.putExtra("Type","LinePage");
                myConfig.setCoCurrentPage("CoLine");
                startActivity(intent);
                break;
            case R.id.btnDownTime:
                //myConfig.myTimer.cancel();
                intent = new Intent(OtherOptions.this, DownMainPage.class);
                intent.putExtra("Type","LinePage");
                startActivity(intent);
                break;

           /* case R.id.btnCamera:
                //myConfig.myTimer.cancel();
                intent = new Intent("MediaStore.ACTION_IMAGE_CAPTURE");
                startActivityForResult(intent,100);
                break;*/

            case R.id.btnBack:
                finish();
                break;

            default:
                break;
        }
    }


}
