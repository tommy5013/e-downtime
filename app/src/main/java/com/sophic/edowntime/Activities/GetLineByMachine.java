package com.sophic.edowntime.Activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityOptions;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.sophic.edowntime.Adapters.GetLineByMachineAdapter;
import com.sophic.edowntime.Helper.AutoLogout;
import com.sophic.edowntime.Helper.LoadingClass;
import com.sophic.edowntime.Model.MachineModel;
import com.sophic.edowntime.Model.MyConfig;
import com.sophic.edowntime.Model.PmModel;
import com.sophic.edowntime.R;
import com.sophic.edowntime.RestAPI.RestCrud;
import com.sophic.edowntime.RestAPI.APIClient;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GetLineByMachine extends AppCompatActivity implements
        android.view.View.OnClickListener {

    private TableLayout layoutMachineList;
    private RestCrud RestCrud;
    private MyConfig myConfig = MyConfig.getInstance();
    private int CureentData=0;
    private int PrevData=0;
    private String PageType="";

    private Button btnBack,btnNewPm;
    private TextView tvTittle;
    private String MachineType;
    private LoadingClass loadingClass;
    private AutoLogout autoLogout;
    private RecyclerView getLineByMachineRV;
    private GetLineByMachine parentActivity;

    @Override
    public void onDestroy() {
        autoLogout.stop();
        Log.d("TimerLog","PmListTimer Stop");
        // myConfig.PmListTimer.cancel();
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
        autoLogout.stop();
    }

    @Override
    public void onUserInteraction()
    {
        autoLogout.reset();
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.layout_pmline);
        parentActivity = this;
//        layoutMachineList = (TableLayout) findViewById(R.id.layoutMachineList);
        btnBack = (Button) findViewById(R.id.btnBack);
        btnNewPm = (Button) findViewById(R.id.btnNewPm);
        tvTittle = (TextView) findViewById(R.id.tvStartDate);
        getLineByMachineRV = (RecyclerView) findViewById(R.id.getLineByMachineRV);

        Intent intent = getIntent();
        MachineType = intent.getStringExtra("MachineType");

        tvTittle.setText("Machine Type : "+MachineType);
        tvTittle.setBackgroundResource(R.drawable.borderlinename);
        tvTittle.setTextColor(getResources().getColor(R.color.white));
        tvTittle.setTypeface(null, Typeface.BOLD);
        tvTittle.setPadding(10,8,10,8);

        btnBack.setOnClickListener(this);
        btnNewPm.setVisibility(View.INVISIBLE);

        PageType=getIntent().getStringExtra("Type");
        autoLogout = new AutoLogout(this);
        autoLogout.start();

        loadingClass = new LoadingClass(this,1);



        RestCrud = APIClient.getClient().create(RestCrud.class);

        ClearTable();

        if(PageType.equals("MachinePage")){

            //String Content=getIntent().getStringExtra("LineName");

            //Log.d("IMHERE",Content);
            //tvTittle.setText(Content);
            GetPmMachine(MachineType,false);
        }
        else{
            AddData(true);
        }

        CreateTimer();

    }

    private void CreateTimer()
    {
       /* myConfig.PmListTimer = new Timer();
        myConfig.PmListTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        if(PageType.equals("MachinePage"))
                        {
                            if(myConfig.getCurrentPage().equals("PmMachine")){
                                Log.d("zxcv","TinerCAlled:  "+PageType);
                                GetPmMachine(LineId,false);
                            }

                        }
                        else
                        {
                            if(myConfig.getCurrentPage().equals("PmLine")){
                                Log.d("zxcv","TinerCAlled:  "+PageType);
                                AddData(false);
                            }


                        }

                        Log.d("zxcv","TimerRuning:  "+myConfig.getCurrentPage());

                        //SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss yyyy-MM-dd");
                        //String currentDateandTime = sdf.format(new Date());
                        //tvLastUpdate.setText("Last Update :" + currentDateandTime);
                    }
                });

            }
        }, 500, 1500); // initial delay 1 second, interval 1 second*/
    }




    public void GetPmMachine(final String MachineType, final boolean Init) {

        MachineModel.AllMachineModel allMachineModel = new MachineModel().new AllMachineModel();
        allMachineModel.LineList=MyConfig.Lines;
        allMachineModel.MachineType = MachineType;

        Call<List<MachineModel.LineByMachine>> call = RestCrud.GetLineByMachine(allMachineModel);
        call.enqueue(new Callback<List<MachineModel.LineByMachine>>() {


            @SuppressLint("WrongConstant")
            @Override
            public void onResponse(Call<List<MachineModel.LineByMachine>> call, Response<List<MachineModel.LineByMachine>> response) {

                final List<MachineModel.LineByMachine> body = response.body();
                if (body == null) {
                    return;
                }

                Log.d("Res",response.body().toString());

                List<MachineModel.LineByMachine> resource = response.body();
                if(resource == null)
                    return;

                int numofdata=0;
                try {
                    numofdata = resource.size();
                }
                catch (Exception ex){
                    Toast.makeText(GetLineByMachine.this,"Error GetLineByMachine"+ex.getMessage(),Toast.LENGTH_LONG).show();
                }

                GetLineByMachineAdapter getLineByMachineAdapter = new GetLineByMachineAdapter(parentActivity,body);
                getLineByMachineRV.setLayoutManager(new GridLayoutManager(GetLineByMachine.this, 4));
                getLineByMachineRV.setNestedScrollingEnabled(false);
                getLineByMachineRV.setAdapter(getLineByMachineAdapter);
                getLineByMachineAdapter.setClickListener(new GetLineByMachineAdapter.ItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        String btntag = body.get(position).LineName;
                        Intent clickM = new Intent(GetLineByMachine.this, table_display.class);
                        Log.d("LineIdS","LineIdS"+btntag);
                        clickM.putExtra("LineId",btntag);
                        clickM.putExtra("MachineType",MachineType);
                        clickM.putExtra("LineName",body.get(position).LineName);
                        ActivityOptions options =
                                ActivityOptions.makeCustomAnimation(parentActivity, R.anim.slide_in_right, R.anim.slide_out_left);
                        startActivity(clickM,options.toBundle());
                        startActivityForResult(clickM, 100);
                    }
                });
                getLineByMachineAdapter.refreshRecyclerView();

//                CureentData = numofdata;

//                if(Init)
//                {
//                    PrevData=CureentData;
//                }
//                else if(PrevData==CureentData)
//                {
//                    return;
//                }
//
//                else
//                {
//                    PrevData=CureentData;
//                }

//                int i = 0;

//                ClearTable();

//                if (numofdata > 0) {
//
//                    int remainder=0;
//                    int rownum=0;
//                    int index = numofdata%4;
//                    int toloop=0;
//
//                    if(numofdata<=4){
//                        toloop=numofdata;
//                        rownum=1;
//                    }
//
//                    else if(index==0){
//                        toloop=4;
//                        rownum=numofdata/4;
//                    }
//
//                    else{
//                        toloop=4;
//                        rownum =numofdata/4;
//                        remainder = numofdata-(4*rownum);
//                    }
//
//
//                    int pos = 0;
//                    for (int j=0;j<rownum;j++)
//                    {
//                        TableRow row = new TableRow(GetLineByMachine.this);
//                        for (i=0; i < toloop; i++) {
//                            String LineNameMap= resource.get(pos).LineName;
//                            int c= resource.get(pos).ErrorCount;
//                            String Id= resource.get(pos).LineId;
//
//                            Button btn = new Button(GetLineByMachine.this);
//
//                            int px = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 100, getResources().getDisplayMetrics());
//                            TableRow.LayoutParams lp = new TableRow.LayoutParams(px,px);
//                            lp.setMargins(30,15,25,0);
//                            //btn.setBackgroundColor(Color.BLUE); // From android.graphics.Color
//                            ObjectAnimator anim = ObjectAnimator.ofInt(btn,"backgroundColor",Color.RED,Color.parseColor("#a30000"),Color.RED);
//                            anim.setDuration(1000);
//                            anim.setEvaluator(new ArgbEvaluator());
//                            anim.setRepeatMode(Animation.REVERSE);
//                            anim.setRepeatCount(Animation.INFINITE);
//                            anim.start();
//                            btn.setTextColor(Color.WHITE); //SET CUSTOM COLOR
//                            btn.setLayoutParams(lp);
//                            btn.setText(LineNameMap+"\n"+c);
//                            btn.setTag(LineNameMap);
//                            btn.setOnClickListener(GetLineByMachine.this);
//
//                            row.addView(btn);
//                            pos++;
//                        }
//                        layoutMachineList.addView(row);
//                    }
//
//                    if(remainder!=0){
//
//                        TableRow row = new TableRow(GetLineByMachine.this);
//                        for (i=0; i < remainder; i++) {
//                            String LineNameMap= resource.get(pos).LineName;
//                            int c= resource.get(pos).ErrorCount;
//                            String Id= resource.get(pos).LineId;
//
//                            Button btn = new Button(GetLineByMachine.this);
//
//                            int px = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 100, getResources().getDisplayMetrics());
//                            TableRow.LayoutParams lp = new TableRow.LayoutParams(px,px);
//                            lp.setMargins(30,15,25,0);
//                            //btn.setBackgroundColor(Color.BLUE); // From android.graphics.Color
//                            ObjectAnimator anim = ObjectAnimator.ofInt(btn,"backgroundColor",Color.RED,Color.parseColor("#a30000"),Color.RED);
//                            anim.setDuration(1000);
//                            anim.setEvaluator(new ArgbEvaluator());
//                            anim.setRepeatMode(Animation.REVERSE);
//                            anim.setRepeatCount(Animation.INFINITE);
//                            anim.start();
//                            btn.setTextColor(Color.WHITE); //SET CUSTOM COLOR
//                            btn.setLayoutParams(lp);
//                            btn.setText(LineNameMap+"\n"+c);
//                            btn.setTag(LineNameMap);
//                            btn.setOnClickListener(GetLineByMachine.this);
//
//                            row.addView(btn);
//                            pos++;
//                        }
//                        layoutMachineList.addView(row);
//                    }
//
//                }
            }

            @Override
            public void onFailure(Call<List<MachineModel.LineByMachine>> call, Throwable t) {
                Log.d("err","error get");
            }
        });

    }
    @Override
    public void onResume(){
        super.onResume();
        autoLogout.start();
        //myConfig.PmListTimer.cancel();
        //CreateTimer();
        Log.d("PMSTAT","Resume:  "+PageType);


    }

    public void AddData(final boolean Init) {

        Call<List<PmModel.GetPmLine>> call = RestCrud.GetPmLine();
        call.enqueue(new Callback<List<PmModel.GetPmLine>>() {


            @SuppressLint("WrongConstant")
            @Override
            public void onResponse(Call<List<PmModel.GetPmLine>> call, Response<List<PmModel.GetPmLine>> response) {

                final List<PmModel.GetPmLine> body = response.body();
                if (body == null) {
                    return;
                }

                Log.d("Res",response.body().toString());

                List<PmModel.GetPmLine> resource = response.body();


                if(resource == null)
                    return;

                int numofdata = resource.size();
                CureentData = numofdata;

                if(Init)
                {
                    PrevData=CureentData;
                }

                else if(PrevData==CureentData){
                    return;
                }

                else
                {
                    PrevData=CureentData;
                }

                GetLineByMachineAdapter getLineByMachineAdapter = new GetLineByMachineAdapter(body,parentActivity);
                getLineByMachineRV.setLayoutManager(new GridLayoutManager(GetLineByMachine.this, 3));
                getLineByMachineRV.setNestedScrollingEnabled(false);
                getLineByMachineRV.setAdapter(getLineByMachineAdapter);
                getLineByMachineAdapter.setClickListener(new GetLineByMachineAdapter.ItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        String btntag = body.get(position).LineNameMap;
                        Intent clickM = new Intent(GetLineByMachine.this, table_display.class);
                        Log.d("LineIdS","LineIdS"+btntag);
                        clickM.putExtra("LineId",btntag);
                        clickM.putExtra("MachineType",MachineType);
                        startActivity(clickM);
                        startActivityForResult(clickM, 100);
                    }
                });
                getLineByMachineAdapter.refreshRecyclerView();

//                ClearTable();
//                Log.d("zxcv","Data update");
//                int i = 0;
//
//                if (numofdata > 0) {
//
//                    loadingClass.show();
//
//                    int remainder=0;
//                    int rownum=0;
//                    int index = numofdata%4;
//                    int toloop=0;
//
//                    if(numofdata<=4){
//                        toloop=numofdata;
//                        rownum=1;
//                    }
//                    else if(index==0){
//                        toloop=4;
//                        rownum=numofdata/4;
//                    }
//                    else{
//                        toloop=4;
//                        rownum =numofdata/4;
//                        remainder = numofdata-(4*rownum);
//                    }
//
//                    TableRow row;
//
//                    int pos =0;
//
//                    for (int j=0;j<rownum;j++){
//
//                        row = new TableRow(GetLineByMachine.this);
//
//                        for (i=0; i < toloop; i++) {
//                            String LineNameMap= resource.get(pos).LineNameMap;
//                            int c= resource.get(pos).c;
//                            int Id= resource.get(pos).Id;
//
//                            Button btn = new Button(GetLineByMachine.this);
//
//                            int px = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 100, getResources().getDisplayMetrics());
//                            TableRow.LayoutParams lp = new TableRow.LayoutParams(px,px);
//                            lp.setMargins(30,15,25,0);
//                            if(c>0){
//                                btn.setBackgroundColor(Color.RED);
//                            }
//                            else{
//                                btn.setBackgroundColor(Color.BLUE); // From android.graphics.Color
//                            }
//
//
//                            /*
//                            ObjectAnimator anim = ObjectAnimator.ofInt(btn,"backgroundColor",Color.RED,Color.parseColor("#a30000"),Color.RED);
//                            anim.setDuration(10000);
//                            anim.setEvaluator(new ArgbEvaluator());
//                            anim.setRepeatMode(Animation.REVERSE);
//                            anim.setRepeatCount(Animation.INFINITE);
//                            anim.start();
//                            */
//
//                            btn.setTextColor(Color.WHITE); //SET CUSTOM COLOR
//                            btn.setLayoutParams(lp);
//                            btn.setText(LineNameMap+"\n"+c);
//                            btn.setTag(Id);
//                            btn.setOnClickListener(GetLineByMachine.this);
//
//                            row.addView(btn);
//                            pos++;
//                        }
////                        layoutMachineList.addView(row);
//                    }
//
//                    if(remainder!=0){
//                        int _remainder = numofdata-remainder;
//                        row = new TableRow(GetLineByMachine.this);
//
//                        for(int i2=_remainder;i2<numofdata;i2++){
//                            String LineNameMap= resource.get(pos).LineNameMap;
//                            int c= resource.get(pos).c;
//                            int Id= resource.get(pos).Id;
//
//                            Button btn = new Button(GetLineByMachine.this);
//
//                            int px = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 100, getResources().getDisplayMetrics());
//                            TableRow.LayoutParams lp = new TableRow.LayoutParams(px,px);
//                            lp.setMargins(30,15,25,0);
//
//                            if(c>0){
//                                btn.setBackgroundColor(Color.RED); // From android.graphics.Color
//                            }
//                            else{
//                                btn.setBackgroundColor(Color.BLUE); // From android.graphics.Color
//                            }
//
//                            /*
//                            ObjectAnimator anim = ObjectAnimator.ofInt(btn,"backgroundColor",Color.RED,Color.parseColor("#a30000"),Color.RED);
//                            anim.setDuration(1000);
//                            anim.setEvaluator(new ArgbEvaluator());
//                            anim.setRepeatMode(Animation.REVERSE);
//                            anim.setRepeatCount(Animation.INFINITE);
//                            anim.start();
//                            */
//
//                            btn.setTextColor(Color.WHITE); //SET CUSTOM COLOR
//                            btn.setLayoutParams(lp);
//                            btn.setText(LineNameMap+"\n"+c);
//                            btn.setTag(Id);
//                            btn.setOnClickListener(GetLineByMachine.this);
//
//                            row.addView(btn);
//                            pos++;
//                        }
////                        layoutMachineList.addView(row);
//                    }
//
//
//                    loadingClass.dismiss();
//
//                }
            }

            @Override
            public void onFailure(Call<List<PmModel.GetPmLine>> call, Throwable t) {
                Log.d("err","error get");
            }
        });
    }

    private void ClearTable()
    {
//        layoutMachineList.removeAllViews();
    }

    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.btnBack:
                if(myConfig.getCurrentPage() == "PmMachine")
                {
                    myConfig.setCurrentPage("PmLine");
                }
                else if(myConfig.getCurrentPage() == "PmLine")
                {
                    myConfig.setCurrentPage("PmMachine");
                }
                else
                {
                    myConfig.setCurrentPage("MainPage");
                }

                finish();
                break;

            case R.id.btnNewPm:
                Intent AddPMActivity = new Intent(GetLineByMachine.this, com.sophic.edowntime.Activities.AddPMActivity.class);
                AddPMActivity.putExtra("Type","PM");
                startActivity(AddPMActivity);//
                break;

            default:
                Button b = (Button)v;
                String btntag = b.getTag().toString();
                String Content = b.getText().toString();
                Toast.makeText(this, "Please Wait Loading Datax...", Toast.LENGTH_LONG).show();

                Intent clickM = new Intent(GetLineByMachine.this, table_display.class);
                Log.d("LineIdS","LineIdS"+btntag);
                clickM.putExtra("LineId",btntag);
                clickM.putExtra("MachineType",MachineType);
                startActivity(clickM);
                startActivityForResult(clickM, 100);


                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 100) {
            if(resultCode == Activity.RESULT_OK){
                GetPmMachine(MachineType,true);
                myConfig.setCurrentPage("PmMachine");

            }
            if (resultCode == Activity.RESULT_CANCELED) {
                // myConfig.getCurrentPage().equals("PmLine");
            }
        }
    }

}
