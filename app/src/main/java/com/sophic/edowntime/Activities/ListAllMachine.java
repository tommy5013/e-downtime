package com.sophic.edowntime.Activities;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.support.constraint.ConstraintLayout;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.sophic.edowntime.Adapters.GridViewAdapter;
import com.sophic.edowntime.Adapters.MachineAdapter;
import com.sophic.edowntime.Adapters.ViewPagerAdapter;
import com.sophic.edowntime.Helper.AlertHelper;
import com.sophic.edowntime.Helper.AutoLogout;
import com.sophic.edowntime.Helper.OnSwipeTouchListener;
import com.sophic.edowntime.Model.CoModel;
import com.sophic.edowntime.Model.MachineModel;
import com.sophic.edowntime.Model.MyConfig;
import com.sophic.edowntime.Model.PmModel;
import com.sophic.edowntime.RestAPI.APIClient;
import com.sophic.edowntime.R;
import com.sophic.edowntime.RestAPI.RestCrud;
import com.sophic.edowntime.indicator.CirclePageIndicator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.sophic.edowntime.Activities.ProcessActivity.printTrack;

public class ListAllMachine extends Base implements android.view.View.OnClickListener {

    public final String TAG = ListAllMachine.class.getName();
    public static int item_grid_num = 6;//每一页中GridView中item的数量
    public static int number_columns = 3;//gridview一行展示的数目
    int PmCureentData = 0, PmPrevData = 0;
    int CoCureentData = 0, CoPrevData = 0;
    boolean InitCo = true;
    boolean InitPm = true;
    boolean FirstRun = true;
    private RestCrud RestCrud;
    private RadioGroup rgroupCheckList;
    private Button line1, CPK, BTU, SPI, line3, btnLogout, btnOthers, AOI, EB, CML, CAMUL;
    private TextView displayuserid, tvLastUpdate;
    private AlertHelper alertHelper;
    private Context mycontext;
    private MyConfig myConfig = MyConfig.getInstance();
    private ListAllMachine parentActivity;
    private RecyclerView machineRV;
    private ViewPager view_pager;
    private ViewPagerAdapter mAdapter;
    private List<String> dataList;
    private List<GridView> gridList = new ArrayList<>();
    private CirclePageIndicator indicator;
    private TextView menuButton;
    private FrameLayout frameLayout;
    private ConstraintLayout machineMenu;
    private FrameLayout swipeFL, swipeFL2;
    private TextView menuTV;
    private ImageView menuCloseIV;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.layout_allmachine);
        parentActivity = this;

        displayuserid = (TextView) findViewById(R.id.displayuserid);
        tvLastUpdate = (TextView) findViewById(R.id.tvLastUpdate);
        btnLogout = (Button) findViewById(R.id.btnLogout);
        line1 = (Button) findViewById(R.id.line1);
        CPK = (Button) findViewById(R.id.CPK);
        line3 = (Button) findViewById(R.id.line3);
        BTU = (Button) findViewById(R.id.BTU);
        SPI = (Button) findViewById(R.id.SPI);
        AOI = (Button) findViewById(R.id.AOI);
        EB = (Button) findViewById(R.id.EB);
        CML = (Button) findViewById(R.id.CAML);
        CAMUL = (Button) findViewById(R.id.CAMUL);
        btnOthers = (Button) findViewById(R.id.btnOthers);

        battery = (TextView) findViewById(R.id.battery);
        tvWifiSSD = (TextView) findViewById(R.id.tvWifiSSD);
        wifi = (TextView) findViewById(R.id.wifi);
        machineRV = (RecyclerView) findViewById(R.id.machineRV);
        menuButton = (TextView) findViewById(R.id.menuButton);
        frameLayout = (FrameLayout) findViewById(R.id.frameLayout);
        swipeFL = (FrameLayout) findViewById(R.id.swipeFL);
        swipeFL2 = (FrameLayout) findViewById(R.id.swipeFL2);
        machineMenu = (ConstraintLayout) findViewById(R.id.machineMenu);
        menuTV = (TextView) findViewById(R.id.menuTV);
        menuCloseIV = (ImageView) findViewById(R.id.menuCloseIV);
        this.registerReceiver(broadcastReceiver, new IntentFilter(Intent.ACTION_BATTERY_CHANGED));

        autoLogout = new AutoLogout(this);
        autoLogout.start();


        RestCrud = APIClient.getClient().create(RestCrud.class);
        alertHelper = new AlertHelper();
        mycontext = getApplicationContext();

        displayuserid.setText("USERNAME:" + MainActivity.username);
        Initialization();

        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss yyyy-MM-dd");
        String currentDateandTime = sdf.format(new Date());
        tvLastUpdate.setText("Last Update :" + currentDateandTime);

        if (savedInstanceState == null) {
            MyConfig.TaskListMachine = new Handler();
            MyConfig.TaskListMachine.post(runnableCode);
            MyConfig.TaskListPM = new Handler();
            MyConfig.TaskListPM.post(UpdatePMCO);
        }

        //set button click
        line1.setOnClickListener(this);
        CPK.setOnClickListener(this);
        line3.setOnClickListener(this);
        AOI.setOnClickListener(this);
        BTU.setOnClickListener(this);
        SPI.setOnClickListener(this);
        EB.setOnClickListener(this);
        btnOthers.setOnClickListener(this);
        CAMUL.setOnClickListener(this);
        CML.setOnClickListener(this);

        btnLogout.setOnClickListener(this);
        menuButton.setOnClickListener(this);
        frameLayout.setOnClickListener(this);
        menuTV.setOnClickListener(this);
        swipeFL.setOnClickListener(this);
        menuCloseIV.setOnClickListener(this);

        GetMachineList();
        initViews();
        initDatas();
        MenuConfiguration();
    }

    private Runnable runnableCode = new Runnable() {
        @Override
        public void run() {
            Initialization();
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    printTrack();
                    //if(myConfig.isTaskListAllMachine()==true){
                    myConfig.setTaskListAllMachine(true);

                    SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss yyyy-MM-dd");
                    String currentDateandTime = sdf.format(new Date());
                    tvLastUpdate.setText("Last Update :" + currentDateandTime);

                    // }
                    Log.d("ISRUN", "LSITRUNING");
                    ProcessActivity.SaveThread(MyConfig.TaskListPM,UpdatePMCO,"runnableCode",TAG);
                }
            });
            MyConfig.TaskListMachine.postDelayed(runnableCode, 2000);
        }
    };
    private Runnable UpdatePMCO = new Runnable() {
        @Override
        public void run() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    UpdatePM();
                    UpdateCO();
                    wifi();
                    MyConfig.TaskListPM.postDelayed(UpdatePMCO, 3000);
                    printTrack();
                    ProcessActivity.SaveThread(MyConfig.TaskListPM,UpdatePMCO,"UpdatePMCO",TAG);
                }
            });
        }
    };

    @Override
    public void onResume() {
        super.onResume();
        wifi();
        autoLogout.start();

    }

    public void onDestroy() {
        Log.d("call", "call");
        MyConfig.TaskListPM.removeCallbacks(UpdatePMCO);
        MyConfig.TaskListMachine.removeCallbacks(runnableCode);
        unregisterReceiver(broadcastReceiver);
        autoLogout.stop();
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
        autoLogout.stop();
    }

    private void GetMachineList() {
        RestCrud = APIClient.getClient().create(RestCrud.class);
        Call<List<MachineModel.AllMachineList>> call = null;
        call = RestCrud.GetMachineList();
        call.enqueue(new Callback<List<MachineModel.AllMachineList>>() {

            @Override
            public void onResponse(Call<List<MachineModel.AllMachineList>> call, Response<List<MachineModel.AllMachineList>> response) {

                List<MachineModel.AllMachineList> allMachineLists = response.body();
            }

            @Override
            public void onFailure(Call<List<MachineModel.AllMachineList>> call, Throwable t) {

            }
        });
    }

    public void Initialization() {

        MachineModel.AllMachineModel allMachineModel = new MachineModel().new AllMachineModel();
        allMachineModel.LineList = MyConfig.Lines;
        Call<List<MachineModel>> call = RestCrud.CountMachineAlert(allMachineModel);
        call.enqueue(new Callback<List<MachineModel>>() {


            @Override
            public void onResponse(Call<List<MachineModel>> call, Response<List<MachineModel>> response) {

                List<MachineModel> body = response.body();
                if (body == null) {
                    return;
                }

                Log.d("Res", response.body().toString());

                List<MachineModel> resource = response.body();


                if (resource == null)
                    return;

                int numofdata = 0;

                try {
                    numofdata = resource.size();
                } catch (Exception ex) {
                    Toast.makeText(ListAllMachine.this, "Error Count Machine:" + ex.getMessage(), Toast.LENGTH_LONG).show();
                }

                if (numofdata > 0) {

                    for (int i = 0; i < resource.size(); i++) {
                        String MachineType = resource.get(i).MachineType;
                        String ErrorCount = resource.get(i).ErrorCount;
                        String OtherParam = resource.get(i).OtherParam;

                        switch (MachineType) {

                            case "NXT":
                                myConfig.setNxtCurrent(Integer.valueOf(ErrorCount));

                                Log.d("ErrorCountX", "onResponse: " + ErrorCount);

                                if (myConfig.getNxtPrevious() == 0) {
                                    myConfig.setNxtPrevious(myConfig.getNxtCurrent());
                                } else {
                                    UpdateErrors(line1, myConfig.getNxtCurrent(), myConfig.getNxtPrevious(), "NXT");

                                }

                                if (myConfig.getNxtCurrent() != myConfig.getNxtPrevious()) {
                                    myConfig.setNxtPrevious(myConfig.getNxtCurrent());
                                } else if (myConfig.getNxtCurrent() > 0) {
                                    myConfig.setNxtPrevious(myConfig.getNxtCurrent());
                                }


                                break;

                            case "SPI":
                                myConfig.setSPICurrent(Integer.valueOf(ErrorCount));

                                if (myConfig.getSPIPrevious() == 0) {
                                    myConfig.setSPIPrevious(myConfig.getSPICurrent());
                                } else {
                                    UpdateErrors(SPI, myConfig.getSPICurrent(), myConfig.getSPIPrevious(), "SPI");
                                }

                                if (myConfig.getSPICurrent() != myConfig.getSPIPrevious()) {
                                    myConfig.setSPIPrevious(myConfig.getSPICurrent());
                                } else if (myConfig.getSPICurrent() > 0) {
                                    myConfig.setSPIPrevious(myConfig.getSPICurrent());
                                }
                                break;

                            case "DEK":
                                myConfig.setDEKCurrent(Integer.valueOf(ErrorCount));

                                if (myConfig.getDEKPrevious() == 0) {

                                    myConfig.setDEKPrevious(myConfig.getDEKCurrent());
                                } else {
                                    UpdateErrors(line3, myConfig.getDEKCurrent(), myConfig.getDEKPrevious(), "DEK");
                                }

                                if (myConfig.getDEKCurrent() != myConfig.getDEKPrevious()) {
                                    myConfig.setDEKPrevious(myConfig.getDEKCurrent());
                                } else if (myConfig.getDEKCurrent() > 0) {
                                    myConfig.setDEKPrevious(myConfig.getDEKCurrent());
                                }
                                break;

                            case "AOI":
                                myConfig.setAOICurrent(Integer.valueOf(ErrorCount));
                                if (myConfig.getAOIPrevious() == 0) {
                                    myConfig.setAOIPrevious(myConfig.getAOICurrent());
                                } else {
                                    UpdateErrors(AOI, myConfig.getAOICurrent(), myConfig.getAOIPrevious(), "AOI");

                                }

                                if (myConfig.getAOICurrent() != myConfig.getAOIPrevious()) {
                                    myConfig.setAOIPrevious(myConfig.getAOICurrent());
                                } else if (myConfig.getAOICurrent() > 0) {
                                    myConfig.setAOIPrevious(myConfig.getAOICurrent());
                                }
                                break;

                            case "BTU":
                                myConfig.setBTUCurrent(Integer.valueOf(ErrorCount));
                                if (myConfig.getBTUPrevious() == 0) {
                                    myConfig.setBTUPrevious(myConfig.getBTUCurrent());
                                } else {
                                    UpdateErrors(BTU, myConfig.getBTUCurrent(), myConfig.getBTUPrevious(), "BTU");

                                }

                                if (myConfig.getBTUCurrent() != myConfig.getBTUPrevious()) {
                                    myConfig.setBTUPrevious(myConfig.getBTUCurrent());
                                } else if (myConfig.getBTUCurrent() > 0) {
                                    myConfig.setBTUPrevious(myConfig.getBTUCurrent());
                                }
                                break;

                            case "EB":
                                myConfig.setEBCurrent(Integer.valueOf(ErrorCount));
                                if (myConfig.getEBPrevious() == 0) {
                                    myConfig.setEBPrevious(myConfig.getEBCurrent());
                                } else {
                                    UpdateErrors(EB, myConfig.getEBCurrent(), myConfig.getEBPrevious(), "EB");

                                }

                                if (myConfig.getEBCurrent() != myConfig.getEBPrevious()) {
                                    myConfig.setEBPrevious(myConfig.getEBCurrent());
                                } else if (myConfig.getEBCurrent() > 0) {
                                    myConfig.setEBPrevious(myConfig.getEBCurrent());
                                }
                                break;

                            case "CPK":
                                myConfig.setCPKCurrent(Integer.valueOf(ErrorCount));
                                if (myConfig.getCPKPrevious() == 0) {
                                    myConfig.setCPKPrevious(myConfig.getCPKCurrent());
                                } else {
                                    UpdateErrors(CPK, myConfig.getCPKCurrent(), myConfig.getCPKPrevious(), "CPK");

                                }

                                if (myConfig.getCPKCurrent() != myConfig.getCPKPrevious()) {
                                    myConfig.setCPKPrevious(myConfig.getCPKCurrent());
                                } else if (myConfig.getCPKCurrent() > 0) {
                                    myConfig.setCPKPrevious(myConfig.getCPKCurrent());
                                }
                                break;

                            case "CAML":
                                myConfig.setCMLCurrent(Integer.valueOf(ErrorCount));
                                if (myConfig.getCMLPrevious() == 0) {
                                    myConfig.setCMLPrevious(myConfig.getCMLCurrent());
                                } else {
                                    UpdateErrors(CML, myConfig.getCMLCurrent(), myConfig.getCMLPrevious(), "CML");

                                }

                                if (myConfig.getCMLCurrent() != myConfig.getCMLPrevious()) {
                                    myConfig.setCMLPrevious(myConfig.getCMLCurrent());
                                } else if (myConfig.getCMLCurrent() > 0) {
                                    myConfig.setCMLPrevious(myConfig.getCMLCurrent());
                                }
                                break;

                            case "CAMUL":
                                myConfig.setCAMULCurrent(Integer.valueOf(ErrorCount));
                                if (myConfig.getCAMULPrevious() == 0) {
                                    myConfig.setCAMULPrevious(myConfig.getCAMULCurrent());
                                } else {
                                    UpdateErrors(CAMUL, myConfig.getCAMULCurrent(), myConfig.getCAMULPrevious(), "CAMUL");

                                }

                                if (myConfig.getCAMULCurrent() != myConfig.getCAMULPrevious()) {
                                    myConfig.setCAMULPrevious(myConfig.getCAMULCurrent());
                                } else if (myConfig.getCPKCurrent() > 0) {
                                    myConfig.setCPKPrevious(myConfig.getCPKCurrent());
                                }
                                break;

                        }

                    }

                }

                if (FirstRun) {
                    FirstRun = false;
                    populateMachine(body);
                } else {
                    myConfig.setTaskListAllMachine(false);
                }

            }

            @Override
            public void onFailure(Call<List<MachineModel>> call, Throwable t) {
                Log.d("err", "error get");
            }
        });

    }

    public void UpdatePM() {

        Call<List<PmModel.GetPmLine>> call = RestCrud.GetPmLine();
        call.enqueue(new Callback<List<PmModel.GetPmLine>>() {


            @Override
            public void onResponse(Call<List<PmModel.GetPmLine>> call, Response<List<PmModel.GetPmLine>> response) {

                List<PmModel.GetPmLine> body = response.body();
                if (body == null) {
                    return;
                }

                Log.d("Res", response.body().toString());

                List<PmModel.GetPmLine> resource = response.body();


                if (resource == null)
                    return;

                int numofdata = resource.size();
                PmCureentData = numofdata;

                if (InitPm) {
                    PmPrevData = PmCureentData;

                } else if (PmPrevData == PmCureentData) {
                    return;
                } else {
                    PmPrevData = PmCureentData;
                }


                Log.d("pm updated", "Data update" + numofdata);
                int i = 0;

                if (numofdata > 0) {

                    if (InitPm == true) {
                        InitPm = false;
                    } else {

                    }
                    alertHelper.showNotification(mycontext);
                    alertHelper.StartVibrate(mycontext);
                    // ShowDialog("PM UPDATED PRESS OK TO CLOSE");

                }
            }

            @Override
            public void onFailure(Call<List<PmModel.GetPmLine>> call, Throwable t) {
                Log.d("err", "error get");
            }
        });
    }

    public void UpdateCO() {

        Call<List<CoModel.GetCoLine>> call = RestCrud.GetCoLine();
        call.enqueue(new Callback<List<CoModel.GetCoLine>>() {


            @Override
            public void onResponse(Call<List<CoModel.GetCoLine>> call, Response<List<CoModel.GetCoLine>> response) {

                List<CoModel.GetCoLine> body = response.body();
                if (body == null) {
                    return;
                }

                Log.d("Res", response.body().toString());

                List<CoModel.GetCoLine> resource = response.body();


                if (resource == null)
                    return;

                int numofdata = resource.size();
                CoCureentData = numofdata;

                if (InitCo) {
                    CoPrevData = CoCureentData;

                } else if (CoPrevData == CoCureentData) {
                    return;
                } else {
                    CoPrevData = CoCureentData;
                }


                Log.d("co updated", "Data update co" + numofdata + InitCo);
                int i = 0;

                if (numofdata > 0) {

                    if (InitCo) {
                        InitCo = false;
                    } else {

                    }

                    alertHelper.showNotification(mycontext);
                    alertHelper.StartVibrate(mycontext);
                    //ShowDialog("CO UPDATED PRESS OK TO CLOSE");
                }
            }

            @Override
            public void onFailure(Call<List<CoModel.GetCoLine>> call, Throwable t) {
                Log.d("err", "error get");
            }
        });
    }


    @SuppressLint("WrongConstant")
    private void UpdateErrors(Button mybtn, int currentval, int preval, String MachineName) {

        mybtn.setText(MachineName + "\n" + currentval);

        if (currentval != preval) {

            /*
            ObjectAnimator anim = ObjectAnimator.ofInt(mybtn,"backgroundColor",Color.RED,Color.parseColor("#a30000"),Color.RED);
            anim.setDuration(1000);
            anim.setEvaluator(new ArgbEvaluator());
            anim.setRepeatMode(Animation.REVERSE);
            anim.setRepeatCount(Animation.INFINITE);
            anim.start();
            */

            mybtn.setBackgroundResource(android.R.color.holo_red_dark);

            if (currentval > preval) {
                alertHelper.showNotification(mycontext);
                alertHelper.StartVibrate(mycontext);

                //ShowDialog("NEW ALERT IN "+MachineName+".PRESS OK TO CLOSE");

                if (myConfig.isTableDisplayIsVisible())
                    table_display.txt_UserName.setText("FOUND NEW ALERT");


                Toast.makeText(ListAllMachine.this, "NEW ALERT IN " + MachineName, Toast.LENGTH_LONG).show();

            } else if (currentval == 0) {
                mybtn.setBackgroundResource(android.R.color.holo_blue_dark);
            }
        } else if (currentval > 0) {
            /*
            ObjectAnimator anim = ObjectAnimator.ofInt(mybtn,"backgroundColor",Color.RED,Color.parseColor("#a30000"),Color.RED);
            anim.setDuration(1000);
            anim.setEvaluator(new ArgbEvaluator());
            anim.setRepeatMode(Animation.REVERSE);
            anim.setRepeatCount(Animation.INFINITE);
            anim.start();
            */
            mybtn.setBackgroundResource(android.R.color.holo_red_dark);
        } else {
            mybtn.setBackgroundResource(android.R.color.holo_green_dark);
        }

    }

    @Override
    public void onClick(View view) {
        Intent intent;
        switch (view.getId()) {
            case R.id.line1:
                //intent = new Intent(ListAllMachine.this, table_display.class);
                //
                intent = new Intent(ListAllMachine.this, GetLineByMachine.class);
                intent.putExtra("Type", "MachinePage");
                intent.putExtra("MachineType", "NXT");
                startActivity(intent);
                break;
            case R.id.SPI:
                //intent = new Intent(ListAllMachine.this, table_display.class);
                intent = new Intent(ListAllMachine.this, GetLineByMachine.class);
                intent.putExtra("MachineType", "SPI");
                intent.putExtra("Type", "MachinePage");
                startActivity(intent);
                break;
            case R.id.line3:
                //intent = new Intent(ListAllMachine.this, table_display.class);
                intent = new Intent(ListAllMachine.this, GetLineByMachine.class);
                intent.putExtra("MachineType", "DEK");
                intent.putExtra("Type", "MachinePage");
                startActivity(intent);
                break;
            case R.id.AOI:
                //intent = new Intent(ListAllMachine.this, table_display.class);
                intent = new Intent(ListAllMachine.this, GetLineByMachine.class);
                intent.putExtra("MachineType", "AOI");
                intent.putExtra("Type", "MachinePage");
                startActivity(intent);
                break;
            case R.id.BTU:
                //intent = new Intent(ListAllMachine.this, table_display.class);
                intent = new Intent(ListAllMachine.this, GetLineByMachine.class);
                intent.putExtra("MachineType", "BTU");
                intent.putExtra("Type", "MachinePage");
                startActivity(intent);
                break;
            case R.id.EB:
                //intent = new Intent(ListAllMachine.this, table_display.class);
                intent = new Intent(ListAllMachine.this, GetLineByMachine.class);
                intent.putExtra("MachineType", "EB");
                intent.putExtra("Type", "MachinePage");
                startActivity(intent);
                break;
            case R.id.CPK:
                //intent = new Intent(ListAllMachine.this, table_display.class);
                intent = new Intent(ListAllMachine.this, GetLineByMachine.class);
                intent.putExtra("MachineType", "CPK");
                intent.putExtra("Type", "MachinePage");
                startActivity(intent);
                break;
            case R.id.CAML:
                //intent = new Intent(ListAllMachine.this, table_display.class);
                intent = new Intent(ListAllMachine.this, GetLineByMachine.class);
                intent.putExtra("MachineType", "CAML");
                intent.putExtra("Type", "MachinePage");
                startActivity(intent);
                break;
            case R.id.CAMUL:
                //intent = new Intent(ListAllMachine.this, table_display.class);
                intent = new Intent(ListAllMachine.this, GetLineByMachine.class);
                intent.putExtra("MachineType", "CAMUL");
                intent.putExtra("Type", "MachinePage");
                startActivity(intent);
                break;

            case R.id.btnOthers:
                intent = new Intent(ListAllMachine.this, OtherOptions.class);
                startActivity(intent);
                break;
            case R.id.btnPM:
                //myConfig.myTimer.cancel();
                intent = new Intent(ListAllMachine.this, PMMainPage.class);
                intent.putExtra("Type", "LinePage");
                myConfig.setCurrentPage("PmLine");
                startActivity(intent);
                break;

            case R.id.btnCO:
                //myConfig.myTimer.cancel();
                intent = new Intent(ListAllMachine.this, COMainPage.class);
                intent.putExtra("Type", "LinePage");
                myConfig.setCoCurrentPage("CoLine");
                startActivity(intent);
                break;

            case R.id.btnLogout:
                //myConfig.myTimer.cancel();
                finish();
                break;

            case R.id.menuButton:
                Animation slide_up = AnimationUtils.loadAnimation(getApplicationContext(),
                        R.anim.slide_up);

                if (machineMenu.getVisibility() == View.GONE) {
                    machineMenu.startAnimation(slide_up);

                    machineMenu.setVisibility(View.VISIBLE);
                    frameLayout.setVisibility(View.VISIBLE);

                    menuButton.setVisibility(View.GONE);
                }
                break;

            case R.id.frameLayout:
                Animation slide_down = AnimationUtils.loadAnimation(getApplicationContext(),
                        R.anim.slide_down);
                machineMenu.startAnimation(slide_down);

                machineMenu.setVisibility(View.GONE);
                frameLayout.setVisibility(View.GONE);
//                menuButton.setVisibility(View.VISIBLE);
                break;

            case R.id.menuTV:

                slide_up = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_up);
                if (machineMenu.getVisibility() == View.GONE) {
                    machineMenu.startAnimation(slide_up);

                    machineMenu.setVisibility(View.VISIBLE);
                    frameLayout.setVisibility(View.VISIBLE);

                    menuButton.setVisibility(View.GONE);
                }
                break;

            case R.id.swipeFL:

                slide_up = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.slide_up);
                if (machineMenu.getVisibility() == View.GONE) {
                    machineMenu.startAnimation(slide_up);

                    machineMenu.setVisibility(View.VISIBLE);
                    frameLayout.setVisibility(View.VISIBLE);

                    menuButton.setVisibility(View.GONE);
                }
                break;

            case R.id.menuCloseIV:
                slide_down = AnimationUtils.loadAnimation(getApplicationContext(),
                        R.anim.slide_down);
                machineMenu.startAnimation(slide_down);

                machineMenu.setVisibility(View.GONE);
                frameLayout.setVisibility(View.GONE);
                break;
            default:
                break;
        }
    }

    public String readJSONFromAsset() {
        String json = null;
        try {
            InputStream is = getAssets().open("otherMachine.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    public List<String> getMachineList() {
        List<String> machineList = new ArrayList<>();
        try {
            JSONArray jsonArray = new JSONArray(readJSONFromAsset());
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject object = jsonArray.getJSONObject(i);
                String machineName = object.get("MachineName").toString();
                machineList.add(machineName);
            }
            //Add Back Button
            machineList.add("STATUS");
            machineList.add("BACK");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return machineList;
    }

    private void populateMachine(List<MachineModel> allMachineList) {
        MachineAdapter machineAdapter = new MachineAdapter(parentActivity, allMachineList);
        machineRV.setLayoutManager(new GridLayoutManager(ListAllMachine.this, 4));
        machineRV.setNestedScrollingEnabled(false);
        machineRV.setAdapter(machineAdapter);
        machineAdapter.setClickListener(new MachineAdapter.ItemClickListener() {
            @Override
            public void onItemClick(View view, int position, String machinName) {
                IntentToNextPage(machinName);
            }
        });
    }

    private void initViews() {
        //初始化ViewPager
        view_pager = (ViewPager) findViewById(R.id.view_pager);
        final ImageView leftArrow = findViewById(R.id.leftArrow);
        final ImageView rightArrow = findViewById(R.id.rightArrow);

        mAdapter = new ViewPagerAdapter();
        view_pager.setAdapter(mAdapter);
        dataList = new ArrayList<>();
        //圆点指示器
        if (getMachineList().size() <= 6) {
            rightArrow.setVisibility(View.GONE);
        }
        indicator = (CirclePageIndicator) findViewById(R.id.indicator);
        indicator.setVisibility(View.VISIBLE);
        indicator.setViewPager(view_pager);
        view_pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int i, float v, int i1) {

            }

            @Override
            public void onPageSelected(int i) {
                switch (i) {
                    case 0:
                        leftArrow.setVisibility(View.GONE);
                        rightArrow.setVisibility(View.VISIBLE);
                        break;
                    case 1:
                        rightArrow.setVisibility(View.GONE);
                        leftArrow.setVisibility(View.VISIBLE);
                        break;

                    default:
                        leftArrow.setVisibility(View.VISIBLE);
                        rightArrow.setVisibility(View.VISIBLE);
                        break;
                }
                indicator.setCurrentItem(i);
            }

            @Override
            public void onPageScrollStateChanged(int i) {

            }
        });

        leftArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                view_pager.setCurrentItem(view_pager.getCurrentItem() - 1, true);
            }
        });

        rightArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                view_pager.setCurrentItem(view_pager.getCurrentItem() + 1, true);
            }
        });
    }

    private void initDatas() {
        if (dataList.size() > 0) {
            dataList.clear();
        }
        if (gridList.size() > 0) {
            gridList.clear();
        }
        //初始化数据
        dataList = getMachineList();
        //计算viewpager一共显示几页
        int pageSize = dataList.size() % item_grid_num == 0
                ? dataList.size() / item_grid_num
                : dataList.size() / item_grid_num + 1;
        for (int i = 0; i < pageSize; i++) {
            GridView gridView = new GridView(this);
            GridViewAdapter adapter = new GridViewAdapter(dataList, i, parentActivity);
            gridView.setNumColumns(number_columns);
            gridView.setAdapter(adapter);
            gridList.add(gridView);
        }
        mAdapter.add(gridList);
    }

    private void closeDrawer() {
        machineMenu.setVisibility(View.GONE);
        frameLayout.setVisibility(View.GONE);
//        menuButton.setVisibility(View.VISIBLE);
    }

    public void IntentToNextPage(String machineType) {
        Intent intent;
        switch (machineType) {
            case "NXT":
                closeDrawer();
                intent = new Intent(ListAllMachine.this, GetLineByMachine.class);
                intent.putExtra("Type", "MachinePage");
                intent.putExtra("MachineType", "NXT");
                startActivity(intent);
                break;
            case "SPI":
                closeDrawer();
                intent = new Intent(ListAllMachine.this, GetLineByMachine.class);
                intent.putExtra("MachineType", "SPI");
                intent.putExtra("Type", "MachinePage");
                startActivity(intent);
                break;
            case "DEK":
                closeDrawer();
                intent = new Intent(ListAllMachine.this, GetLineByMachine.class);
                intent.putExtra("MachineType", "DEK");
                intent.putExtra("Type", "MachinePage");
                startActivity(intent);
                break;
            case "AOI":
                closeDrawer();
                intent = new Intent(ListAllMachine.this, GetLineByMachine.class);
                intent.putExtra("MachineType", "AOI");
                intent.putExtra("Type", "MachinePage");
                startActivity(intent);
                break;
            case "BTU":
                closeDrawer();
                intent = new Intent(ListAllMachine.this, GetLineByMachine.class);
                intent.putExtra("MachineType", "BTU");
                intent.putExtra("Type", "MachinePage");
                startActivity(intent);
                break;
            case "EB":
                closeDrawer();
                intent = new Intent(ListAllMachine.this, GetLineByMachine.class);
                intent.putExtra("MachineType", "EB");
                intent.putExtra("Type", "MachinePage");
                startActivity(intent);
                break;
            case "CPK":
                closeDrawer();
                intent = new Intent(ListAllMachine.this, GetLineByMachine.class);
                intent.putExtra("MachineType", "CPK");
                intent.putExtra("Type", "MachinePage");
                startActivity(intent);
                break;
            case "CAML":
                closeDrawer();
                intent = new Intent(ListAllMachine.this, GetLineByMachine.class);
                intent.putExtra("MachineType", "CAML");
                intent.putExtra("Type", "MachinePage");
                startActivity(intent);
                break;
            case "CAMUL":
                closeDrawer();
                intent = new Intent(ListAllMachine.this, GetLineByMachine.class);
                intent.putExtra("MachineType", "CAMUL");
                intent.putExtra("Type", "MachinePage");
                startActivity(intent);
                break;
        }
    }

    public void IntentOtherMachinePage(String machineType, View view) {
        Intent intent;
        switch (machineType) {
            case "CO":
                closeDrawer();
                intent = new Intent(ListAllMachine.this, COMainPage.class);
                intent.putExtra("Type", "LinePage");
                myConfig.setCoCurrentPage("CoLine");
                startActivity(intent);
                break;
            case "PM":
                closeDrawer();
                intent = new Intent(ListAllMachine.this, PMMainPage.class);
                intent.putExtra("Type", "LinePage");
                myConfig.setCurrentPage("PmLine");
                startActivity(intent);
                break;
            case "DOWN":
                closeDrawer();
                intent = new Intent(ListAllMachine.this, DownMainPage.class);
                intent.putExtra("Type", "LinePage");
                startActivity(intent);
                break;
            case "STATUS":
                closeDrawer();
                intent = new Intent(ListAllMachine.this, ProcessActivity.class);
                intent.putExtra("Type", "ProcessPage");
                startActivity(intent);
                break;
            case "BACK":
                closeDrawer();
                parentActivity.onBackPressed();
                break;
        }
    }

    public void SetImage(String machineType, ImageView view) {
        switch (machineType) {
            case "CO":
                view.setImageResource(R.drawable.icon_co_64);
                break;
            case "PM":
                view.setImageResource(R.drawable.icon_machine_64);
                break;
            case "DOWN":
                view.setImageResource(R.drawable.icon_down_64);
                break;
            case "STATUS":
                view.setImageResource(R.drawable.icon_process_64);
                break;
            case "BACK":
                view.setImageResource(R.drawable.back);
                break;
        }
    }

    private void MenuConfiguration(){
        //Hacky way for swipe menu
        swipeFL.setOnTouchListener(new OnSwipeTouchListener(parentActivity) {
            public void onSwipeTop() {
                Animation slide_up = AnimationUtils.loadAnimation(getApplicationContext(),
                        R.anim.slide_up);

                if (machineMenu.getVisibility() == View.GONE) {
                    machineMenu.startAnimation(slide_up);
                    machineMenu.setVisibility(View.VISIBLE);
                    frameLayout.setVisibility(View.VISIBLE);

                    menuButton.setVisibility(View.GONE);
                }
            }

            public void onSwipeRight() {
//                Toast.makeText(parentActivity, "right", Toast.LENGTH_SHORT).show();
            }

            public void onSwipeLeft() {
//                Toast.makeText(parentActivity, "left", Toast.LENGTH_SHORT).show();
            }

            public void onSwipeBottom() {
//                Toast.makeText(parentActivity, "bottom", Toast.LENGTH_SHORT).show();
            }

        });
        //Hacky way for click menu
        swipeFL2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Animation slide_up = AnimationUtils.loadAnimation(getApplicationContext(),
                        R.anim.slide_up);

                if (machineMenu.getVisibility() == View.GONE) {
                    machineMenu.startAnimation(slide_up);
                    machineMenu.setVisibility(View.VISIBLE);
                    frameLayout.setVisibility(View.VISIBLE);
                    menuButton.setVisibility(View.GONE);
                }
            }
        });
    }
}
