package com.sophic.edowntime.Activities;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Vibrator;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.projectx.libx.Utils.SharedPref;
import com.projectx.libx.View.LicenceDialog;
import com.sophic.edowntime.Helper.Configs.ConfigMainActivity;
import com.sophic.edowntime.Helper.Configs.SharedPreferencesRestCrud;
import com.sophic.edowntime.Controllers.MainActivityController;
import com.sophic.edowntime.Controllers.SerialNumberHashController;
import com.sophic.edowntime.Helper.CamLib;
import com.sophic.edowntime.Helper.ConfirmDialog;
import com.sophic.edowntime.Helper.LoadingClass;
import com.sophic.edowntime.Model.MachineModel;
import com.sophic.edowntime.Model.MyConfig;
import com.sophic.edowntime.RestAPI.APIClient;
import com.sophic.edowntime.BuildConfig;
import com.sophic.edowntime.R;
import com.sophic.edowntime.RestAPI.RestCrud;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    public String TAG = MainActivity.class.getName();
    public static String PACKAGE_NAME;
    private Intent intent;
    public static String username;
    private String password = "";
    private EditText txtusername, txtpassword;
    private Vibrator vibrator;
    private SharedPreferences sharedpreferences;
    private MyConfig myConfig = MyConfig.getInstance();
    public RestCrud restCrud;
    private boolean CheckDone = false;
    private Button btnCapture;
    public final int TAKE_PICTURE = 1;
    private TextView tvVersionNumber;
    private Button btn_setting, btn_PassPMLogin;
    private ConfigMainActivity configMainActivity = new ConfigMainActivity();
    private MainActivityController mainActivityController = new MainActivityController();
    private String Lic="0";
    private MainActivity parentActivity;
    private LoadingClass loadingClass = new LoadingClass();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_main);
        //Initial service
        parentActivity = this;
        mainActivityController.parentActivity = this;
        PACKAGE_NAME = getApplicationContext().getPackageName();
        vibrator = (Vibrator) this.getSystemService(Context.VIBRATOR_SERVICE);

        //EditText
        txtusername = findViewById(R.id.text_username);
        txtpassword = findViewById(R.id.txt_Password);
        //Button
        btnCapture = findViewById(R.id.btnCapture);
        btn_setting = findViewById(R.id.btnSetting);
        btn_PassPMLogin = findViewById(R.id.btn_PassPMLogin);
        //TextView
        tvVersionNumber = findViewById(R.id.tvVersionNumber);

        //Assign Value
        tvVersionNumber.setText("Ver:" + BuildConfig.VERSION_NAME);

        //SharePreferences
        sharedpreferences = getSharedPreferences(configMainActivity.MyPREFERENCES, Context.MODE_PRIVATE);

        //OnClickListener
        btn_PassPMLogin.setOnClickListener(this);
        btn_setting.setOnClickListener(this);
        btnCapture.setOnClickListener(this);

        // Snackbar snackbar = Snackbar.make(findViewById(android.R.id.content), "Welcome To Main Activity", Snackbar.LENGTH_LONG);
        //snackbar.show();
        loadingClass = new LoadingClass(this,1);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case TAKE_PICTURE:
                if (resultCode == Activity.RESULT_OK) {

                }
        }
    }

    //We will use this method to build our URL string and then pass it to the network task object
    //to execute it in the background.
    //When we receive a reply then
    private void authenticate(String userName, String Password) {
        loadingClass.show();
        MachineModel.UserInfo userInfo = new MachineModel().new UserInfo();
        userInfo.Username = userName;
        userInfo.Password = Password;
        userInfo.UserType = "type";
        userInfo.Wifi = APIClient.Wifi;
        Call<MachineModel.UserInfo> call = restCrud.GetUser(userInfo);
        call.enqueue(new Callback<MachineModel.UserInfo>() {
            @Override
            public void onResponse(Call<MachineModel.UserInfo> call, Response<MachineModel.UserInfo> response) {
                loadingClass.dismiss();
                if(response.isSuccessful()){
                    MachineModel.UserInfo body = response.body();
                    if(Lic.equals("0")){
                        Toast.makeText(MainActivity.this,"Device Not Register",Toast.LENGTH_SHORT).show();
                        AuthenticateLicense();
                        return;
                    }
                    if (body.UserType.equals("null")) {
                        Toast.makeText(MainActivity.this, "Error Wrong  Username/Password", Toast.LENGTH_LONG).show();
                    } else if (body.UserType.toLowerCase().equals("lock")) {
                        Toast.makeText(MainActivity.this, "Error User Already Login ", Toast.LENGTH_LONG).show();
                    } else {
                        intent = new Intent(MainActivity.this, LineSelectActivity.class);

                        intent.putExtra("Type", "LinePage");
                        startActivity(intent);
                    }
                }
                else{
                    Toast.makeText(MainActivity.this, response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<MachineModel.UserInfo> call, Throwable t) {
                loadingClass.dismiss();
                Toast.makeText(MainActivity.this, "Error", Toast.LENGTH_LONG).show();
            }
        });
    }


    private void CheckUpdate() {
        Call<String> call = restCrud.CheckUpdate();
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.isSuccessful()) {
                    String result = response.body();
                    Log.d("login", "requestDone: " + result);
                    if (result.contains("True,")) {
                        String currentVersion = BuildConfig.VERSION_NAME;
                        result = result.replace(" ", "");
                        result = result.substring(0, result.length() - 1);
                        result = result.substring(1);

                        String[] UpdateVer = result.split(",");
                        if (!currentVersion.contains(UpdateVer[1])) {
                            Toast.makeText(MainActivity.this, "Found Update Please Restart App!", Toast.LENGTH_LONG).show();
                            mainActivityController.Close();
                        } else {

                        }

                    } else if (result.contains("False,")) {

                    } else {
                        if (result.contains("0")) {
                            Toast.makeText(MainActivity.this, "Incorrect username !", Toast.LENGTH_SHORT).show();
                        } else if (result.equals("-1")) {
                            Toast.makeText(MainActivity.this, "Network Error!", Toast.LENGTH_SHORT).show();
                            Toast.makeText(MainActivity.this, APIClient.URL, Toast.LENGTH_SHORT).show();

                        } else if (result.contains("1")) {

                            if (configMainActivity.Page.equals("Main")) {
                                //intent = new Intent(MainActivity.this, table_display.class);
                                intent = new Intent(MainActivity.this, ListAllMachine.class);
                                intent.putExtra("Type", "LinePage");
                            } else {

                                // intent = new Intent(MainActivity.this, ListAllMachine.class);
                                //intent.putExtra("MachineName", "M1");
                            }
                            startActivity(intent);
                        }
                    }
                    CheckDone = true;
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.e(TAG, t.getMessage());
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d("DEBUG HERE", "Destroy called");

    }

    @Override
    public void onBackPressed() {
        QuitTheApp();
    }

    @Override
    protected void onResume() {
        super.onResume();
        GetSharePreferences();
        //Remember set URL from SharedPreferences before initialize RestCrud
        restCrud = APIClient.getClient().create(RestCrud.class);
        CheckUpdate();
        AuthenticateLicense();
        checkPermission();
        txtusername.setText("11");
        txtpassword.setText("11");

        autoToSetting();

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_PassPMLogin:
                LoginFunction();
                break;

            case R.id.btnSetting:
                SettingFunction();
                break;

            case R.id.btnCapture:
                CamLib camLib = new CamLib();
                camLib.CaptureImage(MainActivity.this, "PM");
                break;
        }
    }

    private void QuitTheApp() {
        final ConfirmDialog cdd = new ConfirmDialog(MainActivity.this, "Confirm Exit?");
        cdd.show();
        cdd.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(final DialogInterface arg0) {
                int stat = cdd.isOkpressed();

                if (stat == 1) {
                    finish();
                } else {

                }
            }
        });
    }

    private void GetSharePreferences() {
        try {
            String URLpath = sharedpreferences.getString(configMainActivity.URL,null);
            if(URLpath != null){
                if(URLpath.substring(URLpath.length()-1).equals("/")){
                    APIClient.URL = "http://" + sharedpreferences.getString(configMainActivity.URL, null);
                }
                else{
                    APIClient.URL = "http://" + sharedpreferences.getString(configMainActivity.URL, null) + "/";
                }
            }
            else{
                Toast.makeText(this, "Error URL format", Toast.LENGTH_SHORT).show();
                return;
            }

            APIClient.Wifi = sharedpreferences.getString(configMainActivity.SerialNumber, null);

            if (APIClient.Wifi.equals("0")) {
                SharedPreferences.Editor editor = sharedpreferences.edit();
                //String restoredText = sharedpreferences.getString(URL, null);
                WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
                WifiInfo wInfo = wifiManager.getConnectionInfo();
                String macAddress = wInfo.getMacAddress();

                editor.putString(configMainActivity.SerialNumber, macAddress);
                editor.commit();
                APIClient.Wifi = sharedpreferences.getString(configMainActivity.SerialNumber, null);
            }
        } catch (Exception ex) {
            SharedPreferences.Editor editor = sharedpreferences.edit();
            //String restoredText = sharedpreferences.getString(URL, null);

            editor.putString(configMainActivity.URL, "URL");
            WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
            WifiInfo wInfo = wifiManager.getConnectionInfo();
            String macAddress = wInfo.getMacAddress();

            editor.putString(configMainActivity.SerialNumber, macAddress);
            editor.commit();

            APIClient.URL = "http://" + sharedpreferences.getString(configMainActivity.URL, null) + "/";
            APIClient.Wifi = sharedpreferences.getString(configMainActivity.SerialNumber, null);
        }

    }

    private void SettingFunction() {
//        LayoutInflater inflater = getLayoutInflater();
//        final Dialog customDialog;
//        final View customView = inflater.inflate(R.layout.custom_setting_dialog, null);
//
//        customDialog = new Dialog(MainActivity.this, R.style.CustomDialog);
//
//        Button btnOk = customView.findViewById(R.id.btnOk);
//        Button btnCancel = customView.findViewById(R.id.btnCancel);
//        final EditText text_username = customView.findViewById(R.id.text_username);
//        final EditText txt_Password = customView.findViewById(R.id.txt_Password);
//
//        btnOk.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//                username = text_username.getText().toString();
//                password = txt_Password.getText().toString();
//                if (username.equalsIgnoreCase(configMainActivity.Username) && password.equalsIgnoreCase(configMainActivity.Password)) {
//                    intent = new Intent(MainActivity.this, SettingActivity.class);
//                    startActivity(intent);
//                    customDialog.dismiss();
//                            /*Toast.makeText(MainActivity.this, "Please Enter UserName Password....", Toast.LENGTH_LONG).show();
//                            return;*/
//                } else {
//                    Toast.makeText(MainActivity.this, "Please Enter Correct UserName & Password....", Toast.LENGTH_LONG).show();
//                }
//
//
//            }
//        });
//
//        btnCancel.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                customDialog.dismiss();
//            }
//        });
//        customDialog.setContentView(customView);
//        customDialog.show();

        intent = new Intent(MainActivity.this, SettingActivity.class);
        startActivity(intent);
    }

    private void LoginFunction() {
        username = txtusername.getText().toString();
        password = txtpassword.getText().toString();
        if (username.equals("") || password.equals("")) {
            Toast.makeText(MainActivity.this, "Please Enter UserName Password....", Toast.LENGTH_LONG).show();
            return;
        }
        configMainActivity.Page = "Main";
        CheckDone = false;

        authenticate(username, password);
    }

    private void AuthenticateLicense(){
        LicenceDialog d = new LicenceDialog();
        d.setAppName(getApplicationInfo().loadLabel(getPackageManager()).toString());
        SharedPref sharedPref = new SharedPref();
        Lic = sharedPref.GetLic(this);
        com.projectx.libx.Utils.APIClient.URL = APIClient.URL;
        d.show(getFragmentManager(),"TAG");
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case 101:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }

            } else {
                //not granted
            }
            break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void checkPermission(){

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, 101);
            }

            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                return;
            }

            Log.d("qq"," serial number : " +new SerialNumberHashController(parentActivity).getSerialNumber());
        }else{
            Log.d("qq"," serial number : " +new SerialNumberHashController(parentActivity).getSerialNumber());
        }
    }

    private void autoToSetting(){
        String restoredText = sharedpreferences.getString(SharedPreferencesRestCrud.URL, null);
        if(restoredText != null && restoredText.equals(SharedPreferencesRestCrud.URL)){
            Intent clickM = new Intent(MainActivity.this,SettingActivity.class);
            startActivity(clickM);
        }
    }
}