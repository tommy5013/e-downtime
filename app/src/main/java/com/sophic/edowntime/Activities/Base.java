package com.sophic.edowntime.Activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.BatteryManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;

import com.sophic.edowntime.Helper.AutoLogout;
import com.sophic.edowntime.R;

public class Base extends AppCompatActivity {
    public TextView battery,wifi,tvWifiSSD;
    public AutoLogout autoLogout;

    public BroadcastReceiver broadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, 0);
            String batteryText = level + "%";
            battery.setText(batteryText);

            if (level >= 0 && level <= 25)
                battery.setCompoundDrawablesWithIntrinsicBounds(R.drawable.battery1, 0, 0, 0);
            else if (level >= 26 && level <= 50)
                battery.setCompoundDrawablesWithIntrinsicBounds(R.drawable.battery2, 0, 0, 0);
            else if (level >= 51 && level <= 75)
                battery.setCompoundDrawablesWithIntrinsicBounds(R.drawable.battery3, 0, 0, 0);
            else if (level >= 71 && level <= 100)
                battery.setCompoundDrawablesWithIntrinsicBounds(R.drawable.battery4, 0, 0, 0);
        }
    };

    public void wifi() {
        int numberOfLevels = 5;
        WifiManager wifiManager = (WifiManager) this.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        WifiInfo wifiInfo = wifiManager.getConnectionInfo();
        int level = WifiManager.calculateSignalLevel(wifiInfo.getRssi(), numberOfLevels);
        int rssi = wifiInfo.getRssi();
        tvWifiSSD.setText(wifiInfo.getSSID());
        int percentage = (int) ((level / numberOfLevels) * 100);

        if (rssi >= -20 && rssi <= -1)
            wifi.setText("100%");
        else if (rssi <= -51 && rssi >= -55)
            wifi.setText("90%");
        else if (rssi <= -56 && rssi >= -62)
            wifi.setText("80%");
        else if (rssi <= -63 && rssi >= -65)
            wifi.setText("75%");
        else if (rssi <= -66 && rssi >= -68)
            wifi.setText("70%");
        else if (rssi <= -69 && rssi >= -60)
            wifi.setText("60%");
        else if (rssi <= -75 && rssi >= -79)
            wifi.setText("50%");
        else if (rssi <= -80 && rssi >= -83)
            wifi.setText("30%");

        Log.d("wifi", "wifi: " + level);
        Log.d("wifi", "wifi: " + wifiInfo.getRssi());

        switch (level) {
            case 0: //wifi.setBackgroundResource(R.drawable.ic_signal_wifi_0_bar_teal_a700_24dp);
                wifi.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_signal_wifi_0_bar_teal_a700_24dp, 0, 0, 0);
                break;

            case 1: //wifi.setBackgroundResource(R.drawable.ic_signal_wifi_1_bar_teal_700_24dp);
                wifi.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_signal_wifi_1_bar_teal_700_24dp, 0, 0, 0);
                break;

            case 2: //wifi.setBackgroundResource(R.drawable.ic_signal_wifi_2_bar_teal_700_24dp);
                wifi.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_signal_wifi_2_bar_teal_700_24dp, 0, 0, 0);
                break;

            case 3: //wifi.setBackgroundResource(R.drawable.ic_signal_wifi_3_bar_teal_700_24dp);
                wifi.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_signal_wifi_3_bar_teal_700_24dp, 0, 0, 0);
                break;

            case 4: //wifi.setBackgroundResource(R.drawable.ic_signal_wifi_4_bar_teal_700_24dp);
                wifi.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_signal_wifi_4_bar_teal_700_24dp, 0, 0, 0);
                break;
        }
    }

    @Override
    public void onUserInteraction() {
        autoLogout.reset();
    }
}
