package com.sophic.edowntime.Activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.sophic.edowntime.Adapters.GetLineByMachineAdapter;
import com.sophic.edowntime.Dialogs.AddPMDialogFragment;
import com.sophic.edowntime.Dialogs.CoMachineDetailDialogFragment;
import com.sophic.edowntime.Helper.AutoLogout;
import com.sophic.edowntime.Helper.LoadingClass;
import com.sophic.edowntime.Model.CoModel;
import com.sophic.edowntime.Model.MyConfig;
import com.sophic.edowntime.Model.ProcessModel;
import com.sophic.edowntime.R;
import com.sophic.edowntime.RestAPI.APIClient;
import com.sophic.edowntime.RestAPI.RestCrud;

import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class COMainPage extends AppCompatActivity implements android.view.View.OnClickListener {
    public String TAG = COMainPage.class.getName();
    private RestCrud restCrud;
    private MyConfig myConfig = MyConfig.getInstance();
    private int CurrentData = 0;
    private int PrevData = 0;
    private String PageType = "";
    private String LineId = "";
    private Button btnBack,btnNewPm;
    private TextView tvTittle;
    private boolean isCreated = true;
    private LoadingClass LoadActivity;
    private AutoLogout autoLogout;
    private RecyclerView getLineByMachineRV;
    private COMainPage parentActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_pmline);
        parentActivity = this;
        //Button
        btnBack = findViewById(R.id.btnBack);
        btnNewPm = findViewById(R.id.btnNewPm);
        //TextView
        tvTittle = findViewById(R.id.tvStartDate);
        //RecyclerView
        getLineByMachineRV = findViewById(R.id.getLineByMachineRV);
        //OnClickListener
        btnBack.setOnClickListener(this);
        btnNewPm.setOnClickListener(this);

        //SetText
        tvTittle.setText("Change Over");
        btnNewPm.setText("NEW CO");

        tvTittle.setBackgroundResource(R.drawable.borderlinename);
        tvTittle.setTextColor(getResources().getColor(R.color.white));
        tvTittle.setTypeface(null, Typeface.BOLD);
        tvTittle.setPadding(10,8,10,8);

        PageType = getIntent().getStringExtra("Type");
        restCrud = APIClient.getClient().create(RestCrud.class);
        LoadActivity = new LoadingClass(this,1);
        autoLogout = new AutoLogout(this);
        autoLogout.start();

        if(PageType.equals("MachinePage")){
            LineId=getIntent().getStringExtra("LineId");
            String Content=getIntent().getStringExtra("LineName");

            Log.d("IMHERE",Content);
            tvTittle.setText(Content);
            GetPmMachine(LineId,false);
        }
        else{
            AddData(true);
        }
        isCreated = true;
    }


    public void GetPmMachine(String LineId,final boolean Init) {
        Call<List<CoModel.GetCoMachine>> call = restCrud.GetCoMachine(LineId);
        LoadActivity.show();
        call.enqueue(new Callback<List<CoModel.GetCoMachine>>() {

            @SuppressLint("WrongConstant")
            @Override
            public void onResponse(Call<List<CoModel.GetCoMachine>> call, Response<List<CoModel.GetCoMachine>> response) {
                if(response.isSuccessful()){
                    LoadActivity.dismiss();
                    final List<CoModel.GetCoMachine> body = response.body();
                    if (body == null) {
                        return;
                    }

                    Log.d("Res",response.body().toString());

                    List<CoModel.GetCoMachine> resource = response.body();
                    if(resource == null){
                        return;
                    }


                    int numofdata = 0;
                    try {
                        numofdata = resource.size();
                    }
                    catch (Exception ex){
                        Toast.makeText(COMainPage.this,"Error Get CO:"+ex.getMessage(), Toast.LENGTH_LONG).show();
                    }

                    CurrentData = numofdata;

                    if(Init)
                    {
                        PrevData= CurrentData;
                    }
                    else if(PrevData== CurrentData)
                    {
                        return;
                    }

                    else
                    {
                        PrevData= CurrentData;
                    }

                    GetLineByMachineAdapter getLineByMachineAdapter = new GetLineByMachineAdapter(parentActivity,body);
                    getLineByMachineRV.setLayoutManager(new GridLayoutManager(COMainPage.this, 4));
                    getLineByMachineRV.setNestedScrollingEnabled(false);
                    getLineByMachineRV.setAdapter(getLineByMachineAdapter);
                    getLineByMachineAdapter.setClickListener(new GetLineByMachineAdapter.ItemClickListener() {
                        @Override
                        public void onItemClick(View view, int position) {
                            String Id = String.valueOf(body.get(position).Id);
                            String lineName = body.get(position).LineNameMap;
                            Toast.makeText(parentActivity, "Please Wait Loading Data...", Toast.LENGTH_LONG).show();

                            if(!PageType.equals("MachinePage")){
                                Intent clickM = new Intent(COMainPage.this,COMainPage.class);
                                clickM.putExtra("Type","MachinePage");
                                clickM.putExtra("LineId",Id);
                                clickM.putExtra("LineName",lineName);
                                myConfig.setCurrentPage("PmMachine");
                                startActivity(clickM);
                            }
                            else{
                                if(parentActivity != null && !parentActivity.isFinishing()){
                                    FragmentManager fm = parentActivity.getSupportFragmentManager();
                                    CoMachineDetailDialogFragment coMachineDetailDialogFragment = CoMachineDetailDialogFragment.newInstance(parentActivity,body.get(position),body);
                                    coMachineDetailDialogFragment.show(fm, TAG);
                                }
                            }
                        }
                    });
                    getLineByMachineAdapter.refreshRecyclerView();
                }
                else{
                    LoadActivity.dismiss();
                    Toast.makeText(parentActivity, response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<CoModel.GetCoMachine>> call, Throwable t) {
                LoadActivity.dismiss();
                Log.d("err","error get");
            }
        });

    }

    public void AddData(final boolean Init) {
        Call<List<CoModel.GetCoLine>> call = restCrud.GetCoLine();
        LoadActivity.show();
        call.enqueue(new Callback<List<CoModel.GetCoLine>>() {
            @SuppressLint("WrongConstant")
            @Override
            public void onResponse(Call<List<CoModel.GetCoLine>> call, Response<List<CoModel.GetCoLine>> response) {

                if(response.isSuccessful()){
                    LoadActivity.dismiss();
                    final List<CoModel.GetCoLine> body = response.body();
                    if (body == null) {
                        return;
                    }

                    Log.d("Res",response.body().toString());

                    List<CoModel.GetCoLine> resource = response.body();

                    if(resource == null){
                        return;
                    }
                    CurrentData = resource.size();

                    if(Init)
                    {
                        PrevData= CurrentData;
                    }

                    else if(PrevData== CurrentData){
                        return;
                    }

                    else
                    {
                        PrevData= CurrentData;
                    }
                    GetLineByMachineAdapter getLineByMachineAdapter = new GetLineByMachineAdapter(body,parentActivity);
                    getLineByMachineRV.setLayoutManager(new GridLayoutManager(COMainPage.this, 4));
                    getLineByMachineRV.setNestedScrollingEnabled(false);
                    getLineByMachineRV.setAdapter(getLineByMachineAdapter);
                    getLineByMachineAdapter.setClickListener(new GetLineByMachineAdapter.ItemClickListener() {
                        @Override
                        public void onItemClick(View view, int position) {
                            String Id = String.valueOf(body.get(position).Id);
                            String lineName = body.get(position).LineNameMap;
                            Toast.makeText(parentActivity, "Please Wait Loading Data...", Toast.LENGTH_LONG).show();

                            if(!PageType.equals("MachinePage")){
                                Intent clickM = new Intent(COMainPage.this,COMainPage.class);
                                clickM.putExtra("Type","MachinePage");
                                clickM.putExtra("LineId",Id);
                                clickM.putExtra("LineName",lineName);
                                myConfig.setCurrentPage("PmMachine");
                                startActivity(clickM);
                            }
//                            else{
//                                Intent clickM = new Intent(COMainPage.this, CoMachineDetail.class);
//                                clickM.putExtra("PmId",Id);
//                                clickM.putExtra("MachineName",lineName);
//                                myConfig.setCurrentPage("CoMachineDetail");
//                                startActivityForResult(clickM, 100);
//
//                            }
                        }
                    });
                    getLineByMachineAdapter.refreshRecyclerView();
                }
                else{
                    LoadActivity.dismiss();
                    Toast.makeText(parentActivity, response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<CoModel.GetCoLine>> call, Throwable t) {
                Log.d("err","error get");
            }
        });
    }

    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.btnBack:
                if(myConfig.getCoCurrentPage() == "CoMachine"){
                    myConfig.setCoCurrentPage("CoLine");
                }
                else if(myConfig.getCoCurrentPage() == "CoLine"){
                    myConfig.setCoCurrentPage("CoMachine");
                }
                else{
                    myConfig.setCoCurrentPage("MainPage");
                }

                finish();
                break;

            case R.id.btnNewPm:
                if(parentActivity != null && !parentActivity.isFinishing()){
                    FragmentManager fm = parentActivity.getSupportFragmentManager();
                    AddPMDialogFragment addPMDialogFragment = AddPMDialogFragment.newInstance(parentActivity,"CO",LineId);
                    addPMDialogFragment.show(fm, TAG);
                }
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 100) {
            if(resultCode == Activity.RESULT_OK){
                GetPmMachine(LineId,true);
                myConfig.setCurrentPage("CoMachine");
                Log.d("TEST123","HERE");
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                // myConfig.getCurrentPage().equals("PmLine");
            }
        }
    }

    @Override
    public void onDestroy() {
        Log.d("TimerLog","PmListTimer Stop");
        autoLogout.stop();
        super.onDestroy();
    }

    @Override
    public void onResume(){
        super.onResume();
        restCrud = APIClient.getClient().create(RestCrud.class);
        autoLogout.start();
        Log.d("PMSTAT","Resume:  "+PageType);

        if(isCreated){
            if(PageType.equals("LinePage"))
            {
                AddData(true);
            }
            else{
                GetPmMachine(LineId,false);
            }
        }
        Map<String, ProcessModel> bobomap =  ProcessActivity.processModel.getProcessModelMap();
        bobomap.size();
        ProcessActivity.processModel.getProcessModelList();
    }

    @Override
    protected void onPause() {
        super.onPause();
        autoLogout.stop();
    }

    @Override
    public void onUserInteraction()
    {
        autoLogout.reset();
    }

}
