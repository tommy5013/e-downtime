package com.sophic.edowntime.Activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Handler;
import android.os.Vibrator;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.api.GoogleApiClient;
//import com.sophic.edowntime.Adapters.DataAdapter;
//import com.sophic.edowntime.Adapters.MyCustomAdapter;
import com.sophic.edowntime.Adapters.TableDisplayAdapter;
import com.sophic.edowntime.Model.Machine;
import com.sophic.edowntime.Dialogs.DisplayDetails_Popup_new;
import com.sophic.edowntime.Dialogs.TableDisplay_Popup_new;
import com.sophic.edowntime.Helper.AutoLogout;
import com.sophic.edowntime.Model.MachineModel;
import com.sophic.edowntime.Model.MyConfig;
import com.sophic.edowntime.R;
import com.sophic.edowntime.RestAPI.APIClient;
import com.sophic.edowntime.RestAPI.RestCrud;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Timer;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class table_display extends AppCompatActivity {
    public String TAG = table_display.class.getName();
    private Timer myTimer = new Timer();
    private Handler sleepHandler = new Handler();
    private Runnable sleep;
    private final int SLEEPTIME = 5000; // 10seconds
    private boolean sleepLCD = false;
    public  TextView txt_last,txt_ListHeader;
    public static TextView txt_TotalData,txt_UserName;
    private Button btn_NewAlert,btn_AlertHistory,btnBack;
    private Window mWindow;
    private ListView mList;
//    private DataAdapter adapter;
    private String[] tableData;
    private String memData;
    private int PrevDataCount = 0;
    private int CurrentDataCount = 0;
    private Vibrator vibrator;
    public  String MachineType;
    private table_display parentActivity;
    private static final String[] EMPTY_STRING_ARRAY = new String[0];
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private Intent intent;
    private GoogleApiClient client;
    private String getnewalert = "true";
    private String curdata= "";
    private String prevdata="";
    private Boolean NotificationStat = true;
    private MyConfig myConfig = MyConfig.getInstance();
    private String LineId="";
    private AutoLogout autoLogout;
    private RecyclerView tableDisplayRV;
    private RestCrud restCrud;
    private TableDisplayAdapter tableDisplayAdapter;
    private String LineName;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_table_display);
        parentActivity = this;
        vibrator = (Vibrator)this.getSystemService(Context.VIBRATOR_SERVICE);
        //Don't let device go in suspend
        memData = "";
        mWindow = this.getWindow();
        mWindow.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        txt_last = (TextView) findViewById(R.id.txt_last);
        txt_TotalData = (TextView) findViewById(R.id.txt_TotalData);
        txt_ListHeader = (TextView) findViewById(R.id.txt_ListHeader);
        txt_UserName = (TextView) findViewById(R.id.txt_UserName);
        btn_NewAlert =(Button) findViewById(R.id.btn_NewPM);
        btn_AlertHistory=(Button) findViewById(R.id.btn_AlertHistory);
        btnBack=(Button) findViewById(R.id.btnBack);
        tableDisplayRV = findViewById(R.id.tableDisplayRV);

        autoLogout = new AutoLogout(this);
        autoLogout.start();

        mList = (ListView) findViewById(R.id.data_list);

        Intent intent = getIntent();
        MachineType = intent.getStringExtra("MachineType");
        LineId=intent.getStringExtra("LineId");
        LineName = intent.getStringExtra("LineName");
        txt_ListHeader.setText(LineName + " (New Alert)");
        if(LineId.contains(".")){
            LineId = LineId.replace(".","_");
        }

        Log.d("PPhere", MachineType + LineId);

        intent = new Intent(this, table_display.class);
        restCrud =  APIClient.getClient().create(RestCrud.class);
        Initialization();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
        btn_NewAlert.setBackgroundResource(R.drawable.buttonselected);
        btn_AlertHistory.setBackgroundResource(R.drawable.buttonunselected2);

        btn_AlertHistory.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                getnewalert = "false";
                Initialization();
                btn_AlertHistory.setBackgroundResource(R.drawable.buttonselected);
                btn_NewAlert.setBackgroundResource(R.drawable.buttonunselected2);
                txt_ListHeader.setText(LineName +" (Assigned)");

            }
        });

        btn_NewAlert.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                prevdata = "";
                getnewalert = "true";
                Initialization();
                btn_NewAlert.setBackgroundResource(R.drawable.buttonselected);
                btn_AlertHistory.setBackgroundResource(R.drawable.buttonunselected2);
                txt_ListHeader.setText(LineName + " (New Alert)");
            }
        });

        btnBack.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
               parentActivity.onBackPressed();
            }
        });


        myConfig.setTableDisplayIsVisible(true);
    }


    @Override
    public void onUserInteraction() {
        if (sleepLCD) {
            WindowManager.LayoutParams params = mWindow.getAttributes();
            params.screenBrightness = 1;
            mWindow.setAttributes(params);
            sleepLCD = false;
        }
        sleepHandler.removeCallbacks(sleep);
        sleepHandler.postDelayed(sleep, SLEEPTIME);
        autoLogout.reset();
    }

    @Override
    protected void onPause() {
        //If you pause the app we need to cancel the timer otherwise it server communication
        //will continue in the background
        //here
        //myTimer.cancel();
        super.onPause();
        autoLogout.stop();

    }

    @Override
    protected void onResume() {
        super.onResume();
        autoLogout.start();
    }

    @Override
    protected void onDestroy() {
        //If the app is killed make sure to clear stuff
        myTimer.cancel();
        myTimer = null;
//        adapter = null;
        memData = null;
        tableData = null;
        myConfig.setTableDisplayIsVisible(false);
        autoLogout.stop();
        super.onDestroy();
    }

    public void Initialization() {

        if(getnewalert.equals("true"))
        {
            GetAlert();
        }
        else {
            GetAlertByPic();
        }
    }
    public void FillUpInformation(String data) {
        ReadJSONString(data);
    }


    private TextView NewTextView(String content) {
        TextView txtView = new TextView(table_display.this);
        txtView.setPadding(0, 0, 0, 0);
        txtView.setText(content);
        txtView.setTextColor(Color.BLACK);
        txtView.setTextSize(15);
        txtView.setGravity(Gravity.CENTER);
        return txtView;
    }

    private LinearLayout NewCell() {
        TableRow.LayoutParams llp = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT);
        LinearLayout cell = new LinearLayout(this);
        cell.setBackgroundColor(Color.TRANSPARENT);
        cell.setBackgroundResource(R.drawable.cell_header);
        cell.setLayoutParams(llp);//2px border on the right for the cell
        cell.setGravity(Gravity.CENTER);
        return cell;

    }

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    public Action getIndexApiAction() {
        Thing object = new Thing.Builder()
                .setName("table_display Page") // TODO: Define a title for the content shown.
                // TODO: Make sure this auto-generated URL is correct.
                .setUrl(Uri.parse("http://[ENTER-YOUR-URL-HERE]"))
                .build();
        return new Action.Builder(Action.TYPE_VIEW)
                .setObject(object)
                .setActionStatus(Action.STATUS_TYPE_COMPLETED)
                .build();
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        AppIndex.AppIndexApi.start(client, getIndexApiAction());
    }

    public static void UpdateTotal(int Count){
        txt_TotalData.setText("Total:"+Count);
    }

    public static int Total(){
        String ii = txt_TotalData.getText().toString();
        ii = ii.replace("Total:", "");

        int c = Integer.parseInt(ii);

        return  c;
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        AppIndex.AppIndexApi.end(client, getIndexApiAction());
        client.disconnect();
    }

    public void showNotification() {
        //PendingIntent pi = PendingIntent.getActivity(this, 0, new Intent(this, MainActivity.class), 0);
       /*
        PendingIntent pi = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_ONE_SHOT);
        Resources r = getResources();
        Notification notification = new NotificationCompat.Builder(this)
                .setTicker("x")
                .setSmallIcon(android.R.drawable.ic_menu_report_image)
                .setContentTitle("C")
                .setContentText("V")
                .setDefaults(Notification.DEFAULT_SOUND)
                .setContentIntent(pi)
                .setAutoCancel(true)
                .build();

        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        notificationManager.notify(0, notification);
        */

        Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), notification);
        r.play();
    }

    public class SetText {

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        table_display.this.overridePendingTransition(R.anim.slide_in_left,
                R.anim.slide_out_right);
    }

    private void ReadJSONString(String data){
        try {
            List<Machine> machineList = new ArrayList<>();
            machineList.clear();
            Machine machine = new Machine();
            JSONArray jsonArray = new JSONArray (data);
            for(int i = 0; i< jsonArray.length(); i++){
                JSONObject object = jsonArray.getJSONObject(i);
                //CPK data is diff with other, use statement to seperate them
                if(MachineType.equals("CPK")){
                    String id = object.get("Id").toString();
                    String machineType = object.get("MachineType").toString();
                    String actionTaken = object.get("ActionTaken").toString();
                    String product = object.get("Product").toString();
                    String repairTime = object.get("RepairTime").toString();
                    String waitingTime = object.get("WaitingTime").toString();
                    String location = object.get("Location").toString();
                    String cpk = object.get("Cpk").toString();
                    String equipmentId = object.get("EquipmentId").toString();
                    String picName = object.get("PICName").toString();
                    String status = object.get("Status").toString();
                    String timeIn = object.get("TimeIn").toString();
                    String errorCode = object.get("ErrorCode").toString();
                    String productName = object.get("productName").toString();
                    machine = new Machine(id,machineType,actionTaken,product,repairTime,waitingTime,location,cpk,equipmentId,picName,status,timeIn,errorCode,productName);
                    machineList.add(machine);
                }
                else{
                    String id = object.get("Id").toString();
                    String machineType = object.get("MachineType").toString();
                    String actionTaken = object.get("ActionTaken").toString();
                    String repairTime = object.get("RepairTime").toString();
                    String waitingTime = object.get("WaitingTime").toString();
                    String picName = object.get("PICName").toString();
                    String status = object.get("Status").toString();
                    String timeIn = object.get("TimeIn").toString();
                    String errorCode = object.get("ErrorCode").toString();
                    String productName = object.get("productName").toString();
                    String ModuleNo = object.get("ModuleNo").toString();
                    String cause = object.get("cause").toString();
                    String remedy = object.get("remedy").toString();
                    String onoff = object.get("onoff").toString();
                    String LineName = object.get("LineName").toString();
                    String MachineName = object.get("MachineName").toString();
                    String DataLineName = object.get("DataLineName").toString();
                    String DataMachineName = object.get("DataMachineName").toString();
                    String LineId = object.get("LineId").toString();
                    machine = new Machine(id,machineType,actionTaken,repairTime,waitingTime,picName,status,timeIn,errorCode,productName,ModuleNo,cause,remedy,onoff,LineName,MachineName,DataLineName,DataMachineName,LineId);
                    machineList.add(machine);
                }

                CurrentDataCount = machineList.size();

                if(PrevDataCount ==0){
                    Log.d("MNM","oldata");
                    PrevDataCount = CurrentDataCount;
                }
                if(PrevDataCount != CurrentDataCount){
                    PrevDataCount = CurrentDataCount;
                    Log.d("MNM","newData");
                    vibrator.vibrate(800);
                }
                txt_TotalData.setText("Total:"+machineList.size());
            }

            tableDisplayAdapter = new TableDisplayAdapter(parentActivity,machineList,getnewalert);
            tableDisplayRV.setLayoutManager(new GridLayoutManager(table_display.this, 1));
            tableDisplayRV.setNestedScrollingEnabled(false);
            tableDisplayRV.setAdapter(tableDisplayAdapter);
            tableDisplayAdapter.setClickListener(new TableDisplayAdapter.ItemClickListener() {
                @Override
                public void onItemClick(View view, int position, Machine machine1) {

                    switch (view.getId()){
                        case R.id.acceptBTN:
                            Button acceptBTN = findViewById(R.id.acceptBTN);
                            Log.d(TAG,"accept");
                            tableDisplayAdapter.setPosition(position);
                            acceptFunction(machine1,acceptBTN.getText().toString());
                            break;
                        case R.id.infoBTN:
                            Log.d(TAG,"Info");
                            infoFunction(machine1);
                            break;
                    }
                }
            });

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void acceptFunction(final Machine machine, final String btnName){
        final TableDisplay_Popup_new tableDisplayDialog;
        tableDisplayDialog = new TableDisplay_Popup_new(parentActivity,machine,btnName);
        //add = new NewAlert_Display_Popup((table_display) context,(table_display) context,rowData,btn_Accept);
        tableDisplayDialog.show();

        tableDisplayDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(final DialogInterface arg0) {

                int stat = tableDisplayDialog.isOkpressed();

                if(stat == 1){
                    Toast.makeText(parentActivity, "Updating Data Please Wait !" , Toast.LENGTH_SHORT).show();
                    MachineModel.AlertDone alertDone = new MachineModel.AlertDone();
                    Call<String> call;
                    restCrud = APIClient.getClient().create(RestCrud.class);
                    String body = MainActivity.username;
                    Call<String> call2 = null;

                    if(btnName.equals("Done")){
                        String Message = tableDisplayDialog.getMessage();


                        alertDone.AlertId = machine.Id;
                        alertDone.PICName= MainActivity.username;
                        alertDone.ActionTaken=Message;

                        call = restCrud.setDone(alertDone);
                        call.enqueue(new Callback<String>() {
                            @Override
                            public void onResponse(Call<String> call, Response<String> response) {
                                Toast.makeText(parentActivity, "Data Updated!" , Toast.LENGTH_SHORT).show();

                            }

                            @Override
                            public void onFailure(Call<String> call, Throwable t) {
                                Toast.makeText(parentActivity, "Error Updating Data" , Toast.LENGTH_SHORT).show();

                            }
                        });
                        //Toast.makeText(context, AlertMessage[0]+"|"+body+"|"+Message+"DONE !", Toast.LENGTH_SHORT).show();
                    }
                    else{
                        int CurrentCount = table_display.Total();
                        CurrentCount--;
                        table_display.UpdateTotal(CurrentCount);
                        call2 = restCrud.setAck(machine.Id+"|"+body);

                        call2.enqueue(new Callback<String>() {
                            @Override
                            public void onResponse(Call<String> call, Response<String> response) {

                                Log.d("err","sucess");
                                tableDisplayAdapter.updateList();
                                vibrator.vibrate(100);
                            }

                            @Override
                            public void onFailure(Call<String> call, Throwable t) {
                                Log.d("err",t.toString());
                                call.cancel();
                            }
                        });
                    }
                }
                else if (stat == -1){
                    Toast.makeText(parentActivity, "Please Select Message!", Toast.LENGTH_SHORT).show();
                }
                else if (stat == -2){
                    Toast.makeText(parentActivity, "Please Input Other Message!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void infoFunction(Machine machine){
        if(parentActivity != null && !parentActivity.isFinishing()){
            FragmentManager fm = parentActivity.getSupportFragmentManager();
            DisplayDetails_Popup_new dialogFragmentCustom = DisplayDetails_Popup_new.newInstance(parentActivity,machine);
            dialogFragmentCustom.show(fm, TAG);
        }
    }

    private void GetAlert(){
        Call<String> call = restCrud.GetAlert(MachineType+","+LineId);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if(response.isSuccessful()){
                    String result = response.body();
                    if (result.equals("-1"))
                        return;
                    SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss yyyy-MM-dd");
                    String currentDateandTime = sdf.format(new Date());
                    txt_last.setText("Last Update :" + currentDateandTime);
                    // txt_UserName.setText(" Username: " + MainActivity.username);

                    //txt_last.setText(MainActivity.username);

                    curdata=result;
                    Log.d("logsx",result);
                    if(prevdata.equals("")){
                        prevdata=result;
                        FillUpInformation(result);
                    }
                    else{
                        if(curdata.equals(prevdata)){

                        }
                        else {
                            if(getnewalert.contains("true")){
                                showNotification();

                            }
                            FillUpInformation(result);
                            prevdata=result;
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.e(TAG, t.getMessage());
            }
        });
    }


    private void GetAlertByPic(){
        Call<String> call = restCrud.GetAlertByPic(MachineType+","+ MainActivity.username+","+LineId);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if(response.isSuccessful()){
                    String result = response.body();
                    if (result.equals("-1"))
                        return;
                    SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss yyyy-MM-dd");
                    String currentDateandTime = sdf.format(new Date());
                    txt_last.setText("Last Update :" + currentDateandTime);
                    // txt_UserName.setText(" Username: " + MainActivity.username);

                    //txt_last.setText(MainActivity.username);

                    curdata=result;
                    Log.d("logsx",result);
                    if(prevdata.equals("")){
                        prevdata=result;
                        FillUpInformation(result);
                    }
                    else{
                        if(curdata.equals(prevdata)){

                        }
                        else {
                            if(getnewalert.contains("true")){
                                showNotification();
                            }
                            FillUpInformation(result);
                            prevdata=result;
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.e(TAG, t.getMessage());
            }
        });
    }
}

