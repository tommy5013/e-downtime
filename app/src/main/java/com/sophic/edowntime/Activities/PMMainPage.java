package com.sophic.edowntime.Activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.sophic.edowntime.Adapters.PMAdapter;
import com.sophic.edowntime.Dialogs.AddPMDialogFragment;
import com.sophic.edowntime.Dialogs.PMDialogFragment;
import com.sophic.edowntime.Helper.AutoLogout;
import com.sophic.edowntime.Helper.LoadingClass;
import com.sophic.edowntime.Model.MyConfig;
import com.sophic.edowntime.Model.PmModel;
import com.sophic.edowntime.R;
import com.sophic.edowntime.RestAPI.APIClient;
import com.sophic.edowntime.RestAPI.RestCrud;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PMMainPage extends AppCompatActivity implements android.view.View.OnClickListener {
    private String TAG = PMMainPage.class.getName();
    private TableLayout layoutMachineList;
    private RestCrud restCrud;
    private MyConfig myConfig = MyConfig.getInstance();
    private int CureentData=0;
    private int PrevData=0;
    private String PageType="";
    private String LineId="";
    private Button btnBack,btnNewPm;
    private TextView tvTittle;
    private boolean isCreated=false;
    private LoadingClass LoadActivity;
    private AutoLogout autoLogout;
    private PMMainPage parentActivity;
    private RecyclerView getLineByMachineRV;

    @Override
    public void onDestroy() {
        autoLogout.stop();
        Log.d("TimerLog","PmListTimer Stop");
        super.onDestroy();
    }

    @Override
    public void onResume(){
        super.onResume();
        autoLogout.start();

        restCrud = APIClient.getClient().create(RestCrud.class);

        Log.d("PMSTAT","Resume:  "+PageType);

        if(isCreated){
            if(PageType.equals("LinePage"))
            {
                AddData(true);
            }
            else{
                GetPmMachine(LineId,false);
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        autoLogout.stop();
    }

    @Override
    public void onUserInteraction()
    {
        autoLogout.reset();
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.layout_pmline);
        parentActivity = this;
        layoutMachineList = (TableLayout) findViewById(R.id.layoutMachineList);
        btnBack = (Button) findViewById(R.id.btnBack);
        btnNewPm = (Button) findViewById(R.id.btnNewPm);
        tvTittle = (TextView) findViewById(R.id.tvStartDate);
        getLineByMachineRV = (RecyclerView) findViewById(R.id.getLineByMachineRV);
        tvTittle.setText("Prevention Maintenance");

        tvTittle.setBackgroundResource(R.drawable.borderlinename);
        tvTittle.setTextColor(getResources().getColor(R.color.white));
        tvTittle.setTypeface(null, Typeface.BOLD);
        tvTittle.setPadding(10,8,10,8);

        btnBack.setOnClickListener(this);
        btnNewPm.setOnClickListener(this);

        autoLogout = new AutoLogout(this);
        autoLogout.start();

        PageType=getIntent().getStringExtra("Type");


        restCrud = APIClient.getClient().create(RestCrud.class);
        LoadActivity = new LoadingClass(this,1);

        if(PageType.equals("MachinePage")){
            LineId=getIntent().getStringExtra("LineId");
            String Content=getIntent().getStringExtra("LineName");

            Log.d("IMHERE",Content);
            tvTittle.setText(Content);
            GetPmMachine(LineId,false);
        }
        else{
            AddData(true);
        }
        isCreated=true;
    }

    public void GetPmMachine(String LineId,final boolean Init) {
        Call<List<PmModel.GetPmMachine>> call = restCrud.GetPmMachine(LineId);
        LoadActivity.show();
        call.enqueue(new Callback<List<PmModel.GetPmMachine>>() {
            @SuppressLint("WrongConstant")
            @Override
            public void onResponse(Call<List<PmModel.GetPmMachine>> call, Response<List<PmModel.GetPmMachine>> response) {

                if(response.isSuccessful()){
                    LoadActivity.dismiss();
                    final List<PmModel.GetPmMachine> body = response.body();
                    if (body == null) {
                        return;
                    }

                    Log.d("Res",response.body().toString());

                    List<PmModel.GetPmMachine> resource = response.body();
                    if(resource == null)
                        return;

                    int numofdata = resource.size();
                    CureentData = numofdata;

                    if(Init)
                    {
                        PrevData=CureentData;
                    }
                    else if(PrevData==CureentData)
                    {
                        return;
                    }

                    else
                    {
                        PrevData=CureentData;
                    }

                    PMAdapter getLineByMachineAdapter = new PMAdapter(parentActivity,body);
                    getLineByMachineRV.setLayoutManager(new GridLayoutManager(parentActivity, 4));
                    getLineByMachineRV.setNestedScrollingEnabled(false);
                    getLineByMachineRV.setAdapter(getLineByMachineAdapter);
                    getLineByMachineAdapter.setClickListener(new PMAdapter.ItemClickListener() {
                        @Override
                        public void onItemClick(View view, int position) {
                            String Id = String.valueOf(body.get(position).Id);
                            String lineName = body.get(position).LineNameMap;
                            Toast.makeText(parentActivity, "Please Wait Loading Data...", Toast.LENGTH_LONG).show();

                            if(!PageType.equals("MachinePage")){
                                Intent clickM = new Intent(parentActivity,PMMainPage.class);
                                clickM.putExtra("Type","MachinePage");
                                clickM.putExtra("LineId",Id);
                                clickM.putExtra("LineName",lineName);
                                myConfig.setCurrentPage("PmMachine");
                                startActivity(clickM);
                            }
                            else{
                                if(parentActivity != null && !parentActivity.isFinishing()){
                                    FragmentManager fm = parentActivity.getSupportFragmentManager();
                                    PMDialogFragment pmDialogFragment = PMDialogFragment.newInstance(parentActivity,body.get(position),body);
                                    pmDialogFragment.show(fm, TAG);
                                }

                            }
                        }
                    });
                    getLineByMachineAdapter.refreshRecyclerView();
                }
                else{
                    LoadActivity.dismiss();
                    Toast.makeText(parentActivity,response.message(),Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<PmModel.GetPmMachine>> call, Throwable t) {
                LoadActivity.dismiss();
                Log.d("err","error get");
            }
        });

    }

    public void AddData(final boolean Init) {
        Call<List<PmModel.GetPmLine>> call = restCrud.GetPmLine();
        LoadActivity.show();
        call.enqueue(new Callback<List<PmModel.GetPmLine>>() {
            @SuppressLint("WrongConstant")
            @Override
            public void onResponse(Call<List<PmModel.GetPmLine>> call, Response<List<PmModel.GetPmLine>> response) {
                if(response.isSuccessful()){
                    LoadActivity.dismiss();
                    final List<PmModel.GetPmLine> body = response.body();
                    if (body == null) {
                        return;
                    }

                    Log.d("Res",response.body().toString());

                    List<PmModel.GetPmLine> resource = response.body();


                    if(resource == null)
                        return;

                    int numofdata = resource.size();
                    CureentData = numofdata;

                    if(Init)
                    {
                        PrevData=CureentData;
                    }

                    else if(PrevData==CureentData){
                        return;
                    }

                    else
                    {
                        PrevData=CureentData;
                    }

                    PMAdapter getLineByMachineAdapter = new PMAdapter(body,parentActivity);
                    getLineByMachineRV.setLayoutManager(new GridLayoutManager(parentActivity, 4));
                    getLineByMachineRV.setNestedScrollingEnabled(false);
                    getLineByMachineRV.setAdapter(getLineByMachineAdapter);
                    getLineByMachineAdapter.setClickListener(new PMAdapter.ItemClickListener() {
                        @Override
                        public void onItemClick(View view, int position) {

                            String Id = String.valueOf(body.get(position).Id);
                            String lineName = body.get(position).LineNameMap;
                            Toast.makeText(parentActivity, "Please Wait Loading Data...", Toast.LENGTH_LONG).show();

                            if(!PageType.equals("MachinePage")){

                                //myConfig.PmListTimer.cancel();
                                Intent clickM = new Intent(parentActivity,PMMainPage.class);
                                clickM.putExtra("Type","MachinePage");
                                clickM.putExtra("LineId",Id);
                                clickM.putExtra("LineName",lineName);
                                myConfig.setCurrentPage("PmMachine");
                                startActivity(clickM);
                            }
//                            else{
//                                //myConfig.PmListTimer.cancel();
//                                Intent clickM = new Intent(parentActivity, PmMachineDetail.class);
//                                clickM.putExtra("PmId",Id);
//                                clickM.putExtra("MachineName",lineName);
//                                //startActivity(clickM);
//                                myConfig.setCurrentPage("PmMachineDetail");
//                                startActivityForResult(clickM, 100);
//
//                            }
                        }
                    });
                    getLineByMachineAdapter.refreshRecyclerView();
                }
                else{
                    LoadActivity.dismiss();
                    Toast.makeText(PMMainPage.this, response.message(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<PmModel.GetPmLine>> call, Throwable t) {
                LoadActivity.dismiss();
                Log.d("err","error get");
            }
        });
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnBack:
                if(myConfig.getCurrentPage() == "PmMachine"){
                    myConfig.setCurrentPage("PmLine");
                }
                else if(myConfig.getCurrentPage() == "PmLine"){
                    myConfig.setCurrentPage("PmMachine");
                }
                else{
                    myConfig.setCurrentPage("MainPage");
                }

                finish();
                break;

            case R.id.btnNewPm:
                if(parentActivity != null && !parentActivity.isFinishing()){
                    FragmentManager fm = parentActivity.getSupportFragmentManager();
                    AddPMDialogFragment addPMDialogFragment = AddPMDialogFragment.newInstance(parentActivity,"PM",LineId);
                    addPMDialogFragment.show(fm, TAG);
                }
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 100) {
            if(resultCode == Activity.RESULT_OK){
                GetPmMachine(LineId,true);
                myConfig.setCurrentPage("PmMachine");

            }
            if (resultCode == Activity.RESULT_CANCELED) {
               // myConfig.getCurrentPage().equals("PmLine");
            }
        }
    }
}
