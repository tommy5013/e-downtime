package com.sophic.edowntime.Activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.projectx.libx.Controller.SnackBarLib;
import com.sophic.edowntime.Helper.Configs.SharedPreferencesRestCrud;
import com.sophic.edowntime.Controllers.SerialNumberHashController;
import com.sophic.edowntime.Controllers.ToastController;
import com.sophic.edowntime.Helper.LoadingClass;
import com.sophic.edowntime.RestAPI.APIClient;
import com.sophic.edowntime.R;
import com.sophic.edowntime.RestAPI.RestCrud;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class SettingActivity extends AppCompatActivity implements android.view.View.OnClickListener, CompoundButton.OnCheckedChangeListener {
    private Intent intent;
    private Vibrator vibrator;
    private EditText txtURL,txtBarcode;
    private Button b1,b2;
    private SharedPreferences sharedpreferences;
    private SharedPreferences.Editor editor;
    private boolean isFrontEnd = true;
    private android.support.v7.widget.SwitchCompat mSwitch;
    private boolean isUrlCorrect = false;
    private SettingActivity parentActivity;
    private TextView mSwitchTV;
    private LoadingClass loadingClass = new LoadingClass();
    private SnackBarLib snackBarLib = new SnackBarLib();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setContentView(R.layout.activity_setting);
        parentActivity = this;
        //Service
        vibrator = (Vibrator)this.getSystemService(Context.VIBRATOR_SERVICE);
        //EditText
        txtURL=findViewById(R.id.txtURL);
        txtBarcode =findViewById(R.id.txtBarcode);
        //Button
        b1=findViewById(R.id.btn_SettingSave);
        b2=findViewById(R.id.btnSettingBack);
        //Switch
        mSwitch = findViewById(R.id.mSwitch);
        //TextView
        mSwitchTV = findViewById(R.id.mSwitchTV);
        //OnclickListener
        b1.setOnClickListener(this);
        b2.setOnClickListener(this);
        //OnCheckChangeListener
        mSwitch.setOnCheckedChangeListener(this);

        //SharePreferences
        sharedpreferences = getSharedPreferences(SharedPreferencesRestCrud.MyPREFERENCES, Context.MODE_PRIVATE);

        SharedPreferencesPopulate();

        loadingClass = new LoadingClass(this,1);
        snackBarLib.initSnackBar(parentActivity,findViewById(R.id.root));

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_SettingSave:
                CheckTheUrl();
                break;
            case R.id.btnSettingBack:
                if(!isUrlCorrect){
                    this.finishAffinity();
                }
                else{
                    this.onBackPressed();
                }
                break;
        }
    }

    private void SharedPreferencesPopulate(){
        String restoredText = sharedpreferences.getString(SharedPreferencesRestCrud.URL, null);
        String BarcodeText = sharedpreferences.getString(SharedPreferencesRestCrud.SerialNumber, null);
        boolean mIsFrontend = sharedpreferences.getBoolean(SharedPreferencesRestCrud.IS_FRONTEND, true);
        boolean mIsUrlCorrect = sharedpreferences.getBoolean(SharedPreferencesRestCrud.IS_URL_CORRECT, false);
        txtURL.setText(restoredText);
//        txtBarcode.setText(BarcodeText);
        txtBarcode.setText(new SerialNumberHashController(parentActivity).getSerialNumber());
        if(mIsFrontend){
            mSwitch.setChecked(false);
            mSwitchTV.setText("Frontend On :");
        }
        else{
            mSwitch.setChecked(true);
            mSwitchTV.setText("Backend On :");
        }
        if(mIsUrlCorrect){
            isUrlCorrect = true;
        }
        else{
            isUrlCorrect = false;
        }
        if(restoredText != null && restoredText.equals(SharedPreferencesRestCrud.URL)){
            isUrlCorrect = false;
        }
    }

    private void SaveSettingFunction(){
        editor = sharedpreferences.edit();
        editor.putString(SharedPreferencesRestCrud.URL, txtURL.getText().toString());
        editor.putString(SharedPreferencesRestCrud.SerialNumber, txtBarcode.getText().toString());
        editor.putBoolean(SharedPreferencesRestCrud.IS_FRONTEND, isFrontEnd);
        editor.putBoolean(SharedPreferencesRestCrud.IS_URL_CORRECT, isUrlCorrect);
        Log.d("Asd",String.valueOf(isFrontEnd));
        editor.apply();
    }

    private void CheckTheUrl(){
        CheckURLFormat();
        RestCrud restCrud = APIClient.getClient().create(RestCrud.class);
        Call<String> call = restCrud.Test();
        loadingClass.show();
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if(response.isSuccessful()){
                    String value = response.body();
                    String finalValue = value.replace("\"", "");
                    if(finalValue.equals("Test Received")){
                        isUrlCorrect = true;
                        SaveSettingFunction();
                        snackBarLib.show("Saved....",snackBarLib.SUCCESSFUL);
                    }
                    else{
                        isUrlCorrect = false;
                        snackBarLib.show("Invalid server URL.",snackBarLib.WARNING);
                    }
                }
                else{
                    snackBarLib.show("Failed to save. Invalid server URL.",snackBarLib.WARNING);
                }
                loadingClass.dismiss();
            }
            @Override
            public void onFailure(Call<String> call, Throwable t) {
                isUrlCorrect = false;
                loadingClass.dismiss();
                snackBarLib.show("Error, cannot connect to server!",snackBarLib.ERROR);
            }
        });
    }

    private void CheckURLFormat(){
        String URLpath = txtURL.getText().toString().trim();
        if(!TextUtils.isEmpty(URLpath)){
            if(URLpath.substring(URLpath.length()-1).equals("/")){
                APIClient.URL = "http://" + URLpath;
            }
            else{
                APIClient.URL = "http://" + URLpath + "/";
            }
        }
        else{
            Toast.makeText(this, "Error URL format", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        switch (buttonView.getId()){
            case R.id.mSwitch:
                if(isChecked){
                    isFrontEnd = false;
                    mSwitchTV.setText("Backend On :");
                }
                else{
                    isFrontEnd = true;
                    mSwitchTV.setText("Frontend On :");
                }
                break;
        }
    }

}


