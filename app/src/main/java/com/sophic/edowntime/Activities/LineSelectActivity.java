package com.sophic.edowntime.Activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.BatteryManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.sophic.edowntime.Adapters.LineListAdapter;
import com.sophic.edowntime.Adapters.LineSelectAdapter;
import com.sophic.edowntime.Controllers.MessageDialogController;
import com.sophic.edowntime.Helper.AutoLogout;
import com.sophic.edowntime.Helper.ConfirmDialog;
import com.sophic.edowntime.Model.MachineModel;
import com.sophic.edowntime.Model.MyConfig;
import com.sophic.edowntime.RestAPI.APIClient;
import com.sophic.edowntime.R;
import com.sophic.edowntime.RestAPI.RestCrud;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LineSelectActivity extends AppCompatActivity {
    public String TAG = LineSelectActivity.class.getName();
//    private GridView gridView;
    private TextView battery,wifi,tvWifiSSD,tvLastUpdate,displayuserid,tvWifiSSD3;
    private Button btnLogout;
    private Intent intent;

    private List<String> list;
    private RestCrud RestCrud;
    private LineListAdapter arrayadapter;
    private MyConfig myConfig = MyConfig.getInstance();

    private AutoLogout autoLogout;

    private LineSelectActivity parentActivity;

    private List<String> Machines = new ArrayList<String>();

    private RecyclerView lineSelectRV;
    private boolean isWeakWifiSignal = false;

    private BroadcastReceiver batteryinfo = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            int level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL,0);
            battery.setText(String.valueOf(level)+"%");

            if (level>=0 && level<=25)
                battery.setCompoundDrawablesWithIntrinsicBounds(R.drawable.battery1, 0, 0, 0);
            else if(level>=26 && level<=50)
                battery.setCompoundDrawablesWithIntrinsicBounds(R.drawable.battery2, 0, 0, 0);
            else if(level>=51 && level<=75)
                battery.setCompoundDrawablesWithIntrinsicBounds(R.drawable.battery3, 0, 0, 0);
            else if(level>=71 && level<=100)
                battery.setCompoundDrawablesWithIntrinsicBounds(R.drawable.battery4, 0, 0, 0);
        }
    };

    @Override
    public void onResume()
    {
        super.onResume();
        wifi();
        autoLogout.start();


    }

    public void onDestroy() {

        Log.d("call","call");
        MyConfig.TaskListPM.removeCallbacks(UpdatePMCO);
        MyConfig.TaskListMachine.removeCallbacks(runnableCode);
        unregisterReceiver(batteryinfo);
        autoLogout.stop();
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
        autoLogout.stop();
    }

    @Override
    public void onUserInteraction()
    {
        autoLogout.reset();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_line_select);
//        gridView = (GridView) findViewById(R.id.gridmain);
        parentActivity = this;
        Button btnu = (Button) findViewById(R.id.btnu);
        Button btn2 = (Button) findViewById(R.id.btn2);
        btnLogout = (Button) findViewById(R.id.btnLogout2);
        battery = (TextView) findViewById(R.id.battery2);
        tvWifiSSD = (TextView) findViewById(R.id.tvWifiSSD2);
        tvWifiSSD3 = (TextView) findViewById(R.id.tvWifiSSD3);
        wifi = (TextView) findViewById(R.id.wifi2);
        tvLastUpdate = (TextView) findViewById(R.id.tvLastUpdate2);
        displayuserid = (TextView) findViewById(R.id.displayuserid2);
        lineSelectRV = (RecyclerView) findViewById(R.id.lineSelectRV);
        this.registerReceiver(this.batteryinfo,new IntentFilter(Intent.ACTION_BATTERY_CHANGED));


        autoLogout = new AutoLogout(this);
        autoLogout.start();



        MyConfig.LineList =  new ArrayList<String>();

        arrayadapter = new LineListAdapter(LineSelectActivity.this, Machines);
        arrayadapter.notifyDataSetChanged();

        displayuserid.setText("USERNAME:"+MainActivity.username);

        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss yyyy-MM-dd");
        String currentDateandTime = sdf.format(new Date());
        tvLastUpdate.setText("Last Update :" + currentDateandTime);

        if(savedInstanceState==null){
            MyConfig.TaskListMachine = new Handler();
            MyConfig.TaskListMachine.post(runnableCode);
            MyConfig.TaskListPM = new Handler();
            MyConfig.TaskListPM.post(UpdatePMCO);
        }

        GetUserLine();
        Update();

        btnu.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                GetUserLine();
                Update();
                arrayadapter.getCount();

            }

        });

        btn2.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                // Machines.clear();
                String lineName = "";
                for(String e : MyConfig.LineList){
                    lineName +=  e+",";
                }
                if(lineName.length() < 1){
                    Toast.makeText(LineSelectActivity.this, "Please Select Line", Toast.LENGTH_LONG).show();
                    return;
                }
                lineName = lineName.substring(0, lineName.length() - 1);
                MyConfig.Lines = lineName;


                SaveLines(lineName);

                intent = new Intent(LineSelectActivity.this, ListAllMachine.class);
                intent.putExtra("lineName","lineName");
                startActivity(intent);
            }

        });

        btnLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                    final ConfirmDialog cdd=new ConfirmDialog(LineSelectActivity.this,"Confirm Logout?");

                    cdd.show();

                    cdd.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(final DialogInterface arg0) {
                            int stat = cdd.isOkpressed();

                            if(stat==1){
                                UpdateLogout();
                                finish();
                            }
                            else{

                            }
                        }
                    });

            }
        });



    }


    public void wifi(){

        WifiManager wifiManager = (WifiManager) this.getApplicationContext().getSystemService(Context.WIFI_SERVICE);

        int numberOfLevels = 5;
        WifiInfo wifiInfo = wifiManager.getConnectionInfo();
        int level = WifiManager.calculateSignalLevel(wifiInfo.getRssi(), numberOfLevels);
        int rssi = wifiInfo.getRssi();
        tvWifiSSD.setText(wifiInfo.getSSID());
        int percentage = (int) ((level/numberOfLevels)*100);

        List<WifiConfiguration> configuredList = wifiManager.getConfiguredNetworks();

       /* List<String> ssidList = new ArrayList<>();
        String x = "";
        for(WifiConfiguration config : configuredList) {
            ssidList.add(config.SSID);
            x=x+config.SSID+"\n";
        }*/
    tvWifiSSD3.setText(wifiInfo.getSSID());

        tvWifiSSD3.setMovementMethod(new ScrollingMovementMethod());

        if (rssi>=-20 && rssi<=-1){
            wifi.setText("100%");
            isWeakWifiSignal = false;
        }
        else if(rssi<=-51 && rssi>=-55){
            wifi.setText("90%");
            isWeakWifiSignal = false;
        }
        else if(rssi<=-56 && rssi>=-62){
            wifi.setText("80%");
            isWeakWifiSignal = false;
        }
        else if(rssi<=-63 && rssi>=-65){
            wifi.setText("75%");
            isWeakWifiSignal = false;
        }
        else if(rssi<=-66 && rssi>=-68){
            wifi.setText("70%");
            isWeakWifiSignal = false;
        }
        else if(rssi<=-69 && rssi>=-60){
            wifi.setText("60%");
            isWeakWifiSignal = false;
        }
        else if(rssi<=-75 && rssi>=-79){
            wifi.setText("50%");
            isWeakWifiSignal = false;
        }
        else if(rssi<=-80 && rssi>=-83){
            wifi.setText("30%");
            isWeakWifiSignal = true;
        }
        else if(rssi<-80){
            isWeakWifiSignal = true;
        }

        Log.d("wifi", "wifi: "+level);
        Log.d("wifi", "wifi: "+wifiInfo.getRssi());

        switch (level)
        {
            case 0: //wifi.setBackgroundResource(R.drawable.ic_signal_wifi_0_bar_teal_a700_24dp);
                wifi.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_signal_wifi_0_bar_teal_a700_24dp, 0, 0, 0);

                break;

            case 1: //wifi.setBackgroundResource(R.drawable.ic_signal_wifi_1_bar_teal_700_24dp);
                wifi.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_signal_wifi_1_bar_teal_700_24dp, 0, 0, 0);

                break;

            case 2: //wifi.setBackgroundResource(R.drawable.ic_signal_wifi_2_bar_teal_700_24dp);
                wifi.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_signal_wifi_2_bar_teal_700_24dp, 0, 0, 0);

                break;

            case 3: //wifi.setBackgroundResource(R.drawable.ic_signal_wifi_3_bar_teal_700_24dp);
                wifi.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_signal_wifi_3_bar_teal_700_24dp, 0, 0, 0);

                break;

            case 4: //wifi.setBackgroundResource(R.drawable.ic_signal_wifi_4_bar_teal_700_24dp);
                wifi.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_signal_wifi_4_bar_teal_700_24dp, 0, 0, 0);

                break;
        }


    }

    Handler updatecheck;
    Handler wificheck;

    private Runnable runnableCode = new Runnable() {
        @Override
        public void run() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {

                    SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss yyyy-MM-dd");
                    String currentDateandTime = sdf.format(new Date());
                    tvLastUpdate.setText("Last Update :" + currentDateandTime);

                    // }
                    //Log.d("ISRUN","LSITRUNING");
                }
            });
            //updatecheck.postDelayed(runnableCode, 2000);
            MyConfig.TaskListMachine.postDelayed(runnableCode, 2000);
        }
    };

    private Runnable UpdatePMCO = new Runnable() {
        @Override
        public void run() {
            wifi();
            if(isWeakWifiSignal){
                new MessageDialogController().showMessageAlert(parentActivity,"Error","Failed process. Your wifi signal strength is low. Please connect to a better wifi network.",TAG);
            }
            //wificheck.postDelayed(UpdatePMCO, 3000);
            MyConfig.TaskListPM.postDelayed(UpdatePMCO, 3000);
        }
    };

    private void SaveLines(String lineName)
    {
        MachineModel.UserInfo userInfo = new MachineModel().new UserInfo();
        userInfo.Username = MainActivity.username;
        userInfo.Password = "";
        userInfo.UserType="type";
        userInfo.SelectedLines = lineName;
        Call<String> call = RestCrud.SaveLine(userInfo);
        call.enqueue(new Callback<String>() {


            @Override
            public void onResponse(Call<String> call, Response<String> response) {

               String body = response.body();

            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                //Toast.makeText(LineSelectActivity.this, "Error", Toast.LENGTH_LONG).show();
            }
        });
    }


    private void Update(){
        //DataLoading = true;
        RestCrud = APIClient.getClient().create(RestCrud.class);

        Call<List<MachineModel.LineList>> call = RestCrud.GetLineList();

        call.enqueue(new Callback<List<MachineModel.LineList>>() {


            @Override
            public void onResponse(Call<List<MachineModel.LineList>> call, Response<List<MachineModel.LineList>> response) {
                List<MachineModel.LineList> body = response.body();

                Machines.clear();
                for (int i=0;i<body.size();i++){
                    Machines.add(body.get(i).LineName+","+body.get(i).PicCount);
                }
                LineSelectAdapter lineSelectAdapter = new LineSelectAdapter(parentActivity,body);
                lineSelectRV.setLayoutManager(new GridLayoutManager(LineSelectActivity.this, 3));
                lineSelectRV.setNestedScrollingEnabled(false);
                lineSelectRV.setAdapter(lineSelectAdapter);


//                gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//                    public void onItemClick(AdapterView<?> parent, View v,
//                                            int position, long id) {
//                        Toast.makeText(LineSelectActivity.this,"AA", Toast.LENGTH_SHORT).show();
//
//                    }
//                });
//
//                gridView.setAdapter(arrayadapter);


            }

            @Override
            public void onFailure(Call<List<MachineModel.LineList>> call, Throwable t) {

            }
        });
    }


    private void GetUserLine(){
        //DataLoading = true;
        RestCrud = APIClient.getClient().create(RestCrud.class);

        Call<String> call = RestCrud.GetUserLine(MainActivity.username);

        call.enqueue(new Callback<String>() {

            @Override
            public void onResponse(Call<String> call, Response<String> response) {

                if(response.isSuccessful()){
                    MyConfig.UserLineList.clear();

                    String body = response.body();
                    String[] _body = body.split(",");

                    for (String item:_body){
                        MyConfig.UserLineList.add(item);
                    }
                }

            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Toast.makeText(LineSelectActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Log.e("LineSelectActivity",t.getMessage());
            }
    });
    }


    private void UpdateLogout(){

        MachineModel.UserInfo userInfo = new MachineModel().new UserInfo();
        userInfo.Username=MainActivity.username;
        userInfo.Password = "";
        userInfo.UserType="type";
        Call<MachineModel.UserInfo> call = RestCrud.UserLogout(userInfo);
        call.enqueue(new Callback<MachineModel.UserInfo>() {


            @Override
            public void onResponse(Call<MachineModel.UserInfo> call, Response<MachineModel.UserInfo> response) {

                MachineModel.UserInfo body = response.body();

            }

            @Override
            public void onFailure(Call<MachineModel.UserInfo> call, Throwable t) {
                //Toast.makeText(LineSelectActivity.this, "Error", Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onBackPressed() {
        final ConfirmDialog cdd=new ConfirmDialog(LineSelectActivity.this,"Confirm Logout?");

        cdd.show();

        cdd.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(final DialogInterface arg0) {
                int stat = cdd.isOkpressed();

                if(stat==1){
                    UpdateLogout();
                    finish();
                }
                else{

                }
            }
        });
    }


}
