package com.sophic.edowntime.Activities;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.widget.Button;

import com.sophic.edowntime.Adapters.ProcessAdapter;
import com.sophic.edowntime.Model.ProcessModel;
import com.sophic.edowntime.R;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

public class ProcessActivity extends AppCompatActivity implements View.OnClickListener {
    private ProcessActivity parentActivity;
    private static Map<String,Handler> handlerMap = new HashMap<>();
    private static Map<String,Runnable> runnableMap = new HashMap<>();
    public static ProcessModel processModel = new ProcessModel();
    private static  Map<String, ProcessModel> processModelMap = new HashMap<>();
    private RecyclerView processRV;
    private Button btnBack;
    private ProcessAdapter processAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_process);
        parentActivity = this;

        processRV = findViewById(R.id.processRV);
        btnBack = findViewById(R.id.btnBack);

        btnBack.setOnClickListener(this);
        initialRecycleView();
    }

    public static void bobo() {
        //If you want to print stack trace on console than use dumpStack() method
        System.err.println("Stack trace of current thread using dumpStack() method");
        Thread.currentThread();
        Thread.dumpStack();

        //This is another way to print stack trace from current method
        System.err.println("Printing stack trace using printStackTrace() method of Throwable ");
        new Throwable().printStackTrace();

        //If you want stack trace as StackTraceElement in program itself than
        //use getStackTrace() method of Thread class
        StackTraceElement[] stackTrace = Thread.currentThread().getStackTrace();

        //Once you get StackTraceElement you can also print it to console
        System.err.println("displaying Stack trace from StackTraceElement in Java");
        for (StackTraceElement st : stackTrace) {
            //  System.err.println(st);
        }
    }

    public static void printTrack() {
        Set<Thread> threadSet = Thread.getAllStackTraces().keySet();
        for (Thread thread : threadSet) {
            StackTraceElement[] st = thread.getStackTrace();
            if (st == null) {
                Log.d("ProcessStatus", "无堆栈...");
                thread.interrupt();
                return;
            }
            for (StackTraceElement e : st) {
                String status = "Class Name: " + e.getClassName() + "\n" +
                        "Method Name: " + e.getMethodName() + "\n" +
                        "Line number: " + e.getLineNumber() + "\n" +
                android.os.Process.getThreadPriority(android.os.Process.myTid());
                android.os.Process.myTid();
                if(e.getClassName().contains(MainActivity.PACKAGE_NAME) && e.getMethodName().equals("run")){
                    if(Looper.myLooper() == Looper.getMainLooper()){
                        Log.d("ProcessStatus", status);
                        thread.interrupt();

                    }
                }
            }
        }


//        StackTraceElement[] st = Thread.currentThread().getStackTrace();
//        if(st==null){
//            System.out.println("无堆栈...");
//            return;
//        }
//        StringBuffer sbf =new StringBuffer();
//        for(StackTraceElement e:st){
////            if(sbf.length()>0){
////                sbf.append(" <- ");
////                sbf.append(System.getProperty("line.separator"));
////            }
////            sbf.append(java.text.MessageFormat.format("{0}.{1}() {2}"
////                    ,e.getClassName()
////                    ,e.getMethodName()
////                    ,e.getLineNumber()));
//            String status = "Class Name: " +e.getClassName() +"\n"+
//                    "Method Name: " + e.getMethodName() + "\n"+
//                    "Line number: " + e.getLineNumber();
//            Log.d("ProcessStatus",status);
//        }
//        android.util.Log.i("wangqx", sbf.toString());
    }

    public static void SaveThread(Handler handler, Runnable runnable, String name,String tag){
        handlerMap.put(name,handler);
        runnableMap.put(name,runnable);
        processModel = new ProcessModel(name,tag,processModel.STATUS_RUNNING, Calendar.getInstance().getTime());
        processModelMap.put(name,processModel);
        processModel.setProcessModelMap(processModelMap);
        processModel.setProcessModelList(new ArrayList<ProcessModel>(processModel.getProcessModelMap().values()));
    }

    public static void StopThread(String name){
        Runnable runnable = runnableMap.get(name);
        Handler handler = handlerMap.get(name);
        if(handler != null){
            if(runnable != null){
                handler.removeCallbacks(runnable);
                try{
                    ProcessActivity.processModel.getProcessModelMap().get(name).Status = processModel.STATUS_STOP;
                }
                catch (Exception e){

                }
            }
        }
    }

    private void initialRecycleView(){
        if(ProcessActivity.processModel.getProcessModelList() != null){
            processAdapter = new ProcessAdapter(parentActivity, ProcessActivity.processModel.getProcessModelList());
            processRV.setLayoutManager(new GridLayoutManager(ProcessActivity.this, 1));
            processRV.setNestedScrollingEnabled(false);
            processRV.setAdapter(processAdapter);
            processAdapter.setClickListener(new ProcessAdapter.ItemClickListener() {
                @Override
                public void onItemClick(View view, int position, String processName) {
                    StopThread(processName);
                }
            });
        }
    }

    private void UpdateRecycleView(){
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable(){
            public void run(){
                processAdapter.refreshRecycleView(ProcessActivity.processModel.getProcessModelList());
                Log.d("Time delay", "1");
                handler.postDelayed(this, 3000);
            }
        }, 1000);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnBack:
                onBackPressed();
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        UpdateRecycleView();
    }
}
