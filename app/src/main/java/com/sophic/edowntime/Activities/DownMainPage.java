package com.sophic.edowntime.Activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.sophic.edowntime.Adapters.DownAdapter;
import com.sophic.edowntime.Dialogs.AddPMDialogFragment;
import com.sophic.edowntime.Dialogs.DownDialogFragment;
import com.sophic.edowntime.Helper.AutoLogout;
import com.sophic.edowntime.Helper.LoadingClass;
import com.sophic.edowntime.Model.DownTimeModel;
import com.sophic.edowntime.Model.MyConfig;
import com.sophic.edowntime.R;
import com.sophic.edowntime.RestAPI.APIClient;
import com.sophic.edowntime.RestAPI.RestCrud;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DownMainPage extends AppCompatActivity implements android.view.View.OnClickListener {
    public String TAG = DownMainPage.class.getName();
    private RestCrud restCrud;
    private MyConfig myConfig = MyConfig.getInstance();
    private int CureentData=0;
    private int PrevData=0;
    private String PageType="";
    private String LineId="";
    private Button btnBack,btnNewPm;
    private TextView tvTittle;
    private boolean isCreated = true;
    private LoadingClass LoadActivity;
    private List<DownTimeModel.GetDownMachineDetail> resourceDetail;
    private AutoLogout autoLogout;
    private RecyclerView getLineByMachineRV;
    private DownMainPage parentActivity;

    @Override
    public void onDestroy() {
        autoLogout.stop();
        Log.d("TimerLog","PmListTimer Stop");
        super.onDestroy();
    }

    @Override
    public void onResume(){
        super.onResume();
        autoLogout.start();
        restCrud = APIClient.getClient().create(RestCrud.class);
        Log.d("PMSTAT","Resume:  "+PageType);

        if(isCreated){
            if(PageType.equals("LinePage"))
            {
                AddData(true);
            }
            else{
                GetPmMachine(LineId,false);
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        autoLogout.stop();
    }

    @Override
    public void onUserInteraction()
    {
        autoLogout.reset();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        parentActivity = this;
        setContentView(R.layout.layout_pmline);
        //Button
        btnBack = (Button) findViewById(R.id.btnBack);
        btnNewPm = (Button) findViewById(R.id.btnNewPm);
        //TextView
        tvTittle = (TextView) findViewById(R.id.tvStartDate);
        //RecyclerView
        getLineByMachineRV = (RecyclerView) findViewById(R.id.getLineByMachineRV);
        //SetText
        tvTittle.setText("Downtime");
        btnNewPm.setText("NEW DOWN");
        //OnclickListener
        btnBack.setOnClickListener(this);
        btnNewPm.setOnClickListener(this);

        tvTittle.setBackgroundResource(R.drawable.borderlinename);
        tvTittle.setTextColor(getResources().getColor(R.color.white));
        tvTittle.setTypeface(null, Typeface.BOLD);
        tvTittle.setPadding(10,8,10,8);

        PageType=getIntent().getStringExtra("Type");
        restCrud = APIClient.getClient().create(RestCrud.class);
        LoadActivity = new LoadingClass(this,1);

        autoLogout = new AutoLogout(this);
        autoLogout.start();

        if(PageType.equals("MachinePage")){
            LineId=getIntent().getStringExtra("LineId");
            String Content=getIntent().getStringExtra("LineName");

            Log.d("IMHERE",Content);
            tvTittle.setText(Content);
            GetPmMachine(LineId,false);
        }
        else{
            AddData(true);
        }
        isCreated = true;
    }

    public void GetPmMachine(String LineId,final boolean Init) {
        Call<List<DownTimeModel.GetDownMachineDetail>> call = restCrud.GetDownMachineDetail(LineId);
        LoadActivity.show();
        call.enqueue(new Callback<List<DownTimeModel.GetDownMachineDetail>>() {
            @SuppressLint("WrongConstant")
            @Override
            public void onResponse(Call<List<DownTimeModel.GetDownMachineDetail>> call, Response<List<DownTimeModel.GetDownMachineDetail>> response) {
                if(response.isSuccessful()){
                    LoadActivity.dismiss();

                    final List<DownTimeModel.GetDownMachineDetail> body = response.body();
                    if (body == null) {
                        return;
                    }

                    Log.d("Res",response.body().toString());

                    resourceDetail = response.body();
                    if(resourceDetail == null)
                        return;

                    int numofdata = resourceDetail.size();
                    CureentData = numofdata;

                    if(Init)
                    {
                        PrevData=CureentData;
                    }
                    else if(PrevData==CureentData)
                    {
                        return;
                    }
                    else
                    {
                        PrevData=CureentData;
                    }
                    DownAdapter downAdapter = new DownAdapter(parentActivity,body);
                    getLineByMachineRV.setLayoutManager(new GridLayoutManager(parentActivity, 4));
                    getLineByMachineRV.setNestedScrollingEnabled(false);
                    getLineByMachineRV.setAdapter(downAdapter);
                    downAdapter.setClickListener(new DownAdapter.ItemClickListener() {
                        @Override
                        public void onItemClick(View view, int position) {
                            String Id = String.valueOf(body.get(position).Id);
                            String lineName = body.get(position).LineNameMap;
                            Toast.makeText(parentActivity, "Please Wait Loading Data...", Toast.LENGTH_LONG).show();

                            if(!PageType.equals("MachinePage")){
                                Intent clickM = new Intent(DownMainPage.this,DownMainPage.class);
                                clickM.putExtra("Type","MachinePage");
                                clickM.putExtra("LineId",Id);
                                clickM.putExtra("LineName",lineName);
                                startActivity(clickM);
                            }
                            else{
                                DownTimeModel.GetDownMachineDetail getDownMachineDetail = resourceDetail.get(position);

                                myConfig.setModelDownTime(getDownMachineDetail);

                                if(parentActivity != null && !parentActivity.isFinishing()){
                                    FragmentManager fm = parentActivity.getSupportFragmentManager();
                                    DownDialogFragment downDialogFragment = DownDialogFragment.newInstance(parentActivity,body.get(position),body);
                                    downDialogFragment.show(fm, TAG);
                                }
                            }
                        }
                    });
                    downAdapter.refreshRecyclerView();
                }
                else{
                    LoadActivity.dismiss();
                    Toast.makeText(parentActivity,response.message(),Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<DownTimeModel.GetDownMachineDetail>> call, Throwable t) {
                LoadActivity.dismiss();
                Toast.makeText(DownMainPage.this,"Error",Toast.LENGTH_SHORT).show();
                Log.d("err","error get");
            }
        });

    }

    public void AddData(final boolean Init) {
        Call<List<DownTimeModel.GetDownLine>> call = restCrud.GetDownLine();
        LoadActivity.show();
        call.enqueue(new Callback<List<DownTimeModel.GetDownLine>>() {
            @SuppressLint("WrongConstant")
            @Override
            public void onResponse(Call<List<DownTimeModel.GetDownLine>> call, Response<List<DownTimeModel.GetDownLine>> response) {
                if(response.isSuccessful()){
                    LoadActivity.dismiss();
                    final List<DownTimeModel.GetDownLine> body = response.body();
                    if (body == null) {
                        return;
                    }

                    Log.d("Res",response.body().toString());

                    List<DownTimeModel.GetDownLine> resource = response.body();


                    if(resource == null)
                        return;

                    int numofdata = resource.size();
                    CureentData = numofdata;

                    if(Init)
                    {
                        PrevData=CureentData;
                    }

                    else if(PrevData==CureentData){
                        return;
                    }

                    else
                    {
                        PrevData=CureentData;
                    }

                    DownAdapter downAdapter = new DownAdapter(body,parentActivity);
                    getLineByMachineRV.setLayoutManager(new GridLayoutManager(parentActivity, 4));
                    getLineByMachineRV.setNestedScrollingEnabled(false);
                    getLineByMachineRV.setAdapter(downAdapter);
                    downAdapter.setClickListener(new DownAdapter.ItemClickListener() {
                        @Override
                        public void onItemClick(View view, int position) {

                            String Id = String.valueOf(body.get(position).Id);
                            String lineName = body.get(position).LineNameMap;
                            Toast.makeText(parentActivity, "Please Wait Loading Data...", Toast.LENGTH_LONG).show();

                            if(!PageType.equals("MachinePage")){
                                Intent clickM = new Intent(DownMainPage.this,DownMainPage.class);
                                clickM.putExtra("Type","MachinePage");
                                clickM.putExtra("LineId",Id);
                                clickM.putExtra("LineName",lineName);
                                startActivity(clickM);
                            }
//                            else{
//                                int tag = Integer.parseInt(Id);
//                                DownTimeModel.GetDownMachineDetail getDownMachineDetail = resourceDetail.get(tag);
//
//                                myConfig.setModelDownTime(getDownMachineDetail);
//
//                                Intent clickM = new Intent(DownMainPage.this, DownMachineDetail.class);
//                                clickM.putExtra("PmId",Id);
//                                clickM.putExtra("MachineName",lineName);
//                                startActivityForResult(clickM, 100);
//
//                            }
                        }
                    });
                    downAdapter.refreshRecyclerView();
                }
                else{
                    LoadActivity.dismiss();
                    Toast.makeText(parentActivity,response.message(),Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<List<DownTimeModel.GetDownLine>> call, Throwable t) {
                LoadActivity.dismiss();
                Toast.makeText(DownMainPage.this,"ERROR",Toast.LENGTH_LONG).show();
                Log.d("err","error get");
            }
        });
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnBack:
                finish();
                break;
            case R.id.btnNewPm:
                if(parentActivity != null && !parentActivity.isFinishing()){
                    FragmentManager fm = parentActivity.getSupportFragmentManager();
                    AddPMDialogFragment addPMDialogFragment = AddPMDialogFragment.newInstance(parentActivity,"DOWN",LineId);
                    addPMDialogFragment.show(fm, TAG);
                }
                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 100) {
            if(resultCode == Activity.RESULT_OK){
                GetPmMachine(LineId,true);
                myConfig.setCurrentPage("CoMachine");
                Log.d("TEST123","HERE");
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                // myConfig.getCurrentPage().equals("PmLine");
            }
        }
    }

}
