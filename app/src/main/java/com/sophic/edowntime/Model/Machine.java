package com.sophic.edowntime.Model;

public class Machine {
    public String Id;
    public String MachineType;
    public String ActionTaken;
    public String Product;
    public String RepairTime;
    public String WaitingTime;
    public String Location;
    public String Cpk;
    public String EquipmentId;
    public String PICName;
    public String Status;
    public String TimeIn;
    public String ErrorCode;
    public String productName;
    public String ModuleNo;
    public String Cause;
    public String Remedy;
    public String OnOff;
    public String LineName;
    public String MachineName;
    public String DataLineName;
    public String DataMachineName;
    public String LineId;

    public Machine(){

    }

    public Machine(String id, String machineType, String actionTaken, String product, String repairTime, String waitingTime, String location, String cpk, String equipmentId, String PICName, String status, String timeIn, String errorCode, String productName) {
        this.Id = id;
        this.MachineType = machineType;
        this.ActionTaken = actionTaken;
        this.Product = product;
        this.RepairTime = repairTime;
        this.WaitingTime = waitingTime;
        this.Location = location;
        this.Cpk = cpk;
        this.EquipmentId = equipmentId;
        this.PICName = PICName;
        this.Status = status;
        this.TimeIn = timeIn;
        this.ErrorCode = errorCode;
        this.productName = productName;
    }

    public Machine(String id, String machineType, String actionTaken, String repairTime, String waitingTime, String PICName, String status, String timeIn, String errorCode, String productName, String moduleNo, String cause, String remedy, String onOff, String lineName, String machineName, String dataLineName, String dataMachineName, String lineId) {
        this.Id = id;
        this.MachineType = machineType;
        this.ActionTaken = actionTaken;
        this.RepairTime = repairTime;
        this.WaitingTime = waitingTime;
        this.PICName = PICName;
        this.Status = status;
        this.TimeIn = timeIn;
        this.ErrorCode = errorCode;
        this.productName = productName;
        this.ModuleNo = moduleNo;
        this.Cause = cause;
        this.Remedy = remedy;
        this.OnOff = onOff;
        this.LineName = lineName;
        this.MachineName = machineName;
        this.DataLineName = dataLineName;
        this.DataMachineName = dataMachineName;
        this.LineId = lineId;
    }
}
