package com.sophic.edowntime.Model;

import android.os.Handler;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;


/**
 * Created by X-PC on 19/2/2018.
 */

public class MyConfig {

    private static MyConfig instance = null;
    protected MyConfig() {
        // Exists only to defeat instantiation.
    }
    public static MyConfig getInstance() {
        if(instance == null) {
            instance = new MyConfig();
        }
        return instance;
    }

    public static List<String> LineList= new ArrayList<String>();
    public static List<String> UserLineList= new ArrayList<String>();

    public static String Lines;

    public static boolean isTaskListPM=false;
    public static Handler TaskListPM;

    public static boolean isTaskListCO=false;
    public static Handler TaskListCO;

    public static boolean isTaskMachineList=false;
    public static Handler TaskListMachine;
    //public Timer myTimer;
    //public Timer PmListTimer;
    //public Timer CoListTimer;
    //public Timer PmMachineTimer;


    private String CurrentPage="";

    public boolean isTableDisplayIsVisible() {
        return TableDisplayIsVisible;
    }

    public void setTableDisplayIsVisible(boolean tableDisplayIsVisible) {
        TableDisplayIsVisible = tableDisplayIsVisible;
    }

    private boolean TableDisplayIsVisible=false;


    public String getCurrentPage() {
        return CurrentPage;
    }

    public void setCurrentPage(String _CurrentPage) {
        CurrentPage = _CurrentPage;
    }


    private String CoCurrentPage="";

    public String getCoCurrentPage() {
        return CoCurrentPage;
    }
    public void setCoCurrentPage(String _CoCurrentPage) {
        CoCurrentPage = _CoCurrentPage;
    }



    //region ListAllMachine Variable
    private boolean TaskListAllMachine=true;

    public boolean isTaskListAllMachine() {
        return TaskListAllMachine;
    }

    public void setTaskListAllMachine(boolean taskListAllMachine) {
        TaskListAllMachine = taskListAllMachine;
    }

    private boolean TaskGetLineByMachine=true;

    public boolean isTaskGetLineByMachine() {
        return TaskGetLineByMachine;
    }

    public void setTaskGetLineByMachine(boolean _TaskGetLineByMachine) {
        TaskGetLineByMachine = _TaskGetLineByMachine;
    }

    private boolean TaskListPMCO=true;

    public boolean isTaskListPMCO() {
        return TaskListPMCO;
    }

    public void setTaskListPMCO(boolean taskListPMCO) {
        TaskListPMCO = taskListPMCO;
    }






    private int NxtPrevious=0;
    private int NxtCurrent;

    public int getNxtPrevious() {
        return NxtPrevious;
    }

    public void setNxtPrevious(int nxtPrevious) {
        NxtPrevious = nxtPrevious;
    }

    public int getNxtCurrent() {
        return NxtCurrent;
    }

    public void setNxtCurrent(int nxtCurrent) {
        NxtCurrent = nxtCurrent;
    }


    private int SPIPrevious=0;
    private int SPICurrent;

    public int getSPIPrevious() {
        return SPIPrevious;
    }

    public void setSPIPrevious(int SPIPrevious) {
        this.SPIPrevious = SPIPrevious;
    }

    public int getSPICurrent() {
        return SPICurrent;
    }

    public void setSPICurrent(int SPICurrent) {
        this.SPICurrent = SPICurrent;
    }

    private int DEKPrevious=0;
    private int DEKCurrent;

    public int getDEKPrevious() {
        return DEKPrevious;
    }

    public void setDEKPrevious(int DEKPrevious) {
        this.DEKPrevious = DEKPrevious;
    }

    public int getDEKCurrent() {
        return DEKCurrent;
    }

    public void setDEKCurrent(int DEKCurrent) {
        this.DEKCurrent = DEKCurrent;
    }


    private int AOIPrevious=0;
    private int AOICurrent;

    public int getAOIPrevious() {
        return AOIPrevious;
    }

    public void setAOIPrevious(int AOIPrevious) {
        this.AOIPrevious = AOIPrevious;
    }

    public int getAOICurrent() {
        return AOICurrent;
    }

    public void setAOICurrent(int AOICurrent) {
        this.AOICurrent = AOICurrent;
    }


    private int CPKPrevious=0;
    private int CPKCurrent;

    public int getCPKPrevious() {
        return CPKPrevious;
    }

    public void setCPKPrevious(int CPKPrevious) {
        this.CPKPrevious = CPKPrevious;
    }

    public int getCPKCurrent() {
        return CPKCurrent;
    }

    public void setCPKCurrent(int CPKCurrent) {
        this.CPKCurrent = CPKCurrent;
    }


    private int BTUPrevious=0;
    private int BTUCurrent;

    public int getBTUPrevious() {
        return BTUPrevious;
    }

    public void setBTUPrevious(int BTUPrevious) {
        this.BTUPrevious = BTUPrevious;
    }

    public int getBTUCurrent() {
        return BTUCurrent;
    }

    public void setBTUCurrent(int BTUCurrent) {
        this.BTUCurrent = BTUCurrent;
    }

    private int EBPrevious=0;
    private int EBCurrent;

    public int getEBPrevious() {
        return EBPrevious;
    }

    public void setEBPrevious(int EBPrevious) {
        this.EBPrevious = EBPrevious;
    }

    public int getEBCurrent() {
        return EBCurrent;
    }

    public void setEBCurrent(int EBCurrent) {
        this.EBCurrent = EBCurrent;
    }

    private int CAMULPrevious=0;
    private int CAMULCurrent;

    public int getCAMULPrevious() {
        return CAMULPrevious;
    }

    public void setCAMULPrevious(int CAMULPrevious) {
        this.CAMULPrevious = CAMULPrevious;
    }

    public int getCAMULCurrent() {
        return CAMULCurrent;
    }

    public void setCAMULCurrent(int CAMULCurrent) {
        this.CAMULCurrent = CAMULCurrent;
    }

    private int CMLPrevious=0;
    private int CMLCurrent;

    public int getCMLPrevious() {
        return CMLPrevious;
    }

    public void setCMLPrevious(int CMLPrevious) {
        this.CMLPrevious = CMLPrevious;
    }

    public int getCMLCurrent() {
        return CMLCurrent;
    }

    public void setCMLCurrent(int CMLCurrent) {
        this.CMLCurrent = CMLCurrent;
    }




    private DownTimeModel.GetDownMachineDetail modelDownTime;

    public void setModelDownTime(DownTimeModel.GetDownMachineDetail modelDownTime) {
        this.modelDownTime = modelDownTime;
    }
    public DownTimeModel.GetDownMachineDetail getModelDownTime() {
        return modelDownTime;
    }


    //endregion
}
