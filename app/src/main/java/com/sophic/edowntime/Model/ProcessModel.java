package com.sophic.edowntime.Model;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ProcessModel {
    public final String STATUS_RUNNING = "running";
    public final String STATUS_STOP = "stop";

    public String Name;
    public String ClassName;
    public String Status;
    public Date LastRunningDate;
    public Map<String, ProcessModel> processModelMap = new HashMap<>();
    public List<ProcessModel> processModelList = new ArrayList<>();

    public ProcessModel(){

    }

    public ProcessModel(String name, String className, String status, Date lastRunningDate) {
        Name = name;
        ClassName = className;
        Status = status;
        LastRunningDate = lastRunningDate;
    }

    public Map<String, ProcessModel> getProcessModelMap() {
        return processModelMap;
    }

    public void setProcessModelMap(Map<String, ProcessModel> processModelMap) {
        this.processModelMap = processModelMap;
    }

    public List<ProcessModel> getProcessModelList() {
        return processModelList;
    }

    public void setProcessModelList(List<ProcessModel> processModelList) {
        this.processModelList = processModelList;
    }
}
