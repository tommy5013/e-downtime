package com.sophic.edowntime.Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by X-PC on 20/2/2018.
 */

public class DownTimeModel {

    public class GetDownLine
    {
        public int c;
        public String LineNameMap;
        public int Id;
    }

    public class GetDownMachine
    {
        public int Id ;
        public int PIC ;
        public int MachineId;
        public int Duration ;
        public String COType;
        public String StationName ;
        public String LineNameMap ;
        public String DateTime;
        public String EndTime ;
    }

    public class GetDownMachineDetail
    {
        public String Id;
        public String  PIC;
        public int MachineId;
        public int Duration;
        public String COType;
        public String StartDate;
        public String StationName;
        public String LineNameMap;
        public String DateTime;
        public String EndTime;
        public String Others;
    }

    public class ModelAddCloseDowntime
    {
        public String Id;
        public String PIC;
        public String  StationName;
        public String  LineNameMap;
        public String  StartDate;
        public String  CoType;
        public String  Others;
    }


}
