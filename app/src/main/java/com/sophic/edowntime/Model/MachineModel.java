package com.sophic.edowntime.Model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class MachineModel {
    public String MachineType;
    public String ErrorCount;
    public String OtherParam;
    @SerializedName("Machine")
    public ArrayList<Machine> machineList;

    public class AllMachineModel
    {
        public String MachineType;
        public String LineList;
    }

    public class Line
    {
        @SerializedName("LineNameMap")
        public String LineNameMap;
    }

    public class LineList
    {
        public String LineName;
        public String PicCount;

    }

    public class UserInfo
    {
        public String Username;
        public String Password;
        public String UserType;
        public String SerialNumber;
        public String Wifi;
        public String SelectedLines;

    }

    public class LineByMachine
    {
        public int ErrorCount;
        public String LineName;
        public String OtherParam;
        public  String LineId;
    }

    public class Machine
    {
        @SerializedName("StationName")
        public String StationName;
    }

    public static class MachineList
    {
        @SerializedName("MachineName")
        public String MachineName;
    }

    public static class AlertDone
    {
        public String AlertId;
        public String PICName;
        public String ActionTaken;

    }

    public class AllMachineList
    {
        public String MachineType;
    }

}
