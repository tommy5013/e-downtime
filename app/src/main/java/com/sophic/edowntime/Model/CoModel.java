package com.sophic.edowntime.Model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by X-PC on 20/2/2018.
 */

public class CoModel {

    public class GetCoLine
    {
        public int c;
        public String LineNameMap;
        public int Id;
    }

    public class GetCoMachine
    {
        public int Id ;
        public int PIC ;
        public int MachineId;
        public int Duration ;
        public String COType;
        public String StationName ;
        public String LineNameMap ;
        public String DateTime;
        public String EndTime ;
    }

    public class GetCoMachineDetail
    {
        public int Id  ;
        public String StartDate;
        public int PIC;
        public int MachineId;
        public int Duration;
        public String COType;
        public String StationNameMap;
        public String LineNameMap;
        public String DateTime;
    }

    public class ModelNewCo
    {
        public String LineName ;
        public String MachineName;
        public String CoType;
        public int Duration;
        public String StartDate;
        public String Username;
    }

    public class CoCheckList
    {
        public int Id;
        public String DateCreated ;
        public int CoId;
        public int MachineId ;
        public String ChecklistId ;
        public String Others ;
        public String EndTime;
        public String PIC ;
    }
}
