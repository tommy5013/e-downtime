package com.sophic.edowntime.Helper;

import android.app.AlertDialog;
import android.content.Context;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;


import com.sophic.edowntime.Controllers.ToastController;
import com.sophic.edowntime.R;

import dmax.dialog.SpotsDialog;
/**
 * Created by X-PC on 2/4/2018.
 */

public class LoadingClass {

    // ProgressDialog progressDialog;
    AlertDialog progressDialog;
    Context context;

    public LoadingClass(){};

    public LoadingClass(Context context, int type,String... CustomMessage){

        this.context = context;

        switch (type)
        {
            case 1:
                this.progressDialog = new SpotsDialog(context,"Please Wait Loading", R.style.Custom);
                break;

            case 2:
                this.progressDialog = new SpotsDialog(context,"Authenticating",R.style.Custom);
                break;

            case 3:
                this.progressDialog = new SpotsDialog(context,CustomMessage[0],R.style.Custom);
                break;
        }

    }

    public void show(){
        //progressDialog.setIndeterminate(true);
        //progressDialog.setMessage("Loading");
        if(progressDialog != null){
            progressDialog.show();
            progressDialog.setCanceledOnTouchOutside(false);
        }
        else{
            new ToastController().SetToast((AppCompatActivity) context,"Please initialize your progress dialog.");
        }
    }

    public void dismiss(){
        if(progressDialog != null && progressDialog.isShowing())
            progressDialog.dismiss();
    }
}
