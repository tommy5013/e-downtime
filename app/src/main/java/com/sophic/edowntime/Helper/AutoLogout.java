package com.sophic.edowntime.Helper;

import android.app.Activity;
import android.content.Intent;
import android.os.Handler;

import com.sophic.edowntime.Activities.MainActivity;
import com.sophic.edowntime.Model.MachineModel;
import com.sophic.edowntime.RestAPI.APIClient;
import com.sophic.edowntime.RestAPI.RestCrud;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AutoLogout {

    static int delay = 900000;

    Activity activity;
    Handler handler;
    Runnable runnable;

    public AutoLogout(Activity activity){
        this.activity = activity;
        this.handler = new Handler();
        this.runnable = new Runnable() {
            @Override
            public void run() {
                logout();
            }
        };
    }

    public void reset(){
        handler.removeCallbacks(runnable);
        handler.postDelayed(runnable, delay);
    }

    public void stop(){
        handler.removeCallbacks(runnable);
    }

    public void start(){
        handler.postDelayed(runnable, delay);
    }

    private void logout(){
        UpdateLogout();
        //redirect user to login screen
        Intent i = new Intent(activity, MainActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        activity.startActivity(i);
    }

    private void UpdateLogout(){
        RestCrud RestCrud = APIClient.getClient().create(RestCrud.class);

        MachineModel.UserInfo userInfo = new MachineModel().new UserInfo();
        userInfo.Username= MainActivity.username;
        userInfo.Password = "";
        userInfo.UserType="type";
        Call<MachineModel.UserInfo> call = RestCrud.UserLogout(userInfo);
        call.enqueue(new Callback<MachineModel.UserInfo>() {
            @Override
            public void onResponse(Call<MachineModel.UserInfo> call, Response<MachineModel.UserInfo> response) {
                MachineModel.UserInfo body = response.body();
            }

            @Override
            public void onFailure(Call<MachineModel.UserInfo> call, Throwable t) {
                //Toast.makeText(LineSelectActivity.this, "Error", Toast.LENGTH_LONG).show();
            }
        });
    }
}
