package com.sophic.edowntime.Helper;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;


import com.sophic.edowntime.RestAPI.APIClient;
import com.sophic.edowntime.RestAPI.RestCrud;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CamLib {

    private com.sophic.edowntime.RestAPI.RestCrud RestCrud;
    LoadingClass LoadActivity;

    public  final int TAKE_PICTURE = 1;



    public void CaptureImage(Context context,String ImageName){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
        //String currentDateandTime = sdf.format(new Date());
        //filename = "PM"+currentDateandTime+"ghg.jpg";
        String filename = ImageName+".jpg";

        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        File photo = new File(Environment.getExternalStorageDirectory(),  filename);
        cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT,
                Uri.fromFile(photo));

        //cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, capturedImageUri);
        //startActivityForResult(cameraIntent, TAKE_PICTURE);
        ((AppCompatActivity) context).startActivityForResult(cameraIntent, TAKE_PICTURE);
    }


    public void UploadImage(Context context,String filename,String OtherParam) {

        LoadActivity = new LoadingClass(context,1);
        LoadActivity.show();
        RestCrud = APIClient.getClient().create(RestCrud.class);


        final File file = new File(Environment.getExternalStorageDirectory(), filename+".jpg");
        RequestBody requestFile =
                RequestBody.create(MediaType.parse("multipart/form-data"), file);

// MultipartBody.Part is used to send also the actual file
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
        String currentDateandTime = sdf.format(new Date());
        String filename2 = "PM" + currentDateandTime + ".jpg";

        MultipartBody.Part body =
                MultipartBody.Part.createFormData("image", filename2, requestFile);

// add another part within the multipart request
        //RequestBody OtherParam =
          //      RequestBody.create(MediaType.parse("multipart/form-data"), Param);

        Call<ResponseBody> call = RestCrud.PostUserImage(body,OtherParam);

        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call,
                                   Response<ResponseBody> response) {
                Log.v("Upload", "success");
                file.delete();
                LoadActivity.dismiss();
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("Upload error:", t.getMessage());
                LoadActivity.dismiss();
            }
        });


    }

    public void UploadImageSync(Context context,String filename,String OhterParam) {



        LoadActivity = new LoadingClass(context,1);
        LoadActivity.show();
        RestCrud = APIClient.getClient().create(RestCrud.class);


        final File file = new File(Environment.getExternalStorageDirectory(), filename+".jpg");
        RequestBody requestFile =
                RequestBody.create(MediaType.parse("multipart/form-data"), file);

// MultipartBody.Part is used to send also the actual file
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd_HHmmss");
        String currentDateandTime = sdf.format(new Date());
        String filename2 = "PM" + currentDateandTime + ".jpg";

        MultipartBody.Part body =
                MultipartBody.Part.createFormData("image", filename2, requestFile);

// add another part within the multipart request
        RequestBody fullName =
                RequestBody.create(MediaType.parse("multipart/form-data"), "Your Name");

        Call<ResponseBody> call = RestCrud.PostUserImage(body,"");


        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call,
                                   Response<ResponseBody> response) {
                Log.v("Upload", "success");
                file.delete();
                LoadActivity.dismiss();

            }


            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.e("Upload error:", t.getMessage());
                LoadActivity.dismiss();

            }
        });

     /*   while (taskfinish[0] ==false){
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }*/

    }



}
