package com.sophic.edowntime.Dialogs;

import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.sophic.edowntime.Activities.MainActivity;
import com.sophic.edowntime.Activities.PMMainPage;
import com.sophic.edowntime.Helper.CamLib;
import com.sophic.edowntime.Helper.LoadingClass;
import com.sophic.edowntime.Model.PmModel;
import com.sophic.edowntime.R;
import com.sophic.edowntime.RestAPI.APIClient;
import com.sophic.edowntime.RestAPI.RestCrud;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PMDialogFragment extends DialogFragment implements View.OnClickListener {
    private ImageView closeIB;
    private PMMainPage parentActivity;
    private String PmId="",MachineName="";
    private LinearLayout linearMain;
    private TextView tvMachineName,tvLineName,tvDateCreated;
    private TextView tvStartDate,tvTotalTime,tvTargetDuration;
    private TextView tvPmType,tvTittle;
    private Button btnDone,btnCamera;
    private RestCrud RestCrud;
    private LoadingClass LoadActivity;
    private PmModel.GetPmMachine getPmMachine;
    private List<PmModel.GetPmMachine> getPmMachineList;
    private CamLib camLib;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setStyle(STYLE_NO_TITLE, 0);
        int style = DialogFragment.STYLE_NORMAL, theme = 0;
        setStyle(0, R.style.DialogWithCloseBtnStyle);
    }

    public PMDialogFragment() {
        // Empty constructor is required for DialogFragment
        // Make sure not to add arguments to the constructor
        // Use `newInstance` instead as shown below
    }

    public static PMDialogFragment newInstance(PMMainPage parentActivity, PmModel.GetPmMachine getPmMachine, List<PmModel.GetPmMachine> getPmMachineList) {
        PMDialogFragment frag = new PMDialogFragment();
        frag.parentActivity = parentActivity;
        frag.getPmMachine = getPmMachine;
        frag.getPmMachineList = getPmMachineList;
        return frag;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        getDialog().setCanceledOnTouchOutside(false);
        getDialog().setCancelable(true);

        return inflater.inflate(R.layout.dialog_comachine_detail, container);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if(getPmMachine != null){
            PmId = String.valueOf(getPmMachine.Id);
            MachineName = getPmMachine.LineNameMap;
        }
        else{
            Toast.makeText(parentActivity, "Machine detail null.", Toast.LENGTH_SHORT).show();
            return;
        }

        LoadActivity = new LoadingClass(parentActivity,1);
        camLib = new CamLib();
        linearMain = (LinearLayout) view.findViewById(R.id.linear_main);
        tvMachineName = (TextView) view.findViewById(R.id.tvMachineName);
        tvLineName = (TextView) view.findViewById(R.id.tvLineName);
        tvDateCreated = (TextView) view.findViewById(R.id.tvDateCreated);
        tvStartDate = (TextView) view.findViewById(R.id.tvStartDate);
        tvTotalTime = (TextView) view.findViewById(R.id.tvTotalTime);
        tvTargetDuration = (TextView) view.findViewById(R.id.tvTargetDuration);
        tvPmType = (TextView) view.findViewById(R.id.tvPmType);
        btnDone = (Button) view.findViewById(R.id.btnDone);
        btnCamera = (Button) view.findViewById(R.id.btnCamera);
        tvTittle =  (TextView) view.findViewById(R.id.tvTitle);
        closeIB = view.findViewById(R.id.closeIV);

        tvTittle.setText("Prevention Maintenance");
        tvMachineName.setText(MachineName);

        RestCrud = APIClient.getClient().create(RestCrud.class);

        GetPmMachineDetail(PmId);
        GetPmMachineChecklist(PmId);

        btnDone.setOnClickListener(this);
        closeIB.setOnClickListener(this);
        btnCamera.setOnClickListener(this);

    }
    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        setWidthScreen();
    }

    private void setWidthScreen(){
        Window window = getDialog().getWindow();
        Point size = new Point();

        Display display = window.getWindowManager().getDefaultDisplay();
        display.getSize(size);

        int width = size.x;

        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        window.setGravity(Gravity.CENTER);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.closeIV:
                dismiss();
                break;

            case R.id.btnDone:
                Toast.makeText(parentActivity,"Please Wait Updating Data",Toast.LENGTH_LONG).show();
                DonePM();
                break;

            case R.id.btnCamera:

                camLib.CaptureImage(parentActivity,"PM");

                break;
        }
    }

    public void GetPmMachineChecklist(final String PmId) {

        Call<List<PmModel.PmCheckList>> call = RestCrud.GetPmCheckList(PmId);
        call.enqueue(new Callback<List<PmModel.PmCheckList>>() {


            @Override
            public void onResponse(Call<List<PmModel.PmCheckList>> call, Response<List<PmModel.PmCheckList>> response) {

                List<PmModel.PmCheckList> body = response.body();
                if (body == null) {
                    return;
                }


                int numofdata = body.size();

                for(int i=0; i<numofdata; i++)
                {
                    final CheckBox checkBox = new CheckBox(parentActivity);

                    checkBox.setId(body.get(i).Id);
                    if(body.get(i).Checklist==null){
                        checkBox.setText(body.get(i).Others);
                    }
                    else {
                        checkBox.setText(body.get(i).Checklist);
                    }


                    if(body.get(i).PIC != null){
                        checkBox.setChecked(true);
                    }

                    checkBox.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            int cboxid = checkBox.getId();

                            if (checkBox.isChecked())
                            {
                                String checked = checkBox.getText().toString();

                                UpdateCheckList(cboxid,"True");
//                                Toast.makeText(parentActivity,  " Checked",Toast.LENGTH_SHORT).show();
                            }
                            else
                            {
                                String unchecked = checkBox.getText().toString();
                                UpdateCheckList(cboxid,"False");
//                                Toast.makeText(parentActivity,  " Unchecked",Toast.LENGTH_SHORT).show();
                            }
                        }
                    });

                    linearMain.addView(checkBox);

                    // Set Flex CheckBox scaling programmatically
                    checkBox.setScaleX(1.5f);
                    checkBox.setScaleY(1.5f);
                    // Get the margins of Flex CheckBox
                    ViewGroup.MarginLayoutParams mlp = (ViewGroup.MarginLayoutParams) checkBox.getLayoutParams();

                    // Set left, top, right and bottom margins of Flex CheckBox
                    mlp.setMargins(50,20,50,20);
                    checkBox.setLayoutParams(mlp);

                    // Apply right padding of Flex CheckBox
                    //checkBox.setPadding(0,0,0,0);
                }


            }


            @Override
            public void onFailure(Call<List<PmModel.PmCheckList>> call, Throwable t) {
                Log.d("err","error get");
            }
        });

    }

    private void UpdateCheckList(int CheckListId,String Status)
    {
        String body = CheckListId+"|"+ MainActivity.username+"|"+Status;
        Call<String> call = null;
        call = RestCrud.UpdatePmCheckList(body);

        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {

                Log.d("UpdateCheckList","Success");
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.d("UpdateCheckList","ERROR");
                Toast.makeText(parentActivity, "Error Updating Data !", Toast.LENGTH_SHORT).show();
                call.cancel();
            }
        });

    }

    public void GetPmMachineDetail(String PmId) {

        Call<List<PmModel.GetPmMachineDetail>> call = RestCrud.GetPmMachineDetail(PmId);
        call.enqueue(new Callback<List<PmModel.GetPmMachineDetail>>() {


            @Override
            public void onResponse(Call<List<PmModel.GetPmMachineDetail>> call, Response<List<PmModel.GetPmMachineDetail>> response) {

                List<PmModel.GetPmMachineDetail> body = response.body();
                // if (body == null) {
                //    return;
                // }


                tvMachineName.setText(body.get(0).StationNameMap);
                tvLineName.setText("Line:"+body.get(0).LineNameMap);

                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                SimpleDateFormat outFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                String dateInString = body.get(0).StartDate;
                dateInString=dateInString.replace("T"," ");
                try {
                    dateInString = outFormat.format(formatter.parse(dateInString));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                tvStartDate.setText("Start Date:"+dateInString);


                Date currentDate = new Date();
                try {
                    Date StartDate = formatter.parse(dateInString);
                    long diff = currentDate.getTime() - StartDate.getTime();
                    long seconds = diff / 1000;
                    long minutes = seconds / 60;
                    tvTotalTime.setText("TotalTime:"+String.valueOf(minutes));

                } catch (ParseException e) {
                    e.printStackTrace();
                }



                dateInString = body.get(0).DateTime;
                dateInString=dateInString.replace("T"," ");
                try {
                    dateInString = outFormat.format(formatter.parse(dateInString));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                tvDateCreated.setText("Date Created:"+dateInString);




                tvTargetDuration.setText("TargetDuration:"+String.valueOf(body.get(0).Duration));
                tvPmType.setText("Type:"+body.get(0).PmType);


            }

            @Override
            public void onFailure(Call<List<PmModel.GetPmMachineDetail>> call, Throwable t) {
                Log.d("err","error get GetPmMachineDetail");
            }
        });

    }

    private void DonePM(){
        String body = PmId+","+MainActivity.username;
        Call<String> call = null;
        LoadActivity.show();
        call = RestCrud.ClosePM(body);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                LoadActivity.dismiss();
                String result = response.body();
                result = result.replace("\"", "");

                if(result.equals("true")) {
                    camLib.UploadImage(parentActivity,"PM","PM,"+PmId);
                    Toast.makeText(parentActivity, "PM Closed!", Toast.LENGTH_SHORT).show();
//                    PMAdapter pmAdapter = new PMAdapter(PmId,getPmMachineList,getPmMachine);
//                    pmAdapter.refreshRecyclerView();
                    parentActivity.onResume();
//                    Intent intent = new Intent();
                    //intent.putExtra("pos", 1);
//                    setResult(Activity.RESULT_OK, intent);
//                    finish();
                    dismiss();
                }
                else {
                    Toast.makeText(parentActivity, "Please Close Checklist", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                LoadActivity.dismiss();
                Toast.makeText(parentActivity, "Error Updating Data !", Toast.LENGTH_SHORT).show();
                call.cancel();

                Intent intent = new Intent();
                //intent.putExtra("pos", 1);
//                setResult(Activity.RESULT_OK, intent);
//                finish();
                dismiss();
            }
        });

    }
}
