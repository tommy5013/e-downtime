package com.sophic.edowntime.Dialogs;

import android.graphics.Point;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.sophic.edowntime.Activities.COMainPage;
import com.sophic.edowntime.Activities.MainActivity;
import com.sophic.edowntime.Helper.LoadingClass;
import com.sophic.edowntime.Model.CoModel;
import com.sophic.edowntime.R;
import com.sophic.edowntime.RestAPI.RestCrud;
import com.sophic.edowntime.RestAPI.APIClient;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CoMachineDetailDialogFragment extends DialogFragment implements View.OnClickListener {
    private ImageView closeIB;
    private COMainPage parentActivity;
    private String PmId="",MachineName="";
    private LinearLayout linearMain;
    private TextView tvMachineName,tvLineName,tvDateCreated;
    private TextView tvStartDate,tvTotalTime,tvTargetDuration;
    private TextView tvPmType,tvTittle;
    private Button btnDone,btnCamera;
    private com.sophic.edowntime.RestAPI.RestCrud RestCrud;
    private LoadingClass LoadActivity;
    private CoModel.GetCoMachine getCoMachine;
    private List<CoModel.GetCoMachine> getCoMachineList;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setStyle(STYLE_NO_TITLE, 0);
        int style = DialogFragment.STYLE_NORMAL, theme = 0;
        setStyle(0, R.style.DialogWithCloseBtnStyle);
    }

    public CoMachineDetailDialogFragment() {
        // Empty constructor is required for DialogFragment
        // Make sure not to add arguments to the constructor
        // Use `newInstance` instead as shown below
    }

    public static CoMachineDetailDialogFragment newInstance(COMainPage parentActivity, CoModel.GetCoMachine getCoMachine,List<CoModel.GetCoMachine> getCoMachineList) {
        CoMachineDetailDialogFragment frag = new CoMachineDetailDialogFragment();
        frag.parentActivity = parentActivity;
        frag.getCoMachine = getCoMachine;
        frag.getCoMachineList = getCoMachineList;
        return frag;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        getDialog().setCanceledOnTouchOutside(false);
        getDialog().setCancelable(true);

        return inflater.inflate(R.layout.dialog_comachine_detail, container);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if(getCoMachine != null){
            PmId = String.valueOf(getCoMachine.Id);
            MachineName = getCoMachine.LineNameMap;
        }
        else{
            Toast.makeText(parentActivity, "Machine detail null.", Toast.LENGTH_SHORT).show();
            return;
        }

        LoadActivity = new LoadingClass(parentActivity,1);

        linearMain = (LinearLayout) view.findViewById(R.id.linear_main);
        tvMachineName = (TextView) view.findViewById(R.id.tvMachineName);
        tvLineName = (TextView) view.findViewById(R.id.tvLineName);
        tvDateCreated = (TextView) view.findViewById(R.id.tvDateCreated);
        tvStartDate = (TextView) view.findViewById(R.id.tvStartDate);
        tvTotalTime = (TextView) view.findViewById(R.id.tvTotalTime);
        tvTargetDuration = (TextView) view.findViewById(R.id.tvTargetDuration);
        tvPmType = (TextView) view.findViewById(R.id.tvPmType);
        btnDone = (Button) view.findViewById(R.id.btnDone);
        tvTittle =  (TextView) view.findViewById(R.id.tvTitle);
        closeIB = view.findViewById(R.id.closeIV);
        btnCamera = view.findViewById(R.id.btnCamera);

        tvTittle.setText("Change Over");
        tvMachineName.setText(MachineName);
        btnCamera.setVisibility(View.GONE);

        RestCrud = APIClient.getClient().create(RestCrud.class);

        GetCoMachineDetail(PmId);
        GetCoMachineChecklist(PmId);

        btnDone.setOnClickListener(this);
        closeIB.setOnClickListener(this);

    }
    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        setWidthScreen();
    }

    private void setWidthScreen(){
        Window window = getDialog().getWindow();
        Point size = new Point();

        Display display = window.getWindowManager().getDefaultDisplay();
        display.getSize(size);

        int width = size.x;

        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        window.setGravity(Gravity.CENTER);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.closeIV:
                dismiss();
                break;

            case R.id.btnDone:
                Toast.makeText(parentActivity,"Please Wait Updating Data",Toast.LENGTH_LONG).show();
                DonePM();
                break;
        }
    }

    public void GetCoMachineChecklist(final String PmId) {

        Call<List<CoModel.CoCheckList>> call = RestCrud.GetCoCheckList(PmId);
        call.enqueue(new Callback<List<CoModel.CoCheckList>>() {


            @Override
            public void onResponse(Call<List<CoModel.CoCheckList>> call, Response<List<CoModel.CoCheckList>> response) {

                List<CoModel.CoCheckList> body = response.body();
                if (body == null) {
                    return;
                }


                int numofdata = body.size();

                for(int i=0; i<numofdata; i++)
                {
                    final CheckBox checkBox = new CheckBox(parentActivity);

                    checkBox.setId(body.get(i).Id);
                    checkBox.setText(body.get(i).Others);

                    if(body.get(i).PIC != null){
                        checkBox.setChecked(true);
                    }

                    checkBox.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            int cboxid = checkBox.getId();

                            if (checkBox.isChecked())
                            {
                                String checked = checkBox.getText().toString();

                                UpdateCheckList(cboxid,"True");
                                Toast.makeText(parentActivity,  " Checked",Toast.LENGTH_SHORT).show();
                            }
                            else
                            {
                                String unchecked = checkBox.getText().toString();
                                UpdateCheckList(cboxid,"False");
                                Toast.makeText(parentActivity,  " Unchecked",Toast.LENGTH_SHORT).show();
                            }
                        }
                    });

                    linearMain.addView(checkBox);

                    // Set Flex CheckBox scaling programmatically
                    checkBox.setScaleX(1.5f);
                    checkBox.setScaleY(1.5f);
                    // Get the margins of Flex CheckBox
                    ViewGroup.MarginLayoutParams mlp = (ViewGroup.MarginLayoutParams) checkBox.getLayoutParams();

                    // Set left, top, right and bottom margins of Flex CheckBox
                    mlp.setMargins(50,20,50,20);
                    checkBox.setLayoutParams(mlp);

                    // Apply right padding of Flex CheckBox
                    //checkBox.setPadding(0,0,0,0);
                }


            }


            @Override
            public void onFailure(Call<List<CoModel.CoCheckList>> call, Throwable t) {
                Log.d("err","error get");
            }
        });

    }

    private void UpdateCheckList(int CheckListId,String Status)
    {
        String body = CheckListId+"|"+ MainActivity.username+"|"+Status;
        Call<String> call = null;
        call = RestCrud.UpdateCoCheckList(body);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {

                Log.d("UpdateCheckList","Success");
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.d("UpdateCheckList","ERROR");
                Toast.makeText(parentActivity, "Error Updating Data !", Toast.LENGTH_SHORT).show();
                call.cancel();
            }
        });

    }

    public void GetCoMachineDetail(String PmId) {
        Call<List<CoModel.GetCoMachineDetail>> call = RestCrud.GetCoMachineDetail(PmId);
        call.enqueue(new Callback<List<CoModel.GetCoMachineDetail>>() {
            @Override
            public void onResponse(Call<List<CoModel.GetCoMachineDetail>> call, Response<List<CoModel.GetCoMachineDetail>> response) {

                if(response.isSuccessful()){

                    List<CoModel.GetCoMachineDetail> body = response.body();
                    // if (body == null) {
                    //    return;
                    // }
                    tvMachineName.setText(body.get(0).StationNameMap);
                    tvLineName.setText("Line:"+body.get(0).LineNameMap);

                    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    SimpleDateFormat outFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                    String dateInString = body.get(0).StartDate;
                    dateInString=dateInString.replace("T"," ");
                    try {
                        dateInString = outFormat.format(formatter.parse(dateInString));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    tvStartDate.setText("Start Date:"+dateInString);


                    Date currentDate = new Date();
                    try {
                        Date StartDate = formatter.parse(dateInString);
                        long diff = currentDate.getTime() - StartDate.getTime();
                        long seconds = diff / 1000;
                        long minutes = seconds / 60;
                        tvTotalTime.setText("TotalTime:"+String.valueOf(minutes));

                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    dateInString = body.get(0).DateTime;
                    dateInString=dateInString.replace("T"," ");
                    try {
                        dateInString = outFormat.format(formatter.parse(dateInString));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    tvDateCreated.setText("Date Created:"+dateInString);

                    tvTargetDuration.setText("TargetDuration:"+String.valueOf(body.get(0).Duration));
                    tvPmType.setText("Type:"+body.get(0).COType);
                }
                else{
                    Toast.makeText(parentActivity, response.message(), Toast.LENGTH_SHORT).show();
                }


            }

            @Override
            public void onFailure(Call<List<CoModel.GetCoMachineDetail>> call, Throwable t) {
                Log.d("err","error get GetCoMachineDetail");
            }
        });

    }

    private void DonePM(){

        String body = PmId+","+ MainActivity.username;
        Call<String> call = null;

        LoadActivity.show();
        call = RestCrud.CloseCo(body);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {

                LoadActivity.dismiss();
                String result = response.body();
                result = result.replace("\"", "");

                if(result.equals("true")) {
                    Toast.makeText(parentActivity, "CO Closed!", Toast.LENGTH_SHORT).show();
                    parentActivity.onResume();
                    dismiss();
                }
                else if(result.contains("false")) {
                    Toast.makeText(parentActivity, "Please Close Checklist", Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                LoadActivity.dismiss();
                Toast.makeText(parentActivity, "Error Updating Data !", Toast.LENGTH_SHORT).show();
                call.cancel();
                dismiss();
            }
        });

    }
}
