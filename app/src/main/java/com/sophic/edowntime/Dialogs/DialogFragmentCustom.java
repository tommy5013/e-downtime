package com.sophic.edowntime.Dialogs;

import android.content.DialogInterface;
import android.graphics.Point;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.sophic.edowntime.R;

import static com.bumptech.glide.load.DecodeFormat.PREFER_RGB_565;

public class DialogFragmentCustom extends DialogFragment {

    private String message,title;
    private AppCompatActivity parentActivity;
    public boolean isDialogClose = false;

    public DialogFragmentCustom() {
        // Empty constructor is required for DialogFragment
        // Make sure not to add arguments to the constructor
        // Use `newInstance` instead as shown below
    }

    public static DialogFragmentCustom newInstance(AppCompatActivity parentActivity, String title, String message) {
        DialogFragmentCustom frag = new DialogFragmentCustom();
        frag.message = message;
        frag.title = title;
        frag.parentActivity = parentActivity;
        return frag;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        getDialog().setCanceledOnTouchOutside(false);
        getDialog().setCancelable(true);

        return inflater.inflate(R.layout.fragment_dialog_custom, container);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        TextView messageTV =  view.findViewById(R.id.messageTV);
        TextView messageTitleTV =  view.findViewById(R.id.messageTitleTV);
        Button btnOk = view.findViewById(R.id.btnOk);
        ImageButton closeIB = view.findViewById(R.id.closeIB);
        ImageView messageIV = view.findViewById(R.id.messageIV);


        messageTV.setText(message);
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getDialog().dismiss();
            }
        });
        closeIB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().dismiss();
            }
        });
        Glide.with(getDialog().getContext())
                .asBitmap().format(PREFER_RGB_565)
                .load(R.drawable.error)
                .override(150,150)
                .into(messageIV);
        messageTitleTV.setText(title);
        messageTV.setText(message);
    }

    @Override
    public void onResume() {
        super.onResume();
        Window window = getDialog().getWindow();
        //https://guides.codepath.com/android/using-dialogfragment
        Point size = new Point();
        // Store dimensions of the screen in `size`
        Display display = null;
        if (window != null) {
            display = window.getWindowManager().getDefaultDisplay();
            display.getSize(size);
            // Set the width of the dialog proportional to 75% of the screen width
            window.setLayout((int) (size.x * 0.85), WindowManager.LayoutParams.WRAP_CONTENT);
            window.setGravity(Gravity.CENTER);
        }
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        isDialogClose = false;
    }

    public boolean IsDialogShowing(){
        if(getDialog().isShowing()){
           return true;
        }
        else{
            return false;
        }
    }
}