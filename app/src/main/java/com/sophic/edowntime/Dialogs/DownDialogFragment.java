package com.sophic.edowntime.Dialogs;

import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.sophic.edowntime.Activities.DownMainPage;
import com.sophic.edowntime.Activities.MainActivity;
import com.sophic.edowntime.Helper.AutoLogout;
import com.sophic.edowntime.Helper.LoadingClass;
import com.sophic.edowntime.Model.CoModel;
import com.sophic.edowntime.Model.DownTimeModel;
import com.sophic.edowntime.Model.MyConfig;
import com.sophic.edowntime.R;
import com.sophic.edowntime.RestAPI.APIClient;
import com.sophic.edowntime.RestAPI.RestCrud;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DownDialogFragment extends DialogFragment implements View.OnClickListener {
    private ImageView closeIB;
    private String PmId="",MachineName="";
    private LinearLayout linearMain;
    private TextView tvMachineName,tvLineName,tvDateCreated;
    private TextView tvStartDate,tvTotalTime,tvTargetDuration;
    private TextView tvPmType,tvTittle;
    private Button btnBack,btnDone;
    private com.sophic.edowntime.RestAPI.RestCrud RestCrud;
    private LoadingClass LoadActivity;
    private EditText tvIssue,tvSolution;
    private DownTimeModel.GetDownMachineDetail getDownMachineDetail;
    private MyConfig myConfig = MyConfig.getInstance();
    private AutoLogout autoLogout;
    private DownMainPage parentActivity;
    private List<DownTimeModel.GetDownMachineDetail> getDownMachineDetailList;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setStyle(STYLE_NO_TITLE, 0);
        int style = DialogFragment.STYLE_NORMAL, theme = 0;
        setStyle(0, R.style.DialogWithCloseBtnStyle);
    }

    public DownDialogFragment() {
        // Empty constructor is required for DialogFragment
        // Make sure not to add arguments to the constructor
        // Use `newInstance` instead as shown below
    }

    public static DownDialogFragment newInstance(DownMainPage parentActivity, DownTimeModel.GetDownMachineDetail getDownMachineDetail,List<DownTimeModel.GetDownMachineDetail> getDownMachineDetailList) {
        DownDialogFragment frag = new DownDialogFragment();
        frag.parentActivity = parentActivity;
        frag.getDownMachineDetail = getDownMachineDetail;
        frag.getDownMachineDetailList = getDownMachineDetailList;
        return frag;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        getDialog().setCanceledOnTouchOutside(false);
        getDialog().setCancelable(true);

        return inflater.inflate(R.layout.dialog_downmachine_detail, container);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if(getDownMachineDetail != null){
            PmId = String.valueOf(getDownMachineDetail.Id);
            MachineName = getDownMachineDetail.LineNameMap;
        }
        else{
            Toast.makeText(parentActivity, "Machine detail null.", Toast.LENGTH_SHORT).show();
            return;
        }

        closeIB = view.findViewById(R.id.closeIV);

        LoadActivity = new LoadingClass(parentActivity,1);

        autoLogout = new AutoLogout(parentActivity);
        autoLogout.start();

        getDownMachineDetail = new DownTimeModel().new GetDownMachineDetail();

        getDownMachineDetail = myConfig.getModelDownTime();

//        MachineName=getIntent().getStringExtra("MachineName");

        linearMain = (LinearLayout) view.findViewById(R.id.linear_main);
        tvMachineName = (TextView)  view.findViewById(R.id.tvMachineName);
        tvLineName = (TextView)  view.findViewById(R.id.tvLineName);
        tvDateCreated = (TextView)  view.findViewById(R.id.tvDateCreated);
        tvStartDate = (TextView)  view.findViewById(R.id.tvStartDate);
        tvTotalTime = (TextView)  view.findViewById(R.id.tvTotalTime);
        tvTargetDuration = (TextView)  view.findViewById(R.id.tvTargetDuration);
        tvPmType = (TextView)  view.findViewById(R.id.tvPmType);
        btnBack = (Button)  view.findViewById(R.id.btnBack);
        btnDone = (Button)  view.findViewById(R.id.btnDone);
        tvTittle =  (TextView)  view.findViewById(R.id.tvTitle);
        tvSolution=  (EditText)  view.findViewById(R.id.tvSolution);
        tvIssue =  (EditText)  view.findViewById(R.id.tvIssue);
        tvTittle.setText("DOWNTIME");

        tvIssue.setEnabled(false);
        tvMachineName.setText(MachineName);
        tvTargetDuration.setVisibility(View.GONE);
        RestCrud = APIClient.getClient().create(RestCrud.class);

        //GetCoMachineDetail(PmId);
        AddData();
        closeIB.setOnClickListener(this);
        btnBack.setOnClickListener(this);
        btnDone.setOnClickListener(this);

    }
    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        setWidthScreen();
    }

    private void setWidthScreen(){
        Window window = getDialog().getWindow();
        Point size = new Point();

        Display display = window.getWindowManager().getDefaultDisplay();
        display.getSize(size);

        int width = size.x;

        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        window.setGravity(Gravity.CENTER);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.closeIV:
                dismiss();
                break;

            case R.id.btnDone:
                String solution = tvSolution.getText().toString();
                if(solution.isEmpty()){
                    Toast.makeText(parentActivity,"Please Key In Solution",Toast.LENGTH_LONG).show();
                    return;
                }

                DonePM();
                break;

        }
    }

    private  void AddData(){

        tvMachineName.setText(getDownMachineDetail.StationName);
        tvLineName.setText("Line:"+ getDownMachineDetail.LineNameMap);

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat outFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        String dateInString = getDownMachineDetail.StartDate;
        dateInString=dateInString.replace("T"," ");
        try {
            dateInString = outFormat.format(formatter.parse(dateInString));
        } catch (Exception e) {
            e.printStackTrace();
        }
        tvStartDate.setText("Start Date:"+dateInString);


        Date currentDate = new Date();
        try {
            Date StartDate = formatter.parse(dateInString);
            long diff = currentDate.getTime() - StartDate.getTime();
            long seconds = diff / 1000;
            long minutes = seconds / 60;
            tvTotalTime.setText("TotalTime:"+String.valueOf(minutes));

        } catch (ParseException e) {
            e.printStackTrace();
        }

        dateInString = getDownMachineDetail.DateTime;
        dateInString=dateInString.replace("T"," ");
        try {
            dateInString = outFormat.format(formatter.parse(dateInString));
        } catch (Exception e) {
            e.printStackTrace();
        }
        tvDateCreated.setText("Date Created:"+dateInString);

        tvTargetDuration.setText("TargetDuration:"+String.valueOf(getDownMachineDetail.Duration));
        tvPmType.setText("Type:"+ getDownMachineDetail.COType);

        if(getDownMachineDetail.Others.contains(":")){
            String[] Issue = getDownMachineDetail.Others.split(":");
            tvIssue.setText(Issue[1]);
        }
        else {
            tvIssue.setText(getDownMachineDetail.Others);
        }

    }

    public void GetCoMachineDetail(String PmId) {

        Call<List<CoModel.GetCoMachineDetail>> call = RestCrud.GetCoMachineDetail(PmId);
        call.enqueue(new Callback<List<CoModel.GetCoMachineDetail>>() {


            @Override
            public void onResponse(Call<List<CoModel.GetCoMachineDetail>> call, Response<List<CoModel.GetCoMachineDetail>> response) {

                List<CoModel.GetCoMachineDetail> body = response.body();
                // if (body == null) {
                //    return;
                // }


                tvMachineName.setText(body.get(0).StationNameMap);
                tvLineName.setText("Line:"+body.get(0).LineNameMap);

                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                SimpleDateFormat outFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                String dateInString = body.get(0).StartDate;
                dateInString=dateInString.replace("T"," ");
                try {
                    dateInString = outFormat.format(formatter.parse(dateInString));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                tvStartDate.setText("Start Date:"+dateInString);


                Date currentDate = new Date();
                try {
                    Date StartDate = formatter.parse(dateInString);
                    long diff = currentDate.getTime() - StartDate.getTime();
                    long seconds = diff / 1000;
                    long minutes = seconds / 60;
                    tvTotalTime.setText("TotalTime:"+String.valueOf(minutes));

                } catch (ParseException e) {
                    e.printStackTrace();
                }

                dateInString = body.get(0).DateTime;
                dateInString=dateInString.replace("T"," ");
                try {
                    dateInString = outFormat.format(formatter.parse(dateInString));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                tvDateCreated.setText("Date Created:"+dateInString);

                tvTargetDuration.setText("TargetDuration:"+String.valueOf(body.get(0).Duration));
                tvPmType.setText("Type:"+body.get(0).COType);


            }

            @Override
            public void onFailure(Call<List<CoModel.GetCoMachineDetail>> call, Throwable t) {
                Log.d("err","error get GetDownMachineDetail");
            }
        });

    }

    private void DonePM(){

        String body = PmId+","+ MainActivity.username;
        Call<String> call = null;

        LoadActivity.show();
        DownTimeModel.ModelAddCloseDowntime modelAddCloseDowntime = new DownTimeModel().new ModelAddCloseDowntime();
        modelAddCloseDowntime.Others = "Solution:"+tvSolution.getText().toString();
        modelAddCloseDowntime.PIC = MainActivity.username;
        modelAddCloseDowntime.Id = getDownMachineDetail.Id;
        modelAddCloseDowntime.StationName= getDownMachineDetail.StationName;
        modelAddCloseDowntime.LineNameMap= getDownMachineDetail.LineNameMap;
        modelAddCloseDowntime.CoType= "DOWNTIME";

        call = RestCrud.CloseDownTime(modelAddCloseDowntime);
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {

                LoadActivity.dismiss();
                String body = response.body();

                if(body.contains("debug"))
                {
                    Toast.makeText(parentActivity, "Debug:"+body, Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent();
                    //intent.putExtra("pos", 1);
//                    setResult(Activity.RESULT_OK, intent);
                    dismiss();
                }
                else if(body.contains("true")){
                    Toast.makeText(parentActivity, "Downtime Closed!", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent();
                    //intent.putExtra("pos", 1);
//                    setResult(Activity.RESULT_OK, intent);
//                    DownAdapter getLineByMachineAdapter = new DownAdapter(PmId,getDownMachineDetailList,getDownMachineDetail);
//                    getLineByMachineAdapter.refreshRecyclerView();
                    parentActivity.onResume();

                    dismiss();
                }
                else{
                    Toast.makeText(parentActivity, "Error: "+body, Toast.LENGTH_SHORT).show();
                }



            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                LoadActivity.dismiss();
                Toast.makeText(parentActivity, "Error Updating Data !", Toast.LENGTH_SHORT).show();
                call.cancel();

                Intent intent = new Intent();
                //intent.putExtra("pos", 1);
//                setResult(Activity.RESULT_OK, intent);
                dismiss();
            }
        });

    }
}
