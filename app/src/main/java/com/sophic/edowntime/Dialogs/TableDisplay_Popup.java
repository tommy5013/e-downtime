//package com.sophic.edowntime.Dialogs;
//
//import android.app.Activity;
//import android.app.Dialog;
//import android.os.Bundle;
//import android.view.View;
//import android.view.Window;
//import android.widget.Button;
//import android.widget.EditText;
//import android.widget.RadioButton;
//import android.widget.RadioGroup;
//import android.widget.TextView;
//
//import com.sophic.edowntime.Activities.table_display;
//import com.sophic.edowntime.R;
//import com.sophic.edowntime.RestAPI.APIClient;
//import com.sophic.edowntime.Utilities.NetworkCallback;
//import com.sophic.edowntime.Utilities.NetworkTask;
//import com.sophic.edowntime.RestAPI.RestCrud;
//
//import java.util.ArrayList;
//import java.util.List;
//
//public class TableDisplay_Popup extends Dialog implements
//        android.view.View.OnClickListener  {
//
//
//    private Button btn_save,btn_select_date;
//    TextView txtMachine,txtSlot,txtUserId,txtPartNumber,txtPer,txtLength,txtTime,txtTrolley;
//    RadioGroup radioGroub;
//    RadioButton MessageId;
//    TextView tvRemedy;
//    EditText tboxOther;
//    Button btn_cancel,btn_pick_book;
//    Button btn_book_temp;
//    private table_display tableDisplay;
//    String[] Cells;
//    String buttonContent;
//    String TypeOfValue = "";
//    private NetworkCallback netCallbackBook;
//    private NetworkCallback netCallbackPick;
//    com.sophic.edowntime.RestAPI.RestCrud RestCrud;
//
//    public int isOkpressed() {
//        return Okpressed;
//    }
//
//    public void setOkpressed(int okpressed) {
//        Okpressed = okpressed;
//    }
//
//
//    public String getMessage() {
//        return Message;
//    }
//
//    public void setMessage(String message) {
//        Message = message;
//    }
//
//    public int Okpressed;
//    public String Message;
//    public Activity activity;
//
//    String Data[];
//    //public TableDisplay_Popup(table_display tableDisplay,Activity a,String[] cells,Button btn) {
//    public TableDisplay_Popup(table_display tableDisplay,Activity a,String[] cells,Button btn,String _TypeOfValue,String[] Data) {
//        super(a);
//        // TODO Auto-generated constructor stub
//        this.activity = a;
//        this.Cells = cells;
//        this.btn_book_temp = btn;
//        this.buttonContent = btn.getText().toString();
//        this.tableDisplay = tableDisplay;
//        this.TypeOfValue = _TypeOfValue;
//        this.Data = Data;
//
//    }
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//       super.onCreate(savedInstanceState);
//       requestWindowFeature(Window.FEATURE_NO_TITLE);
//
//
//        if(TypeOfValue.equals("Done")) {
//
//            //
//            RestCrud = APIClient.getClient().create(RestCrud.class);
//
//            setContentView(R.layout.activity_table_display_popup);
//            radioGroub = (RadioGroup) findViewById(R.id.RadioGroup1);
//            tboxOther = (EditText)  findViewById(R.id.tboxOther);
//            radioGroub.setOnClickListener(this);
//
//            String remedy = Data[6];
//
//            int OrignalLength = remedy.length();
//            int firstIndex = remedy.indexOf("1. ");
//            List<String> RemedyList = new ArrayList<String>();
//
//            Boolean nextdata = false;
//
//            if(firstIndex == 0)
//            {
//                nextdata = true;
//            }
//            else{
//                nextdata = false;
//            }
//
//            if (firstIndex == 0)
//            {
//                int firstIndexEnd = remedy.indexOf(".", remedy.indexOf('.') + 1);
//                String FirstRemedy = remedy.substring(firstIndex, firstIndexEnd + 1);
//                RemedyList.add(FirstRemedy);
//                int nextindex = 2;
//                int NextRemedyEnd=0;
//                String NextRemedy="";
//                while (nextdata)
//                {
//                    /*
//                    int ind = remedy.indexOf("   "+nextindex+". ");
//                    Log.e("ZZ",remedy);
//                    if(ind != -1)
//                    {
//                        NextRemedyEnd = remedy.indexOf('.', ind + 5);
//                        NextRemedy = remedy.substring(ind + 3, NextRemedyEnd - (ind + 2));
//                    }
//                    if (ind == -1)
//                    {
//                        ind = remedy.indexOf("  " + nextindex + ". ");
//                        NextRemedyEnd = remedy.indexOf('.', ind + 5);
//                        NextRemedy = remedy.substring(ind + 2, NextRemedyEnd - (ind + 2));
//                    }
//                    if (ind == -1)
//                    {
//                        ind = remedy.indexOf(" " + nextindex + ". ");
//                        NextRemedyEnd = remedy.indexOf('.', ind + 5);
//                        NextRemedy = remedy.substring(ind + 1, NextRemedyEnd - (ind + 2));
//                    }
//                    if (ind == -1)
//                    {
//                        nextdata = false;
//                        break;
//                    }
//                    */
//                    int ind = remedy.indexOf(" " + nextindex + ". ");
//                    if (ind == -1)
//                    {
//                        nextdata = false;
//                        break;
//                    }
//                    NextRemedyEnd = remedy.indexOf('.', ind + 3);
//                    NextRemedy = remedy.substring(ind, NextRemedyEnd+1);
//
//                    //int NextIndexEnd = remedy.indexOf(".", remedy.indexOf('.') + 1);
//                    nextindex++;
//
//                    RemedyList.add(NextRemedy);
//
//                    if ((NextRemedyEnd + 1) == OrignalLength)
//                        nextdata = false;
//                }
//
//
//                for (int i=0;i<RemedyList.size();i++){
//                    RadioButton rbn = new RadioButton(TableDisplay_Popup.this.getContext());
//                    rbn.setId(i + 1000);
//                    rbn.setText(RemedyList.get(i));
//                    radioGroub.addView(rbn);
//
//                }
//
//            }
//            else if(remedy.equals(null) || remedy.contains("null") || remedy==null){
//
//            }
//            else
//            {
//                RadioButton rbn = new RadioButton(TableDisplay_Popup.this.getContext());
//                rbn.setId(0);
//                rbn.setText(remedy);
//                radioGroub.addView(rbn);
//            }
//
//            RadioButton rbn = new RadioButton(TableDisplay_Popup.this.getContext());
//            rbn.setText("Other");
//            radioGroub.addView(rbn);
//        }
//        else{
//            setContentView(R.layout.accpet_layout);
//        }
//
//        btn_pick_book=(Button) findViewById(R.id.btn_pick_book);
//        //btn_pick_book.setText(buttonContent);
//        tvRemedy=(TextView) findViewById(R.id.tvRemedy);
//        btn_cancel=(Button) findViewById(R.id.btn_cancel);
//        //tboxOther.setText(Data[6]);
//
//        btn_cancel.setOnClickListener(this);
//        btn_pick_book.setOnClickListener(this);
//    }
//
//    public void Initialize(){
//
//    }
//
//    @Override
//    public void onClick(View view) {
//        switch (view.getId()) {
//            case R.id.btn_cancel:
//                setOkpressed(0);
//                dismiss();
//                break;
//
//            case R.id.btn_pick_book:
//                setOkpressed(1);
//
//                if(TypeOfValue.equals("Done")){
//                    if (radioGroub.getCheckedRadioButtonId() == -1)
//                    {
//                        setOkpressed(-1);
//                    }
//                    else
//                    {
//                        int selectedId = radioGroub.getCheckedRadioButtonId();
//                        MessageId = (RadioButton) findViewById(selectedId);
//                        String msg = MessageId.getText().toString();
//                        if(msg.equals("Other")){
//                            msg = tboxOther.getText().toString();
//                            if(msg.equals("")){
//                                setOkpressed(-2);
//                            }
//                            else{
//                                setMessage(msg);
//                            }
//
//                        }
//                        else{
//                            setMessage(msg);
//                        }
//
//                    }
//
//
//                }
//                dismiss();
//                break;
//        }
//    }
//
//}
