package com.sophic.edowntime.Dialogs;

import android.graphics.Point;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.sophic.edowntime.Model.Machine;
import com.sophic.edowntime.R;

public class DisplayDetails_Popup_new extends DialogFragment {

    private AppCompatActivity parentActivity;
    private Machine machine;
    private TextView tvRemedy,tvCause,tvInfo;

    public DisplayDetails_Popup_new() {
        // Empty constructor is required for DialogFragment
        // Make sure not to add arguments to the constructor
        // Use `newInstance` instead as shown below
    }

    public static DisplayDetails_Popup_new newInstance(AppCompatActivity parentActivity, Machine machine) {
        DisplayDetails_Popup_new frag = new DisplayDetails_Popup_new();
        frag.parentActivity = parentActivity;
        frag.machine = machine;
        return frag;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        int style = DialogFragment.STYLE_NORMAL, theme = 0;
        setStyle(style, R.style.DialogWithCloseBtnStyle);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootview = inflater.inflate(R.layout.layout_detailview_new,container,false);
        getDialog().requestWindowFeature(STYLE_NO_TITLE);
        //Found here https://stackoverflow.com/questions/16480114/dialogfragment-setcancelable-property-not-working
        //instead of getDialog().setCancelable(false); you have to use directly setCancelable(false);
        setCancelable(false);

        ImageView closeIB = rootview.findViewById(R.id.closeIV);
        closeIB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().dismiss();
            }
        });

        tvInfo= rootview.findViewById(R.id.tvInfo);
        tvRemedy= rootview.findViewById(R.id.tvRemedy);
        tvCause= rootview.findViewById(R.id.tvCause);
        //btn_cancel=(Button) findViewById(R.id.btn_cancel);
        if(machine != null){
            if(machine.Remedy != null && !TextUtils.isEmpty(machine.Remedy) && !machine.Remedy.equals("null")){
                tvRemedy.setText(machine.Remedy);
            }
            else{
                tvRemedy.setText("null");
            }
            if(machine.Cause != null && !TextUtils.isEmpty(machine.Cause) && !machine.Cause.equals("null")){
                tvCause.setText(machine.Cause);
            }
            else{
                tvCause.setText("null");;
            }
            tvInfo.setText("Assigned To:"+"null");
        }

        return rootview;
    }

    @Override
    public void onResume() {
        super.onResume();
        Window window = getDialog().getWindow();
        //https://guides.codepath.com/android/using-dialogfragment
        Point size = new Point();
        // Store dimensions of the screen in `size`
        Display display = null;
        if (window != null) {
            display = window.getWindowManager().getDefaultDisplay();
            display.getSize(size);
            // Set the width of the dialog proportional to 75% of the screen width
//            window.setLayout((int) (size.x * 0.95), WindowManager.LayoutParams.WRAP_CONTENT);
            window.setLayout((int) (size.x * 0.95), WindowManager.LayoutParams.WRAP_CONTENT);
            window.setGravity(Gravity.CENTER);
        }
    }
}
