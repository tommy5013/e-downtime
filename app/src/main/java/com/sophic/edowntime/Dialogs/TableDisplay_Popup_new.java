package com.sophic.edowntime.Dialogs;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.sophic.edowntime.Model.Machine;
import com.sophic.edowntime.R;
import com.sophic.edowntime.RestAPI.APIClient;
import com.sophic.edowntime.RestAPI.RestCrud;

import java.util.ArrayList;
import java.util.List;

public class TableDisplay_Popup_new extends Dialog implements View.OnClickListener  {

    private String TAG = TableDisplay_Popup_new.class.getName();
    private Button btn_save,btn_select_date;
    private TextView txtMachine,txtSlot,txtUserId,txtPartNumber,txtPer,txtLength,txtTime,txtTrolley;
    private RadioGroup radioGroub;
    private RadioButton MessageId;
    private TextView tvRemedy;
    private EditText tboxOther;
    private Button btn_cancel,btn_pick_book;
    private RestCrud RestCrud;
    private AppCompatActivity parentActivity;
    private Machine machine;
    private String buttonName;
    private int Okpressed;
    private String Message;
    private ImageView closeIV;

    public TableDisplay_Popup_new(AppCompatActivity parentActivity, Machine machine, String buttonName) {
        super(parentActivity,R.style.DialogWithCloseBtnStyle);
        this.parentActivity = parentActivity;
        this.machine = machine;
        this.buttonName = buttonName;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
       super.onCreate(savedInstanceState);
       requestWindowFeature(Window.FEATURE_NO_TITLE);
//       getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        if(buttonName.equals("Done")) {

            RestCrud = APIClient.getClient().create(RestCrud.class);

            setContentView(R.layout.activity_table_display_popup);
            radioGroub = findViewById(R.id.RadioGroup1);
            tboxOther = findViewById(R.id.tboxOther);
            closeIV = findViewById(R.id.closeIV);
            closeIV.setOnClickListener(this);
            radioGroub.setOnClickListener(this);

            String remedy = "null";
            //TODO
            int OrignalLength = remedy.length();
            int firstIndex = remedy.indexOf("1. ");
            List<String> RemedyList = new ArrayList<String>();

            Boolean nextdata = false;

            if(firstIndex == 0)
            {
                nextdata = true;
            }
            else{
                nextdata = false;
            }

            if (firstIndex == 0)
            {
                int firstIndexEnd = remedy.indexOf(".", remedy.indexOf('.') + 1);
                String FirstRemedy = remedy.substring(firstIndex, firstIndexEnd + 1);
                RemedyList.add(FirstRemedy);
                int nextindex = 2;
                int NextRemedyEnd=0;
                String NextRemedy="";
                while (nextdata)
                {
                    int ind = remedy.indexOf(" " + nextindex + ". ");
                    if (ind == -1)
                    {
                        nextdata = false;
                        break;
                    }
                    NextRemedyEnd = remedy.indexOf('.', ind + 3);
                    NextRemedy = remedy.substring(ind, NextRemedyEnd+1);

                    //int NextIndexEnd = remedy.indexOf(".", remedy.indexOf('.') + 1);
                    nextindex++;

                    RemedyList.add(NextRemedy);

                    if ((NextRemedyEnd + 1) == OrignalLength)
                        nextdata = false;
                }


                for (int i=0;i<RemedyList.size();i++){
                    RadioButton rbn = new RadioButton(TableDisplay_Popup_new.this.getContext());
                    rbn.setId(i + 1000);
                    rbn.setText(RemedyList.get(i));
                    radioGroub.addView(rbn);

                }

            }
            else if(remedy.equals(null) || remedy.contains("null") || remedy==null){

            }
            else
            {
                RadioButton rbn = new RadioButton(TableDisplay_Popup_new.this.getContext());
                rbn.setId(0);
                rbn.setText(remedy);
                rbn.setTextColor(parentActivity.getResources().getColor(R.color.black));
                radioGroub.addView(rbn);
            }

            RadioButton rbn = new RadioButton(TableDisplay_Popup_new.this.getContext());
            rbn.setText("Other");
            rbn.setTextColor(parentActivity.getResources().getColor(R.color.black));
            radioGroub.addView(rbn);
        }
        else{
            setContentView(R.layout.accpet_layout);
            closeIV = findViewById(R.id.closeIV);
            closeIV.setOnClickListener(this);
        }

        btn_pick_book = findViewById(R.id.btn_pick_book);
        tvRemedy = findViewById(R.id.tvRemedy);
        btn_cancel = findViewById(R.id.btn_cancel);
        btn_cancel.setOnClickListener(this);
        btn_pick_book.setOnClickListener(this);

        setDialogWidth();
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_cancel:
                setOkpressed(0);
                dismiss();
                break;

            case R.id.btn_pick_book:
                setOkpressed(1);
                if(buttonName.equals("Done")){
                    radioGroub.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(RadioGroup group, int checkedId) {
                            RadioButton radioButton = findViewById(group.getCheckedRadioButtonId());
                            Log.d(TAG,radioButton.getText().toString());
                            if(group.getCheckedRadioButtonId() == -1){
                                setOkpressed(-1);
                            }
                            else{
                                String msg = radioButton.getText().toString();
                                if(msg.equals("Other")){
                                    msg = tboxOther.getText().toString();
                                    if(msg.equals("")){
                                        setOkpressed(-2);
                                    }
                                    else{
                                        setMessage(msg);
                                    }
                                }
                                else{
                                    setMessage(msg);
                                }
                            }
                        }
                    });
                }
                dismiss();
                break;

            case R.id.closeIV:
                dismiss();
                break;
        }
    }

    private void setDialogWidth(){
        //Found here https://juejin.im/entry/588458bcb123db7389d2074f
        Window win = getWindow();
        WindowManager.LayoutParams lp = null;
        if (win != null) {
            lp = win.getAttributes();
            lp.gravity = Gravity.CENTER;
            lp.width = WindowManager.LayoutParams.MATCH_PARENT;
            lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
            win.setAttributes(lp);
        }
    }

    public int isOkpressed() {
        return Okpressed;
    }

    public void setOkpressed(int okpressed) {
        Okpressed = okpressed;
    }


    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }


}
