package com.sophic.edowntime.Dialogs;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.sophic.edowntime.Activities.COMainPage;
import com.sophic.edowntime.Activities.DownMainPage;
import com.sophic.edowntime.Activities.MainActivity;
import com.sophic.edowntime.Activities.PMMainPage;
import com.sophic.edowntime.Helper.AlertHelper;
import com.sophic.edowntime.Helper.AutoLogout;
import com.sophic.edowntime.Helper.LoadingClass;
import com.sophic.edowntime.Model.CoModel;
import com.sophic.edowntime.Model.DownTimeModel;
import com.sophic.edowntime.Model.MachineModel;
import com.sophic.edowntime.Model.MyConfig;
import com.sophic.edowntime.Model.PmModel;
import com.sophic.edowntime.R;
import com.sophic.edowntime.RestAPI.APIClient;
import com.sophic.edowntime.RestAPI.RestCrud;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddPMDialogFragment extends DialogFragment implements View.OnClickListener {
    public String TAG = AddPMDialogFragment.class.getName();
    private TextView mDisplayDate;
    private TextView mDisplayTime;
    private DatePickerDialog.OnDateSetListener mDateSetListener;
    private Spinner spinnerLine,spinnerMachine;
    private ArrayList<String> LineArray = new ArrayList<String>();
    private ArrayList<String> MachineArray = new ArrayList<String>();
    private Button btnSavePm,btnBack;
    private RestCrud RestCrud;
    private TextView tvPMType,tvPmDuration,tvPMDate,tvPmTime,tvTittle,tvTypeText,lblDuration;
    private AlertHelper alertHelper;
    private MyConfig myConfig = MyConfig.getInstance();
    private String PageType="";
    private LoadingClass LoadActivity;
    private AutoLogout autoLogout;
    private AppCompatActivity parentActivity;
    private ImageView closeIV;
    private String LineId;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setStyle(STYLE_NO_TITLE, 0);
        int style = DialogFragment.STYLE_NORMAL, theme = 0;
        setStyle(0, R.style.DialogWithCloseBtnStyle);
    }

    public AddPMDialogFragment() {
        // Empty constructor is required for DialogFragment
        // Make sure not to add arguments to the constructor
        // Use `newInstance` instead as shown below
    }

    public static AddPMDialogFragment newInstance(AppCompatActivity parentActivity, String PageType, String LineId) {
        AddPMDialogFragment frag = new AddPMDialogFragment();
        frag.parentActivity = parentActivity;
        frag.PageType = PageType;
        frag.LineId = LineId;
        return frag;
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        getDialog().getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        getDialog().setCanceledOnTouchOutside(false);
        getDialog().setCancelable(true);

        return inflater.inflate(R.layout.dialog_add_pm, container);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        alertHelper = new AlertHelper();
        LoadActivity = new LoadingClass(parentActivity,1);

        mDisplayDate = (TextView) view.findViewById(R.id.tvPMDate);
        mDisplayTime = (TextView) view.findViewById(R.id.tvPmTime);
        RestCrud = APIClient.getClient().create(RestCrud.class);
        spinnerLine=(Spinner)view.findViewById(R.id.spinnerLine);
        spinnerMachine=(Spinner)view.findViewById(R.id.spinnerMachine);
        btnSavePm =(Button)view.findViewById(R.id.btnSavePm);
        btnBack =(Button)view.findViewById(R.id.btnBack);
        tvTittle = (TextView) view.findViewById(R.id.tvStartDate);
        tvTypeText = (TextView) view.findViewById(R.id.tvTypeText);
        tvPMType= (TextView) view.findViewById(R.id.tvPMType);
        tvPmDuration = (TextView) view.findViewById(R.id.tvPmDuration);
        tvPMDate = (TextView) view.findViewById(R.id.tvPMDate);
        tvPmTime = (TextView) view.findViewById(R.id.tvPmTime);
        lblDuration = (TextView) view.findViewById(R.id.textView22);
        closeIV = (ImageView) view.findViewById(R.id.closeIV);
        closeIV.setOnClickListener(this);

        initialize();
    }
    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
        setWidthScreen();
    }

    private void setWidthScreen(){
        Window window = getDialog().getWindow();
        Point size = new Point();

        Display display = window.getWindowManager().getDefaultDisplay();
        display.getSize(size);

        int width = size.x;

        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        window.setGravity(Gravity.CENTER);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.closeIV:
                dismiss();
                break;

            case R.id.btnSavePm:
                SavePm();
                break;
        }
    }

    private void initialize(){
        autoLogout = new AutoLogout(parentActivity);
        autoLogout.start();

        Calendar c = Calendar.getInstance();

        int Hr12=c.get(Calendar.HOUR);
        int Min=c.get(Calendar.MINUTE);

        String ampm="PM";
        int _ampm = c.get(Calendar.AM_PM);
        if(_ampm == Calendar.AM){
            ampm="AM";
        }

        Date d = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("M/d/yyyy");
        String formattedDate = df.format(d);

        String time = Hr12 + ":" + Min + " " + ampm;
        mDisplayTime.setText(time);
        mDisplayDate.setText(formattedDate);


        if(PageType.equals("PM")){
            tvTittle.setText("New Prevention Maintenance");
            tvTypeText.setText("Preventive Maintenance Type");
        }
        else if(PageType.equals("DOWN")){
            tvTittle.setText("New Downtime");
            tvTypeText.setText("Issue:                   ");
            tvPmDuration.setVisibility(View.GONE);
            lblDuration.setVisibility(View.GONE);
        }
        else{
            tvTittle.setText("New Change Over");
            tvTypeText.setText("Change Over               ");
        }
        mDisplayDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog = new DatePickerDialog(
                        parentActivity,
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                        mDateSetListener,
                        year,month,day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();

            }
        });

        mDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {

                month = month + 1;
                String date = month + "/" + day+ "/" + year;
                mDisplayDate.setText(date);
            }
        };

        mDisplayTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar cal = Calendar.getInstance();

                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);

                TimePickerDialog timePickerDialog = new TimePickerDialog(parentActivity, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int hourOfDay, int minute) {
                        Calendar calendar = Calendar.getInstance();
                        calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
                        calendar.set(Calendar.MINUTE, minute);

                        String am_pm;
                        if(calendar.get(Calendar.AM_PM) == Calendar.AM)
                            am_pm = "AM";
                        else
                            am_pm = "PM";

                        String hour = (calendar.get(Calendar.HOUR) == 0) ? "12" : ""+calendar.get(Calendar.HOUR);
                        String minutes = fixZero(calendar.get(Calendar.MINUTE));
                        String time = hour + ":" + minutes + " " + am_pm;

                        mDisplayTime.setText(time);
                    }
                }, hour,minute, false);
                timePickerDialog.show();
            }
        });


        LoadLine();
        btnSavePm.setOnClickListener(this);
        btnBack.setOnClickListener(this);

        //Adding check if line seelected
        spinnerLine.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {

                LoadMachine(spinnerLine.getSelectedItem().toString());


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                // TODO Auto-generated method stub

            }
        });
    }

    private String fixZero(int num){
        return (num < 10) ? "0" + num : "" + num;
    }

    private  void LoadMachine(String LineName)
    {
        spinnerMachine.setAdapter(null);
        MachineArray.clear();
        RestCrud = APIClient.getClient().create(RestCrud.class);
        LoadActivity.show();
        Call<List<MachineModel.Machine>> call = RestCrud.GetMachine(LineName);
        myConfig.setTaskListAllMachine(false);
        myConfig.setTaskListPMCO(false);
        call.enqueue(new Callback<List<MachineModel.Machine>>() {

            @Override
            public void onResponse(Call<List<MachineModel.Machine>> call, Response<List<MachineModel.Machine>> response) {
                LoadActivity.dismiss();
                List<MachineModel.Machine> resource = response.body();

                if(resource!=null){
                    for (int i=0;i<resource.size();i++){
                        MachineArray.add(resource.get(i).StationName);

                    }
                    String[] arraySpinner = new String[MachineArray.size()];
                    for (int i=0;i<MachineArray.size();i++){
                        arraySpinner[i]=MachineArray.get(i);
                    }

                    SetAdap2(arraySpinner);

                }
                myConfig.setTaskListAllMachine(true);
                myConfig.setTaskListPMCO(true);
                Toast.makeText(parentActivity,"Machine Loaded" , Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onFailure(Call<List<MachineModel.Machine>> call, Throwable t) {
                Toast.makeText(parentActivity, "ERROR LOADING MACHINE!", Toast.LENGTH_SHORT).show();
                myConfig.setTaskListAllMachine(true);
            }
        });
    }

    private void LoadLine()
    {
        RestCrud = APIClient.getClient().create(RestCrud.class);

        LoadActivity.show();
        Call<List<MachineModel.Line>> call = RestCrud.GetLine();


        call.enqueue(new Callback<List<MachineModel.Line>>() {


            @Override
            public void onResponse(Call<List<MachineModel.Line>> call, Response<List<MachineModel.Line>> response) {
                LoadActivity.dismiss();
                List<MachineModel.Line> resource = response.body();

                if(resource!=null || resource.size() != 0 ){
                    for (int i=0;i<resource.size();i++){
                        String linenamemap = resource.get(i).LineNameMap.replace(".","_");
                        LineArray.add(linenamemap);
                    }
                    String[] arraySpinner = new String[LineArray.size()];
                    for (int i=0;i<LineArray.size();i++){
                        arraySpinner[i]=LineArray.get(i);
                    }

                    SetAdap(arraySpinner);

                }
            }

            @Override
            public void onFailure(Call<List<MachineModel.Line>> call, Throwable t) {
                Toast.makeText(parentActivity, "ERROR LOADING LINE!", Toast.LENGTH_SHORT).show();
            }
        });


    }

    private  void SetAdap(String[] arraySpinner1){
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(parentActivity,
                R.layout.spinner_item, arraySpinner1);
        adapter.setDropDownViewResource(R.layout.spinner_item);
        spinnerLine.setAdapter(adapter);

    }

    private  void SetAdap2(String[] arraySpinner1){
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(parentActivity,
                R.layout.spinner_item, arraySpinner1);
        adapter.setDropDownViewResource(R.layout.spinner_item);

        spinnerMachine.setAdapter(adapter);

    }


    private void SaveAPM(String StartDate) {

        Log.d("PM ERROR","HERE003");
        String Line = spinnerLine.getSelectedItem().toString();
        String Machine = spinnerMachine.getSelectedItem().toString();
        int Duration = Integer.valueOf(tvPmDuration.getText().toString());
        String PmTYpe = tvPMType.getText().toString();

        final PmModel.ModelNewPm modelNewPm = new PmModel().new ModelNewPm();

        modelNewPm.LineName = Line;
        modelNewPm.MachineName = Machine;
        modelNewPm.PmType = PmTYpe;
        modelNewPm.Duration = Duration;
        modelNewPm.StartDate = StartDate;
        modelNewPm.Username = MainActivity.username;

        LoadActivity.show();
        Log.d("PM ERROR","HERE004"+"|"+modelNewPm.LineName+"|"+modelNewPm.MachineName+"|"+modelNewPm.Username+"|"+modelNewPm.StartDate);
        Call<String> call = RestCrud.AddNewPm(modelNewPm);

        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                LoadActivity.dismiss();
                String dt = response.body().toString();
                Log.d("PM ERROR","HERE005"+dt.toString());
                //Toast.makeText(AddPMActivity.this, dt, Toast.LENGTH_LONG).show();
                Toast.makeText(parentActivity, "PM ADDED", Toast.LENGTH_LONG).show();
                ((PMMainPage)parentActivity).onResume();
                dismiss();
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.d("PM ERROR","HERE007:"+t.getMessage());
                Toast.makeText(parentActivity, t.getMessage(), Toast.LENGTH_LONG).show();
                //finish();
            }
        });

    }

    private void SaveAPM2(String StartDate) {

        String Line = spinnerLine.getSelectedItem().toString();
        String Machine = spinnerMachine.getSelectedItem().toString();

        String PmTYpe = tvPMType.getText().toString();

        CoModel.ModelNewCo modelNewCo = new CoModel().new ModelNewCo();


        if(Line.isEmpty() ||tvPmDuration.getText().toString().isEmpty() || Machine.isEmpty() || StartDate.isEmpty() || PmTYpe.isEmpty()){
            Toast.makeText(parentActivity,"PLease Enter All details",Toast.LENGTH_SHORT).show();
            return;
        }

        int Duration = Integer.valueOf(tvPmDuration.getText().toString());

        modelNewCo.LineName = Line;
        modelNewCo.MachineName = Machine;
        modelNewCo.CoType = PmTYpe;
        modelNewCo.Duration = Duration;
        modelNewCo.StartDate = StartDate;
        modelNewCo.Username = MainActivity.username;



        Call<String> call = RestCrud.AddNewCo(modelNewCo);
        LoadActivity.show();
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                LoadActivity.dismiss();
                String dt = response.body().toString();

                if(dt.equals("Success"))
                {
                    Toast.makeText(parentActivity, "CO ADDED", Toast.LENGTH_LONG).show();
                    myConfig.setCoCurrentPage("MainPage");
                    ((COMainPage)parentActivity).onResume();
                    dismiss();
                }
                else{
                    Toast.makeText(parentActivity, "ERROR Adding Co here"+dt, Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                Log.d("PM ERROR",t.getMessage().toString());
                Toast.makeText(parentActivity, "ERROR CREATING NEWa CO"+t.getMessage(), Toast.LENGTH_LONG).show();
                dismiss();
            }
        });

    }

    private void SaveDownTime(String StartDate) {

        String Line = spinnerLine.getSelectedItem().toString();
        String Machine = spinnerMachine.getSelectedItem().toString();
        String PmTYpe = tvPMType.getText().toString();

        DownTimeModel.ModelAddCloseDowntime modelNewDown = new DownTimeModel().new ModelAddCloseDowntime();


        if(Line.isEmpty() || Machine.isEmpty() || StartDate.isEmpty() || PmTYpe.isEmpty()){
            Toast.makeText(parentActivity,"PLease Enter All Details",Toast.LENGTH_SHORT).show();
            return;
        }

        modelNewDown.LineNameMap = Line;
        modelNewDown.StationName = Machine;
        modelNewDown.CoType = "DOWNTIME";
        modelNewDown.StartDate = StartDate;
        modelNewDown.Others = "Issue:"+PmTYpe;
        modelNewDown.PIC = MainActivity.username;



        Call<String> call = RestCrud.AddNewDownTime(modelNewDown);
        LoadActivity.show();
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                LoadActivity.dismiss();
                String dt = response.body().toString();

                if(dt.contains("debug"))
                {
                    Toast.makeText(parentActivity, "Debug:"+dt, Toast.LENGTH_LONG).show();
                    myConfig.setCoCurrentPage("MainPage");
                    dismiss();
                }

                if(dt.contains("true"))
                {
                    Toast.makeText(parentActivity, "DOWNTIME ADDED", Toast.LENGTH_LONG).show();
                    ((DownMainPage)parentActivity).onResume();
                    myConfig.setCoCurrentPage("MainPage");
                    dismiss();
                }
                else{
                    Toast.makeText(parentActivity, "ERROR Adding Downtime "+dt, Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                LoadActivity.dismiss();
                Toast.makeText(parentActivity, "ERROR CREATING NEW Downtime"+t.getMessage(), Toast.LENGTH_LONG).show();
                dismiss();
            }
        });

    }

    private void SavePm(){
        String time = (String) mDisplayTime.getText();
        String date = (String) mDisplayDate.getText();
        String _date = date.replace("/", "-");
        String time24="";

        if(time.isEmpty() || date.isEmpty()){
            Toast.makeText(parentActivity,"Please Add Date and Time",Toast.LENGTH_SHORT).show();
            return;
        }

        try {
            SimpleDateFormat inFormat = new SimpleDateFormat("hh:mm aa");
            SimpleDateFormat outFormat = new SimpleDateFormat("HH:mm");
            time24 = outFormat.format(inFormat.parse(time));
            time24 = time24.replace(":", "-");
        }
        catch (ParseException e)
        {
            Log.d("PM ERROR",e.getMessage());
        }
        String StartDate=_date +" "+time24;

        Log.d("PM ERROR","HERE001");
        if(PageType.equals("PM")){
            Log.d("PM ERROR","HERE002");
            SaveAPM(StartDate);
        }
        else if(PageType.equals("DOWN")){
            SaveDownTime(StartDate);
        }
        else {
            SaveAPM2(StartDate);
        }
    }
}
