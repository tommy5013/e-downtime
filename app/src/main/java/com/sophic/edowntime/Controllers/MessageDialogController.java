package com.sophic.edowntime.Controllers;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;

import com.sophic.edowntime.Dialogs.DialogFragmentCustom;

public class MessageDialogController {

    public void showMessageAlert(AppCompatActivity parentActivity, String title, String message,String TAG){
        if(parentActivity != null && !parentActivity.isFinishing()){
            FragmentManager fm = parentActivity.getSupportFragmentManager();
            Fragment fr = parentActivity.getSupportFragmentManager().findFragmentByTag(TAG);

            DialogFragmentCustom dialogFragmentCustom = DialogFragmentCustom.newInstance(parentActivity,title, message);
            if (fr == null) {
                dialogFragmentCustom.show(fm, TAG);
            }
        }
    }
}
