package com.sophic.edowntime.Controllers;

import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

public class ToastController {

    public void SetToast(AppCompatActivity parentActivity, String message){
        Toast.makeText(parentActivity, message, Toast.LENGTH_SHORT).show();
    }
}
