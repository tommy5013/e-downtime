package com.sophic.edowntime.Controllers;

import android.app.Dialog;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.sophic.edowntime.Activities.MainActivity;
import com.sophic.edowntime.R;

public class MainActivityController {

    public MainActivity parentActivity;

    public void callLoginDialog() {
        Dialog myDialog = new Dialog(parentActivity);
        myDialog.setContentView(R.layout.login_popup);
        myDialog.setCancelable(false);
        Button login = (Button) myDialog.findViewById(R.id.btn_PassLogin);
        myDialog.show();

        login.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                //your login calculation goes here
            }
        });
    }

    public void Close() {
        parentActivity.finishAffinity();
    }
}
