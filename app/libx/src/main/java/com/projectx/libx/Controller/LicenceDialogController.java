package com.projectx.libx.Controller;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.projectx.libx.Config;
import com.projectx.libx.Model.LicenceModel;
import com.projectx.libx.Utils.APIClient;
import com.projectx.libx.Utils.RestCrud;
import com.projectx.libx.Utils.SharedPref;
import com.projectx.libx.View.LicenceDialog;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LicenceDialogController {


    void SaveLicenceStatus(final Context context){
        SharedPref sharedPref = new SharedPref();
        sharedPref.SaveLic(context,"1");
    }


    public void CheckLicence(final Context context, String DeviceName, String Location,String AppName) {

        //get lic

        RestCrud restCrud = APIClient.getClient().create(RestCrud.class);
        LicenceModel m = new LicenceModel();
        m.DeviceName = DeviceName;
        m.Location = Location;
        m.AppName = AppName;
        Call<LicenceModel> call = restCrud.CheckLicence(m);

        call.enqueue(new Callback<LicenceModel>() {
//test
            @SuppressLint("WrongConstant")
            @Override
            public void onResponse(Call<LicenceModel> call, Response<LicenceModel> response) {
//
                if(response.isSuccessful()){
                    LicenceModel m  = response.body();

                    if(m.LicStatus==-1){
                        Toast.makeText(context, "Error: Licence Not Found For This Device", Toast.LENGTH_SHORT).show();
                    }
                    else if(m.LicStatus==0){
                        Toast.makeText(context, "Error: Licence Registered With Other Device", Toast.LENGTH_SHORT).show();
                    }
                    else if(m.LicStatus==1){
                        SaveLicenceStatus(context);
                        Toast.makeText(context, "Sucess: Please Restart App", Toast.LENGTH_SHORT).show();
                    }
                }
            }


            @Override
            public void onFailure(Call<LicenceModel> call, Throwable t) {

                Toast.makeText(context, "Errror:"+t.getMessage(), Toast.LENGTH_SHORT).show();
                //DataLoading=false;
            }

        });


    }

}
