package com.projectx.libx.Utils;

import com.projectx.libx.Model.LicenceModel;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Url;

public interface RestCrud {

    @GET
    Call<ResponseBody> downloadFileWithDynamicUrlSync(@Url String fileUrl);

    @Headers({
            "Content-Type: application/json",
            "Accept: text/plain"
    })
    @POST("RestApi/CheckLicence")
    Call<LicenceModel> CheckLicence (@Body LicenceModel licenceModel);

}
